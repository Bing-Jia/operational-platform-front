/**
 * @File   : Routers.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/4
 * @Desc   : 项目路由总览
 **/

import React from "react";
import {Route, Switch} from "react-router-dom";

import Login from "./containers/Login/Login";
import PvHome from "./containers/PvHome/PvHome";
import Website from "./containers/Website/Website";
import Register from "./containers/Register/Register";
import Projects from "./containers/Projects/Projects";
import NotFound from "./components/NotFound/NotFound";
import Backstage from "./containers/Backstage/Backstage";
import PvProject from "./containers/PvProject/PvProject";
import PvProjects from "./containers/PvProjects/PvProjects";


export default class Routers extends React.Component {

  render() {
    return (
      <Switch>
        {/** 官网 **/}
        <Route exact path={'/'} component={Website}/>

        {/** 登录 **/}
        <Route exact path={'/login'} component={Login}/>

        {/** 注册 **/}
        <Route exact path={'/register'} component={Register}/>

        {/** 系列选择 **/}
        <Route exact path={'/classify'} component={Projects}/>

        {/** 光伏系列 **/}
        <Route exact path={'/pv'} component={PvHome}/>
        <Route exact path={'/pv/projects/:auth_id'} component={PvProjects}/>
        <Route path={'/pv/project/:auth_id/:project_id'} component={PvProject}/>


        {/** 风机系列 **/}
        {/*<Route exact path={'/projects/fan'} component={FanHome}/>*/}
        {/*<Route exact path={'/fan/projects/:auth_id'} component={FanProjects}/>*/}
        {/*<Route exact path={'/fan/project/:auth_id'} component={FanProject}/>*/}

        {/** 后台管理 **/}
        <Route path={'/backstage'} component={Backstage}/>

        {/** 错误跳转 **/}
        <Route
          render={
            function () {
              return <NotFound/>;
            }
          }
        />
      </Switch>
    )
  }
}