/**
 * 通知函数
 * */
import {notification} from 'antd/lib/index'


export const openNotification = (message, description, duration=2) => {
  const args = {
    message: message,
    description: description,
    duration: duration,
  };
  notification.open(args);
};

