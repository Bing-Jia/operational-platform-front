/**
 * 设置axios请求头认证
 * */

import axios from 'axios'

const setAuthToken = state => {
  if(state){
    // 请求头添加token
    axios.defaults.headers.common["HTTP_AUTHORIZATION"] = localStorage.getItem('Authorization')
  }else{
    // 请求头移除token
    delete axios.defaults.headers.common["HTTP_AUTHORIZATION"]
  }
};

export default setAuthToken