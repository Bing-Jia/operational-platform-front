/**
 * @File   : Projects.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/4
 * @Desc   : 项目选择页
 **/

import {Row, Col} from "antd";
import {connect} from "react-redux";
import React, {Component} from "react";

import {getProjectTotal} from "src/api/request";
import logo from 'src/assets/images/logo/logo.png'
import fan from 'src/assets/images/projects/风机.png'
import pv from 'src/assets/images/projects/光伏板.png'
import pv_image from 'src/assets/images/banner/pv.jpg'
import fan_image from 'src/assets/images/banner/fan.jpg'
import Copyright from 'src/components/Copyright/Copyright'
import CellBlock from 'src/components/CellBlock/CellBlock'
import ProjectHeader from 'src/components/Header/ProjectHeader/ProjectHeader'

import "./Projects.sass";


class Projects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      "navigation": {
        "logo": logo,
        "title": '平台选择页',
        "navList": [
          {"name": "首页", "url": "/"},
          {"name": "后台", "url": "/backstage"},
          {"name": "数据库", "url": "/"},
        ]
      },
      "PV": {
        "logo": pv,
        "title": '无人机智能光伏运维',
        "image": pv_image,
        "url": '/pv',
        "description": "无人机智能光伏运维利用无人机搭载可见光和热成像图片采集设备，通过标注、训练的缺陷" +
          "模型来识别光伏组件的可见光和热成像的缺陷并生成缺陷报告和光伏地图等相关信息。",
        "projectNum": 3,
        "imageNum": 2020,
        "defectNum": 103
      },
      "Fan": {
        "logo": fan,
        "title": '无人机智能风机运维',
        "image": fan_image,
        "url": '/fan',
        "description": "无人机智能光伏运维利用无人机搭载可见光和热成像图片采集设备，通过标注、训练的缺陷" +
          "模型来识别风机组件的可见光和热成像的缺陷并生成缺陷报告和缺陷修复建议等相关信息。",
        "projectNum": 2,
        "imageNum": 2020,
        "defectNum": 103
      }
    }
  }

  componentWillMount() {
    this.props.getProjectTotal()
      // .then(ret => console.log(ret))
  }

  render() {
    const {navigation, PV, Fan} = this.state;
    return (
      <div className={'projects'} >
        <ProjectHeader data={navigation}/>
        <div className={'content'}>
          <Row className={'project-list'} justify="space-around" gutter={[32, 16]}>
            <Col span={12}>
              <CellBlock data={PV}/>
            </Col>
            <Col span={12}>
              <CellBlock data={Fan}/>
            </Col>
          </Row>
        </div>
        <Copyright/>
      </div>
    )
  }
}

export default connect(
  null, {getProjectTotal})(Projects)