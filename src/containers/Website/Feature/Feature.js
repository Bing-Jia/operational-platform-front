/**
 * 首页特征图
 * */
import {Row} from "antd";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import React, {Component} from "react";

import FeatureUnit from "src/components/Unit/FeatureUnit/FeatureUnit";

class Feature extends Component {

  render() {
    const {featureList} = this.props;
    return (
      <Row justify="space-around" style={{position: 'relative', top: '-60px'}}>
        {featureList.map((data, index) =>
          <FeatureUnit key={index} data={data}/>
        )}
      </Row>
    )
  }
}

Feature.propTypes = {
  featureList: PropTypes.array
};

const mapStateToProps = (state) => {
  return ({
    featureList: state.domainData.featureList
  })
};


export default connect(mapStateToProps, null)(Feature)
