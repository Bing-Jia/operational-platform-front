/**
 * @File   : Features.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/2
 * @Desc   : 官网特征组件
 **/

import PropTypes from "prop-types";
import {connect} from "react-redux";
import React, {Component} from "react";

import FeaturesUnit from "src/components/Unit/FeaturesUnit/FeaturesUnit";


class Features extends Component {

  render() {
    const {featuresData} = this.props;
    return (
      <FeaturesUnit data={featuresData}/>
    )
  }
}

Features.propTypes = {
  featuresData: PropTypes.object
};

const mapStateToProps = (state) => {
  return ({
    featuresData: state.domainData.featuresData
  })
};



export default connect(mapStateToProps, null)(Features)