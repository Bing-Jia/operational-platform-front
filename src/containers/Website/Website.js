/**
 * @File   : Website.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/4
 * @Desc   : 官网页面
 **/

import React, {Component} from "react";

import WebsiteHeader from "src/components/Header/WebsiteHeader/WebsiteHeader";

import News from "./News/News";
import Lease from "./Lease/Lease";
import Teams from "./Teams/Teams";
import Banner from "./Banner/Banner";
import Results from "./Results/Results";
import Feature from "./Feature/Feature";
import Features from "./Features/Features";
import Equipment from "./Equipment/Equipment";
import HomeBottom from "./HomeBottom/HomeBottom";

export default class Website extends Component {

  render() {
    return (
      <div className={'website-page'}>
          <WebsiteHeader/>
          <Banner key={'website-scroll'}/>
          <Feature key={'website-feature'}/>
          <Equipment key={'website-equipment'}/>
          <Features key={'website-features'}/>
          <Teams key={'website-teams'}/>
          <Results key={'website-results'}/>
          <Lease key={'website-lease'}/>
          <News key={'website-news'}/>
          <HomeBottom key={'website-bottom'}/>
          {/*<Gojs/>*/}
      </div>
    )
  }
}
