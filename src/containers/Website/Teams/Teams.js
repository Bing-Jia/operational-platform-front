/**
 * @File   : Teams.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/2
 * @Desc   : 官网团队组件
 **/

import PropTypes from "prop-types";
import {connect} from "react-redux";
import React, {Component} from "react";

import TeamUnit from "src/components/Unit/TeamUnit/TeamUnit";


class Teams extends Component {

  render() {
    const {teamsData} = this.props;
    return (
      <TeamUnit data={teamsData}/>
    )
  }
}

Teams.propTypes = {
  teamsData: PropTypes.object
};

const mapStateToProps = (state) => {
  return ({
    teamsData: state.domainData.teamsData
  })
};

export default connect(mapStateToProps, null)(Teams)