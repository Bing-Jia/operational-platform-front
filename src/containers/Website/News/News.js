/**
 * @File   : News.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/2
 * @Desc   : 官网 资讯
 **/

import PropTypes from "prop-types";
import {connect} from "react-redux";
import React, {Component} from "react";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

import NewUnit from "src/components/Unit/NewUnit/NewUnit";


class News extends Component {

  render() {
    const {newsData} = this.props;
    return (
      <NewUnit data={newsData}/>
    )
  }
}

const mapToStateProps = (state) => {
  return ({
    newsData: state.domainData.newsData
  })
};

News.pripsTypes = {
  newsData: PropTypes.object
};

export default connect(mapToStateProps, null)(News)