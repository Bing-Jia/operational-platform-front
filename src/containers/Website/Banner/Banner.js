/**
 * @File   : Website.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/4
 * @Desc   : 官网轮播图
 **/

import {Carousel} from "antd";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import React, {Component} from "react";

import BannerUnit from "src/components/Unit/BannerUnit/BannerUnit";


class Banner extends Component {

  render() {
    const {bannerList} = this.props;
    return (
      <Carousel className={'scroll-image'} autoplay dotPosition={'right'}>
        {
          bannerList.map((data, index) => <BannerUnit key={index} data={data}/>)
        }
      </Carousel>
    )
  }
}

Banner.propTypes = {
  bannerList: PropTypes.array,
};

const mapStateToProps = (state) => {
  return ({
    bannerList: state.domainData.bannerList
  })
};

export default connect(mapStateToProps, null)(Banner)



