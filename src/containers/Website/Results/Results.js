/**
 * @File   : Results.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/2
 * @Desc   : 官网 项目成果展示
 **/

import PropTypes from "prop-types";
import {connect} from "react-redux";
import React, {Component} from "react";

import ResultUnit from "src/components/Unit/ResultUnit/ResultUnit";


class Results extends Component {

  render() {
    const {resultsData} = this.props;
    return (
      <ResultUnit data={resultsData}/>
    )
  }
}

Results.propTypes = {
  resultsData: PropTypes.object
};

const mapStateToProps = (state) => {
  return ({
    resultsData: state.domainData.resultsData
  })
};


export default connect(mapStateToProps, null)(Results)

