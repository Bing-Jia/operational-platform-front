/**
 * @File   : HomeBottom.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/2
 * @Desc   : 官网 底部联系
 **/

import {Row, Col} from "antd";
import React, {Component} from 'react'
import intl from 'react-intl-universal';
import {NavLink} from "react-router-dom";

import qq from '../../../assets/images/footer/qq.png'
import wx from '../../../assets/images/footer/微信.png'
import wb from '../../../assets/images/footer/微博.png'
import logo from '../../../assets/images/logo/logo.png'
import Copyright from '../../../components/Copyright/Copyright'


export default class HomeBottom extends Component {

  render() {
    return (
      <div className={'website_bottom'} style={{backgroundColor: '#2a2a54'}}>
        <Row justify="space-around" align="middle" style={{height: '260px'}}>
          <Col span={4}>
            <img src={logo} alt="" style={{width: '100px', height: '100px'}}/>
          </Col>
          <Col span={12}>
            <Row justify="space-around" align="top" className='footer_info'>
              <Col span={6}>
                <h1>{intl.get("company")}</h1>
                <h4>{intl.get("about")}</h4>
                <h4>{intl.get("information")}</h4>
                <h4>{intl.get("cooperation")}</h4>
              </Col>
              <Col span={6}>
                <h1>{intl.get("contact")}</h1>
                <h4>{intl.get("email")}</h4>
                <h4>{intl.get("blog")}</h4>
                <h4>{intl.get("tel")} 021-686868</h4>
              </Col>
              <Col span={6}>
                <h1>{intl.get("service")}</h1>
                <h4>{intl.get("pv")}</h4>
                <h4>{intl.get("fan")}</h4>
                <h4>{intl.get("new energy")}</h4>
              </Col>
            </Row>
          </Col>
          <Col span={4}>
            <h2 style={{color: 'cadetblue'}}>{intl.get("platformName")}</h2>
            <div className="social-icons" style={{textAlign: 'center'}}>
              <NavLink to="#">
                <img src={qq} alt="" style={{height: "20px", width: "20px", marginLeft: '10px'}}/></NavLink>
              <NavLink to="#">
                <img src={wx} alt="" style={{height: "20px", width: "20px", marginLeft: '10px'}}/></NavLink>
              <NavLink to="#">
                <img src={wb} alt="" style={{height: "20px", width: "20px", marginLeft: '10px'}}/></NavLink>
            </div>
          </Col>
          <Copyright/>
        </Row>
      </div>
    )
  }
}