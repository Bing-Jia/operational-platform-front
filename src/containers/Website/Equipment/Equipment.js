/**
 * 官网设备
 * */

import PropTypes from "prop-types";
import {connect} from "react-redux";
import React, {Component} from "react";

import EquipmentUnit from "src/components/Unit/EquipmentUnit/EquipmentUnit";


class Equipment extends Component {

  render() {
    const {equipmentData} = this.props;
    return (
      <EquipmentUnit data={equipmentData}/>
    )
  }
}

Equipment.propTypes = {
  equipmentData: PropTypes.object
};

const mapStateToProps = (state) => {
  return ({
    equipmentData: state.domainData.equipmentData
  })
};


export default connect(mapStateToProps, null)(Equipment)
