/**
 * @File   : Lease.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/2
 * @Desc   : 官网租赁
 **/

import PropTypes from "prop-types";
import {connect} from "react-redux";
import React, {Component} from "react";


import LeaseUnit from "src/components/Unit/LeaseUnit/LeaseUnit";

class Lease extends Component {

  render() {
    const {leasesData} = this.props;
    return (
      <LeaseUnit data={leasesData}/>
    )
  }
}

Lease.propsTypes = {
  leasesData: PropTypes.object
};


const mapStateToProps = (state) => {
  return ({
    leasesData: state.domainData.leasesData
  })
};


export default connect(mapStateToProps, null)(Lease)