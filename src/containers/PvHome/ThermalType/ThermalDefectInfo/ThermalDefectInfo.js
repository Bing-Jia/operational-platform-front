/**
 * @File   : ThermalDefectInfo.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/2
 * @Desc   : 热成像缺陷信息
 **/

import React, {Component} from 'react';

import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";
import DynamicTabs from "src/components/Tabs/DynamicTabs/DynamicTabs";

class ThermalDefectInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return (
      <div style={{backgroundColor: 'white'}}>
        <UnitTitle mainTitle={'热成像'} subTitle={'缺陷情况'}/>
        <DynamicTabs/>
      </div>
    )
  }
}

export default ThermalDefectInfo