/**
 * @File   : ThermalType.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/7
 * @Desc   : 热成像缺陷类型
 **/

import {Row, Col} from "antd";
import React, {Component} from "react";

import ThermalDefectBar from "./ThermalDefectBar/ThermalDefectBar";
import ThermalDefectInfo from "./ThermalDefectInfo/ThermalDefectInfo";
import ThermalDefectTable from "./ThermalDefectTable/ThermalDefectTable";

import "./ThermalType.sass";

class ThermalType extends Component {

  render() {
    return (
      <Row className={'pv-thermal-type'} justify="space-between" gutter={[8, 0]}>
        <Col className={'unit'} span={8}>
          <ThermalDefectTable/>
        </Col>
        <Col className={'unit'} span={8}>
          <ThermalDefectBar/>
        </Col>
        <Col className={'unit'} span={8}>
          <ThermalDefectInfo/>
        </Col>
      </Row>
    )
  }
}

export default ThermalType