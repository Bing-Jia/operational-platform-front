/**
 * @File   : ThermalDefectBar.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/2
 * @Desc   : 热成像缺陷类型 表格
 **/

import React, {Component} from "react";

import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";
import EchartMixture from "src/components/Echart/EchartMixture/EchartMixture";

class ThermalDefectBar extends Component {
  constructor(props){
    super(props);
    this.state ={}
  }
  render() {
    return (
      <div style={{backgroundColor:'white'}}>
        <UnitTitle mainTitle={'热成像'} subTitle={'缺陷图标'}/>
        <EchartMixture />
      </div>
    )
  }
}

export default ThermalDefectBar