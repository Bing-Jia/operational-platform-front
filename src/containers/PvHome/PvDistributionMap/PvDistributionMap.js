/**
 * @File   : PvDistributionMap.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/7
 * @Desc   : 光伏分布地图
 **/

import {Col, Row} from "antd";
import {connect} from "react-redux";
import React, {Component} from "react";

import {getProjectsByType} from "src/api/request";
import BaiduMap from "src/components/Map/BaiduMap/BaiduMap";
import SrollBar from "src/components/Scroll/ScrollBar/ScrollBar";
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";

import "./PvDistributionMap.sass"


class PvDistributionMap extends Component {

  render() {
    return (
      <div className='pv-home-map'>
        <UnitTitle mainTitle={'光伏'} subTitle={'项目分布'}/>
        <Row justify='space-around' gutter={8} className='project-map'>
          <Col span={14} className='map'>
            <BaiduMap/>
          </Col>
          <Col span={8} className='project-list' style={{textAlign: 'center'}}>
            <SrollBar data={this.props.projects.results}/>
          </Col>
        </Row>
      </div>
    )
  }
}


export default connect(
  state => (
    {
      projects: state.domainData.projects,
      dataInfo: state.domainData.dataList,
      userInfo: state.domainData.userList,
      defectInfo: state.domainData.defectList,
      defectTypeInfo: state.domainData.defectTypeList,
    }
  ),
  {getProjectsByType}
)(PvDistributionMap)

