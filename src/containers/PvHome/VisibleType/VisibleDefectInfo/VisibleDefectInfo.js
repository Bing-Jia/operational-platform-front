/**
 * @File   : VisibleDefectInfo.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/2
 * @Desc   : 可见光缺陷类型修复组件
 **/

import React, {Component} from 'react'
import CollapseContent from "src/components/Collapse/CollapseContent/CollapseContent";
import UnitTitle from "../../../../components/Title/UnitTitle/UnitTitle";

class VisibleDefectInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return (
      <div style={{backgroundColor: 'white'}}>
        <UnitTitle mainTitle={'建议'} subTitle={'修复建议'}/>
        <CollapseContent/>
      </div>
    )
  }
}

export default VisibleDefectInfo