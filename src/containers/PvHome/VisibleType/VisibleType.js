/**
 * @File   : VisibleType.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/7
 * @Desc   : 可见光缺陷类型
 **/

import React, {Component} from 'react';
import {Row, Col, Timeline} from "antd";

import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";

import RepairVideo from "./RepairVideo/RepairVideo";
import VisibleDefectBar from "./VisibleDefectBar/VisibleDefectBar";
import VisibleDefectInfo from "./VisibleDefectInfo/VisibleDefectInfo";
import VisibleDefectTable from './VisibleDefectTable/VisibleDefectTable';

import "./VisibleType.sass";


class VisibleType extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return (
      <Row className={'pv-visible-type'} justify="space-between" gutter={[8, 0]}>
        <Col className={'type-content'} span={16}>
          <Row className={'visible-info'} justify="space-between">
            <Col span={24}>
              <UnitTitle mainTitle={'可见光'} subTitle={'缺陷列表'}/>
              <Row>
                <Col span={12}>
                  <VisibleDefectTable title={false}/>
                </Col>
                <Col span={12}>
                  <VisibleDefectBar/>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className={'repair-info'} justify="space-between">
            <Col span={12}>
              <VisibleDefectInfo/>
            </Col>
            <Col span={12}>
              <RepairVideo/>
            </Col>
          </Row>
        </Col>
        <Col className={'timeline-content'} span={8}>
          <div className={'content'}>
            <UnitTitle mainTitle={'时间线'} subTitle={'项目'}/>
            <Timeline className={'time-line'}>
              <Timeline.Item>
                <h4>
                  创建项目
                </h4>
                <span>
                  2020-09-01&nbsp;&nbsp;by 张三
                </span>
                <p>
                  项目所在地：西宁
                </p>
              </Timeline.Item>
              <Timeline.Item>
                <h4>
                  创建项目
                </h4>
                <span>
                  2020-09-11&nbsp;&nbsp;by 李四
                </span>
                <p>
                  项目所在地：南京
                </p>
              </Timeline.Item>
              <Timeline.Item>
                <h4>
                  创建项目
                </h4>
                <span>
                  2020-10-22&nbsp;&nbsp;by 洪飞
                </span>
                <p>
                  项目所在地：上海
                </p>
              </Timeline.Item>
            </Timeline>
          </div>
        </Col>
      </Row>
    )
  }
}

export default VisibleType