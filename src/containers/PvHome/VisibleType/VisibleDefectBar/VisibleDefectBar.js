/**
 * @File   : VisibleDefectBar.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/2
 * @Desc   : 可见光缺陷类型 表格
 **/

import React, {Component} from 'react'
import EchartBar from "src/components/Echart/EchartBar/EchartBar";

class VisibleDefectBar extends Component {
  constructor(props){
    super(props);
    this.state ={}
  }
  render() {
    return (
      <div>
        <EchartBar/>
      </div>
    )
  }
}

export default VisibleDefectBar