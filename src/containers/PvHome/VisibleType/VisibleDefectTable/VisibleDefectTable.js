/**
 * @File   : VisibleDefectTable.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/2
 * @Desc   : 可见光缺陷类型图表
 **/

import React, {Component} from 'react';
import {connect} from "react-redux";

import {getAdminDefectType} from "src/api/request";
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";
import TableWithClick from "src/components/Table/TableWithClick/TableWithClick";


class VisibleDefectTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      requestStatus: false
    }
  }

  componentDidMount() {
    this.props.getAdminDefectType()
      .then(
        this.setState({
          requestStatus: true
        })
      );
  }


  render() {
    const {defectTypeInfo, title} = this.props;
    const columns = [
      {
        title: '序号',
        width: 80,
        dataIndex: 'id'
      }, {
        title: '缺陷',
        dataIndex: 'name'
      }, {
        title: '类型',
        dataIndex: 'type'
      }, {
        title: '操作',
        width: 80,
        fixed: 'right',
        dataIndex: 'description',
        render:
          (text) =>
            <a title={text}>描述</a>
      }
    ];
    return (
      <div style={{backgroundColor: 'white', height:'100%'}}>
        {title ? <UnitTitle mainTitle={'热成像'} subTitle={'缺陷列表'}/> : null}
        <TableWithClick columns={columns} data={defectTypeInfo.results}/>
      </div>
    )
  }
}

export default connect(
  state => ({
    defectTypeInfo: state.domainData.defectTypeList,
  }),
  {getAdminDefectType})
(VisibleDefectTable)