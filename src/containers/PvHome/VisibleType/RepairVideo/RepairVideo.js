/**
 * @File   : RepairVideo.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/2
 * @Desc   : 修复视频组件
 **/

import React, {Component} from 'react';
import {Player} from 'video-react';

import 'video-react/dist/video-react.css';
import videoImage from "src/assets/images/banner/pv.jpg";
import repairVideo from "src/assets/video/repairVideo.mp4";
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";


class RepairVideo extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return (
      <div style={{height: '100%', backgroundColor: 'white'}}>
        <UnitTitle mainTitle={'视频'} subTitle={'修复视频'}/>
        <div style={{margin: '10px 2% 0px 2%', width: '96%', position: 'relative'}}>
          <Player ref="player" videoId="video-1" poster={videoImage}>
            <source src={repairVideo}/>
          </Player>
        </div>
      </div>
    )
  }
}

export default RepairVideo