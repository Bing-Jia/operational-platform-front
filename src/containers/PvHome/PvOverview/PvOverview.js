/**
 * @File   : PvOverview.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/4
 * @Desc   : 光伏总览
 **/

import {Row, Col} from "antd";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import React, {Component} from "react";

import {
  UserOutlined, AlignCenterOutlined,
  AreaChartOutlined, BarChartOutlined, FundOutlined
} from '@ant-design/icons';

import InfoShowUnit from "src/components/Unit/InfoShowUnit/InfoShowUnit";


import "./PvOverview.sass";


class PvOverview extends Component {

  render() {
    const {
      dataInfo, userInfo, defectInfo, projects, defectTypeInfo
    } = this.props;
    return (
      <div className={'pv-home-overview'}>
        <Row className={'overview-content'} justify="space-around" align="middle">
          <Col span={4}>
            <InfoShowUnit
              componentTitle={() => <UserOutlined/>}
              title='用户数'
              number={userInfo.total}
              radio={userInfo.radio}
              lift={true}
            />
          </Col>

          <Col span={4}>
            <InfoShowUnit
              componentTitle={() => <AlignCenterOutlined/>}
              title='项目数'
              number={projects.count}
              radio={projects.radio}
              lift={true}
            />
          </Col>

          <Col span={4}>
            <InfoShowUnit
              componentTitle={() => <FundOutlined/>}
              title='可见光图片数'
              title_icon_color='rgb(26, 187, 156)'
              number={dataInfo.total}
              radio={dataInfo.radio}
              color='#1ABB9C'
              lift={true}
            />
          </Col>

          <Col span={4}>
            <InfoShowUnit
              componentTitle={() => <FundOutlined/>}
              title='热成像图片数'
              title_icon_color='orangered'
              number={dataInfo.total}
              radio={dataInfo.radio}
              color='orangered'
              lift={true}
            />
          </Col>

          <Col span={4}>
            <InfoShowUnit
              componentTitle={() => <BarChartOutlined/>}
              title='缺陷类型数'
              number={defectTypeInfo.count}
              radio={defectTypeInfo.radio}
              lift={false}
            />
          </Col>

          <Col span={4}>
            <InfoShowUnit
              componentTitle={() => <AreaChartOutlined/>}
              title='预警缺陷数'
              number={defectInfo.total}
              radio={defectInfo.radio}
              last={true}
              lift={true}
            />
          </Col>
        </Row>
      </div>
    )
  }
}

const mapToStateProps = (state) => {
  return (
    {
      dataInfo: state.domainData.dataList,
      userInfo: state.domainData.userList,
      defectInfo: state.domainData.defectList,
      projects: state.domainData.projects,
      defectTypeInfo: state.domainData.defectTypeList,
    }
  )
};

PvOverview.propTypes = {
  dataInfo: PropTypes.object,
  userInfo: PropTypes.object,
  defectInfo: PropTypes.object,
  projectInfo: PropTypes.object,
  defectTypeInfo: PropTypes.object,
};


export default connect(mapToStateProps, null)(PvOverview)

