/**
 * @File   : PvHome.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/4
 * @Desc   : 光伏项目首页
 **/

import {Spin, Space} from "antd";
import {connect} from "react-redux";
import React, {Component, Fragment} from "react";

import {getProjectsByType} from "src/api/request";
import logo from "src/assets/images/logo/logo.png";
import Copyright from "src/components/Copyright/Copyright";
import ProjectHeader from "src/components/Header/ProjectHeader/ProjectHeader";

import PvOverView from "./PvOverview/PvOverview";
import ThermalType from "./ThermalType/ThermalType";
import VisibleType from "./VisibleType/VisibleType";
import PvDistributionMap from "./PvDistributionMap/PvDistributionMap";

import "./PvHome.sass";


class PvHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      "navigation": {
        "logo": logo,
        "title": '无人机光伏运维',
        "navList": [
          {"name": "首页", "url": "/"},
          {"name": "后台", "url": "/backstage"},
          {"name": "项目", "url": "/pv/projects/1"},
        ]
      },
      "requestStatus": false,
      "project": {}
    }
  };

  componentDidMount() {
    this.props.getProjectsByType('1')
      .then(
        this.setState({
          requestStatus: true
        })
      );
  }

  render() {
    const {navigation, requestStatus} = this.state;
    return (
      <Fragment>
        {requestStatus ?
          (
            <div className={'pv-home'}>
              <ProjectHeader data={navigation}/>
              <Space className={'space'} direction="vertical" size={8}>
                <PvOverView/>
                <PvDistributionMap/>
                <ThermalType/>
                <VisibleType/>
              </Space>
              <Copyright/>
            </div>
          ) : <Spin/>
        }
      </Fragment>
    )
  }
}

export default connect(
  state => ({
    projects: state.domainData.projects,
    dataInfo: state.domainData.dataList,
    userInfo: state.domainData.userList,
    defectInfo: state.domainData.defectList,
    defectTypeInfo: state.domainData.defectTypeList,
  }),
  {getProjectsByType})(PvHome)