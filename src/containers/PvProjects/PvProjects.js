/**
 * 光伏项目列表
 * */

import {Spin, Space} from "antd";
import React, {Component, Fragment} from "react";

import logo from "src/assets/images/logo/logo.png";
import Copyright from "src/components/Copyright/Copyright";
import ProjectHeader from "src/components/Header/ProjectHeader/ProjectHeader";

import OverView from "./Overview/Overview";
import DefectInfo from "./DefectInfo/DefectInfo";
import DetailsTable from "./DetailsTable/DetailsTable";
import CreateProject from "./CreateProject/CreateProject";

import "./PvProjects.sass";

export default class PvProjects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      requestStatus: false,
      navigation: {
        logo: logo,
        title: '无人机光伏运维',
        navList: [
          {name: "首页", url: "/"},
          {name: "后台", url: "/backstage"},
        ]
      },
    }
  }

  componentDidMount() {
    this.setState({
      requestStatus: true
    })
  }

  render() {
    const {requestStatus, navigation} = this.state;
    const {auth_id} = this.props.match.params;
    return (
      <Fragment>
        {
          requestStatus ?
            (
              <div className={'pv-projects'}>
                <ProjectHeader data={navigation}/>
                <Space className={'space'} direction="vertical"  size={8} >
                  <OverView/>
                  <DetailsTable auth={auth_id}/>
                  <DefectInfo/>
                  <CreateProject/>
                </Space>
                <Copyright/>
              </div>
            ) :
            <Spin/>
        }
      </Fragment>
    )
  }
}