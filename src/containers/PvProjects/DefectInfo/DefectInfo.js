/**
 * @File   : DefectInfo.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/7
 * @Desc   : 项目组缺陷统计信息
 **/

import {Row, Col, Tabs} from "antd";
import React, {Component} from "react";
import {LineChartOutlined, BarChartOutlined} from "@ant-design/icons";

import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";
import EchartBar from "src/components/Echart/EchartBar/EchartBar";
import EchartMixture from "src/components/Echart/EchartMixture/EchartMixture";

import "./DefectInfo.sass";

const {TabPane} = Tabs;

class DefectInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return (
      <div className={'defect-info'}>
        <UnitTitle mainTitle={'项目统计'} subTitle={'图表'}/>
        <Tabs className={'tabs'} defaultActiveKey={'1'} centered>
          <TabPane  tab={<span> <LineChartOutlined className={'visible-icon'} /> 可见光 </span>} key="1">
            <Row align={'space-around'}>
              <Col span={15}>
                <EchartMixture text='缺陷统计'/>
              </Col>
              <Col span={8} >
                <EchartBar text='缺陷占比'/>
              </Col>
            </Row>
          </TabPane>
          <TabPane  tab={<span> <BarChartOutlined className={'thermal-icon'} /> 热成像 </span>} key="2">
            <Row align={'space-around'}>
              <Col span={15}>
                <EchartMixture text='缺陷统计' sub_text='热成像'/>
              </Col>
              <Col span={8} offset={1}>
                <EchartBar text='缺陷占比'/>
              </Col>
            </Row>
          </TabPane>
        </Tabs>
      </div>
    )
  }
}

export default DefectInfo