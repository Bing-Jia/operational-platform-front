/**
 * @File   : DetailsTable.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/2
 * @Desc   : 项目详情表格
 **/

import {Popover} from "antd";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {NavLink} from "react-router-dom";
import {Progress, Table, Button, Spin} from "antd";
import React, {Component, Fragment} from "react";


import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";
import WebUpload from "src/components/Upload/WebUpload/WebUpload";
import {getProjectsByType, checkDefect, getProjectProgress} from "src/api/request";


import "./DetailsTable.sass";


class DetailsTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projectList: [],
      checkProject: null,
      timerId: null,
    };
  }

  componentWillMount() {
    this.props.getProjectsByType('1')
      .then(
        ret => {
          this.setState({
              status: true,
              projectList: ret.projectsData.results
            }
          )
        }
      );
  }


  getProgress = (id) => {
    this.props.getProjectProgress(id).then(
      ret => {
        // 更新分析进度信息
        this.setState({
          projectList: this.state.projectList.map(
            (project) => project.id === id ?
              {
                ...project,
                analysis_status: ret.data.analysis_status,
                analysis_progress: ret.data.analysis_progress,
                view_status: ret.data.view_status
              } : project
          )
        });

        //关闭定时器
        if (ret.data.view_status === '1') {
          clearInterval(this.state.timerId);
        }
      }
    )
  };

  startCheck = (id) => {
    // 启动定时器
    this.state.timerId = setInterval(
      () => this.getProgress(id),
      1000
    );

    // 添加检测项目id
    this.setState({
      ...this.state,
      checkProject: id
    });

    // 开始检测
    this.props.checkDefect({
      project_id: id,
      check_type: 'thermal'
    })
  };

  render() {
    const columns = [
      {
        title: '序号',
        width: '100px',
        dataIndex: 'id',
        textAlign: 'center',
        render: (text, record, index) => `${index + 1}`,
      },
      {
        title: '项目名称',
        dataIndex: 'name',
        textAlign: 'center',
        render: (text, record) => <NavLink to={`/pv/project/${this.props.auth}/${record.id}`}>{text}</NavLink>
      },
      {
        title: '所属公司',
        dataIndex: 'company',
        textAlign: 'center',
      },
      {
        title: '项目联系人',
        dataIndex: 'owner',
        textAlign: 'center',
      },
      {
        title: '项目时间',
        textAlign: 'center',
        dataIndex: 'operation_date',
        render: (text) => text.split("T")[0]
      },
      {
        key: 'status',
        title: '状态',
        textAlign: 'center',
        children: [
          {
            width: '120px',
            title: '图片状态',
            textAlign: 'center',
            dataIndex: 'image_status',
            render: (text, record) =>
              text === '0' ?
                <Popover placement="left" content={<WebUpload project_id={record.id}/>}>
                  <NavLink to="#" style={{color: 'orangered'}}>上传图片</NavLink>
                </Popover>
                :
                <span style={{color: '#1ABB9C'}}>已上传</span>
          },
          {
            width: '220px',
            title: '分析进度',
            textAlign: 'center',
            dataIndex: 'analysis_progress',
            render: (text) =>
              <Progress
                strokeColor={{
                  from: '#108ee9',
                  to: '#87d068',
                }}
                percent={text}
                status="active"
                style={{width: "100px"}}
              />
          },
          {
            width: '120px',
            title: '检测状态',
            textAlign: 'center',
            dataIndex: 'analysis_status',
            render: ((text, record) => {
                if (text === '0') {
                  return <Button danger type={'primary'} onClick={() => this.startCheck(record.id)}>点击分析 </Button>
                } else if (text === '1' && record.analysis_progress === 100) {
                  return (
                    <span style={{color: 'red'}}>生成报告...</span>
                  )
                } else if (text === '1' && record.analysis_progress < 100) {
                  return (
                    <span style={{color: '#1ABB9C'}}>分析中...</span>
                  )
                } else {
                  return <span style={{color: '#1a3a95'}}>分析完成</span>
                }
              }
            )
          }
        ]
      }
    ];
    const {projectList} = this.state;
    return (
      <div className={'details-table'}>
        <UnitTitle mainTitle={'项目信息'} subTitle={'列表'}/>
        <Table columns={columns} dataSource={projectList.length === 0 ? null : projectList}/>
      </div>
    )
  }
}

DetailsTable.propTypes = {
  projects: PropTypes.object,
};

export default connect(
  state => ({
    projects: state.domainData.projects,
  }),
  {getProjectsByType, checkDefect, getProjectProgress}
)(DetailsTable)