/**
 * @File   : ProjectData.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/17
 * @Desc   : 项目数据
 **/

import React, {Component} from "react";
import {Popconfirm, Table} from "antd";
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";
import WebUpload from "src/components/Upload/WebUpload/WebUpload";

import "./ProjectData.sass";


class ProjectData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [
        {
          key: '1',
          index: '1',
          dataFrom: '上电漕泾光伏发电有限公司',
          dataSize: '50000张',
          dataType: '热成像、可见光',
          collectDate: '2020-02-21',
        }
      ],
      columns: [
        {
          title: '序号',
          align: 'center',
          dataIndex: 'index',
        },
        {
          align: 'center',
          title: '数据来源',
          dataIndex: 'dataFrom',
        },
        {
          align: 'center',
          title: '数据大小',
          dataIndex: 'dataSize',
        },
        {
          align: 'center',
          title: '数据类型',
          dataIndex: 'dataType',
        },
        {
          align: 'center',
          title: '采集时间',
          dataIndex: 'collectDate',
        },
        {
          title: '操作',
          align: 'center',
          dataIndex: 'operation',
          render: (text, record) =>
            this.state.dataSource.length >= 1 ? (
              <Popconfirm title="删除该条目?" onConfirm={() => this.deleteItem(record.key)}>
                <a style={{color: 'red'}}>删除</a>
              </Popconfirm>
            ) : null,
        },
      ],
    }
  }

  render() {
    const {dataSource, columns} = this.state;
    const {project_id} = this.props.match.params;
    return (
      <div className={'pv-project-data'}>
        <UnitTitle mainTitle={'项目'} subTitle={'项目数据'}/>
        <Table dataSource={dataSource} columns={columns} style={{padding: '20px'}} bordered/>
        <WebUpload project_id={project_id}/>
      </div>
    )
  }
}

export default ProjectData