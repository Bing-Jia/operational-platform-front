/**
 * @File   : ProjectTeam.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/17
 * @Desc   : 项目团队
 **/

import React, {Component} from "react";
import {Row, Col, Divider, Switch, Select, Modal, notification, Rate, Button} from "antd";
import {DeleteOutlined, ExclamationCircleOutlined, UserAddOutlined, CrownTwoTone} from "@ant-design/icons";

import Qiu from "src/assets/images/teams/qiu.jpg";
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";

import "./ProjectTeam.sass";

const {Option} = Select;
const {confirm} = Modal;

class ProjectTeam extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userList: [
        {
          fullName: '薛晓冰',
          email: 'xuexb@sepd.com.cn',
          userAvatar: Qiu,
          isAdmin: true,
          role: '产品所有者',
          state: true,
        },
        {
          fullName: '李小明',
          email: 'lixm@sepd.com.cn',
          userAvatar: Qiu,
          isAdmin: false,
          role: '设计',
          state: false,
        }
      ],
      roleList: ['前端', '后端', '管理员', '设计', '产品所有者']
    }
  }

  addUser = () => {
    this.setState({
      userList: [...this.state.userList, {
        fullName: '王雪丽',
        email: 'wangxl@sepd.com.cn',
        userAvatar: Qiu,
        isAdmin: false,
        role: '前端',
        state: false,
      }],
    })
  };

  deleteUser = (index) => {
    let that = this;
    confirm({
      title: '删除该用户?',
      icon: <ExclamationCircleOutlined/>,
      content: '该用户将会从你的项目成员中删除！',
      okText: '确认',
      okType: 'danger',
      cancelText: '取消',
      onOk() {
        that.setState({
          userList: that.state.userList.filter((_, i) => i !== index)
        });
        that.openNotificationWithIcon('success')
      },
      onCancel() {
        console.log('Cancel');
      },
    })
  };

  openNotificationWithIcon = type => {
    notification[type]({
      message: '操作成功',
      description: "成功的将用户从该项目成员列表删除"
    });
  };

  changeToAdmin = (index) => {
    const userList = [...this.state.userList]
    this.setState({
      userList: userList.map((item, key) => key === index ? {...item, isAdmin: !item.isAdmin} : item)
    })
  };

  render() {
    const {userList, roleList} = this.state;
    return (
      <div className={'pv-project-team'}>
        <UnitTitle mainTitle={'项目'} subTitle={'项目成员'}/>
        <Row className={'add-btn'}>
          <Button type={"primary"} icon={<UserAddOutlined/>} onClick={this.addUser}>
            新成员
          </Button>
        </Row>
        <Row className={'user-list'}>
          <Row style={{width: '100%',}}>
            <Col span={8}><span>成员</span></Col>
            <Col span={3}><span>管理者</span></Col>
            <Col span={8}><span>角色</span></Col>
            <Col span={3}><span>活跃度</span></Col>
            <Col span={2}><span>操作</span></Col>
          </Row>
          {
            userList.map((item, index) => {
              return (
                <div className={'user-list-content'} key={index}>
                  <Divider/>
                  <Row key={index} align="middle">
                    <Col className={'user-avatar'} span={8}>
                      <Row>
                        <Col span={4}>
                          <img src={item.userAvatar} alt=""/>
                        </Col>
                        <Col span={20}>
                          <span>{item.fullName}</span>&nbsp;&nbsp;{item.isAdmin ? (<CrownTwoTone/>) : (<></>)}
                          <p>{item.email}</p>
                        </Col>
                      </Row>
                    </Col>
                    <Col span={3}>
                      <Switch checkedChildren="是" unCheckedChildren="否" defaultChecked={item.isAdmin}
                              onChange={this.changeToAdmin.bind(this, index)}/>
                    </Col>
                    <Col className={'user-role'} span={8}>
                      <Select defaultValue={`${item.role}`}>
                        {roleList.map((item_data, index_data) => {
                          return (
                            <Option value={item_data} key={index_data}>{item_data}</Option>
                          )
                        })}
                      </Select>
                    </Col>
                    <Col className={'user-active'} span={3}>
                      {item.state ?
                        (<Rate className={'rate'} allowHalf disabled count={3} defaultValue={5}/>) : (
                          <Rate className={'rate'} allowHalf disabled count={3} defaultValue={1.5}/>)}
                    </Col>
                    <Col className={'user-operation'} span={2}>
                      <DeleteOutlined className={'delete-icon'}  onClick={this.deleteUser.bind(this, index)}/>
                    </Col>
                  </Row>
                </div>
              )
            })
          }
        </Row>
      </div>
    )
  }
}


export default ProjectTeam