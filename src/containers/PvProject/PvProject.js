/**
 * @File   : DetailsTable.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/2
 * @Desc   : 项目信息页
 **/

import {Row, Col, Spin} from "antd";
import React, {Component} from "react";
import {Switch, Redirect, Route} from "react-router-dom"

import logo from "src/assets/images/logo/logo.png";
import ProjectMenu from "src/components/Menu/ProjectMenu/ProjectMenu";
import ManageHeader from "src/components/Header/ManageHeader/ManageHeader";
import ProjectReboot from "src/components/Reboot/ProjectReboot/ProjectReboot";

import ProjectMap from "./ProjectMap/ProjectMap";
import ProjectData from "./ProjectData/ProjectData";
import ProjectTeam from "./ProjectTeam/ProjectTeam";
import ProjectHome from "./ProjectHome/ProjectHome";
import ProjectReport from "./ProjectReport/ProjectReport";
import ProjectDefect from "./ProjectDefect/ProjectDefect";
import ProjectMessage from "./ProjectMessage/ProjectMessage";
import ProjectEquipment from "./ProjectEquipment/ProjectEquipment";
import ProjectPermission from "./ProjectPermission/ProjectPermission";

import "./PvProject.sass";


export default class PvProject extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: {
        collapsed: false,
        menuWidth: 3
      },
      number: 0,
      loadStatus: false,
    }
  };

  changeMenuWidth = () => {
    this.setState({
      collapsed: {
        collapsed: !this.state.collapsed.collapsed,
        menuWidth: !this.state.collapsed.collapsed ? 2 : 3
      }
    })
  };

  componentDidMount() {
    setTimeout(
      () => this.setState({
        loadStatus: !this.state.loadStatus
      }), 2000
    )
  }

  render() {
    const {collapsed} = this.state;
    const {auth_id, project_id} = this.props.match.params;
    return (

      <div className={'pv-project'}>
        <ManageHeader menuWidth={collapsed.menuWidth} logo={logo} projectName={'金山光伏项目一'}
                      changeMenuWidth={this.changeMenuWidth}/>
        <Row className={'pv-project-content'}>
          <Col className={'menu'} span={collapsed.menuWidth}>
            <ProjectMenu collapsed={collapsed.collapsed} auth_id={auth_id} project_id={project_id}/>
          </Col>
          <Col className={'content'} span={24 - collapsed.menuWidth}>
            <Switch>
              <Route exact path={'/pv/project/:auth_id/:project_id/home'} component={ProjectHome}/>
              <Route exact path={'/pv/project/:auth_id/:project_id/report'} component={ProjectReport}/>
              <Route exact path={'/pv/project/:auth_id/:project_id/defect'} component={ProjectDefect}/>
              <Route exact path={'/pv/project/:auth_id/:project_id/map'} component={ProjectMap}/>
              <Route exact path={'/pv/project/:auth_id/:project_id/message'} component={ProjectMessage}/>
              <Route exact path={'/pv/project/:auth_id/:project_id/equipment'} component={ProjectEquipment}/>
              <Route exact path={'/pv/project/:auth_id/:project_id/data'} component={ProjectData}/>
              <Route exact path={'/pv/project/:auth_id/:project_id/team'} component={ProjectTeam}/>
              <Route exact path={'/pv/project/:auth_id/:project_id/permission'} component={ProjectPermission}/>
              <Redirect to={'/pv/project/:auth_id/:project_id/home'} from={'/pv/project/:auth_id/:project_id'}/>
            </Switch>
          </Col>
          <Row>
            <ProjectReboot/>
          </Row>
        </Row>
      </div>
    )
  }
}