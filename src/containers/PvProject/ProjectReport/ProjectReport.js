/**
 * @File   : ProjectReport.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/16
 * @Desc   : 项目报告
 **/

import {connect} from "react-redux";
import React, {Component} from "react";
import {Row, Col, Divider} from "antd";

import {getProject} from "src/api/request"
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";


import "./ProjectReport.sass";

class ProjectReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pdf: null,
      operation_date: null,
    }
  }

  componentDidMount() {
    this.props.getProject(this.props.match.params.project_id)
      .then(
        ret => {
          console.log(ret)
          let operation_date = ret.data.operation_date.replace('T', ' ').replace('Z', '')
          this.setState({
            operation_date,
            pdf: (ret.data.report_path).replace('/media', '')
          })
        }
      )
      .catch(
        err => console.log('--------log', err)
      )
  }

  render() {
    const {project_id} = this.props.match.params;
    return (
      <div className={'pv-project-report'}>
        <UnitTitle mainTitle={'项目'} subTitle={'项目报告'}/>
        <Row className={'report-content'} justify="space-around" align="middle">
          <Col className={'report'} span={15}>
            <object data={this.state.pdf} type="application/pdf"/>
          </Col>
          <Col className={'description'} span={8}>
            <Row className={'description-content'}>
              <Row>
                <h4><strong>报告名称 :</strong></h4>
                <span>检测报告</span>
              </Row>
              <Divider/>
              <Row>
                <span><strong>生成时间 :</strong></span>
                <span>{this.state.operation_date}</span>
              </Row>
              <Divider/>
              <Row>
                <span><strong>模板类型 :</strong></span>
                <span>光伏报告类型模板</span>
              </Row>
              <Divider/>
              <Row>
                <h4><strong>报告摘要:</strong></h4>
                <textarea
                  defaultValue={
                    "该报告主要统计基于采集到的缺陷数据配合目标检测算法训练出的模型，进行光伏数据" +
                    "的检测识别，并统计其缺陷类型、占比、情况等信息。"
                  }
                />
              </Row>
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default connect(null, {getProject})(ProjectReport)