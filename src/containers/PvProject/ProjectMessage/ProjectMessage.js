/**
 * @File   : ProjectMessage.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/17
 * @Desc   : 项目信息
 **/

import {Tabs} from "antd";
import React, {Component} from 'react';

import TimelineComponent from "src/components/Timeline/Timeline";
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";
import ListWithAvatar from "src/components/List/ListWithAvatar/ListWithAvatar";

import "./ProjectMessage.sass";

const {TabPane} = Tabs;

class ProjectMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return (
      <div className={'pv-project-message'}>
        <UnitTitle mainTitle={'项目'} subTitle={'项目信息'}/>
        <Tabs className={'message-tabs'} type={'card'} defaultActiveKey="1">
          <TabPane tab="行业资讯" key="1">
            <ListWithAvatar/>
          </TabPane>
          <TabPane tab="系统消息" key="2">
            <TimelineComponent/>
          </TabPane>
        </Tabs>
      </div>
    )
  }
}

export default ProjectMessage