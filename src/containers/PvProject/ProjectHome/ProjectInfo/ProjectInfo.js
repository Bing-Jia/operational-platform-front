/**
 * @File   : ProjectInfo.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/15
 * @Desc   : 项目信息
 **/

import React, {Component} from "react";
import {Row, Col, Button, Divider} from "antd";

import {getProject} from "src/api/request"
import ProjectLogo from "src/assets/images/banner/pv.jpg"
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";
import ProjectMap from "src/components/Map/ProjectMap/ProjectMap";

import "./ProjectInfo.sass";
import {connect} from "react-redux";

class ProjectInfo extends Component {
  constructor(props){
    super(props);
    this.state ={
      name: '',
      company: '',
      operation_date: '',
      owner: '',
      description: '',
      location: '',
    }
  }

  componentDidMount() {
    console.log('---------xx', this.props.projectId)
    this.props.getProject(this.props.projectId)
      .then(
        ret => {
          console.log(ret)
          let operation_date = ret.data.operation_date.replace('T', ' ').replace('Z', '')
          this.setState({
            operation_date,
            name: ret.data.name,
            company: ret.data.company,
            owner: ret.data.owner,
            description: ret.data.description,
            location: ret.data.location,
          })
        }
      )
      .catch(
        err => console.log('--------log', err)
      )
  }

  render() {
    return (
      <div className={'pv-home-project-info'}>
          <UnitTitle mainTitle={'项目'} subTitle={'项目介绍'}/>
          <Row gutter={16} justify="space-around" align="middle">
            <Col className={'project-info'} span={15}>
              <Row className={'content'} gutter={16} align={"middle"}>
                <Col className={'content-project-icon'} span={8}>
                  <img src={ProjectLogo} alt="" />
                  <Button className={'btn-change-button'} type="primary" block >
                    更换图标
                  </Button>
                  <Button className={'btn-default-button'} type="danger" block >
                    使用默认图片
                  </Button>
                </Col>
                <Col span={16}>
                  <Row>
                    <h4><strong>项目名称:&nbsp;&nbsp;</strong></h4>
                    <span>{this.state.name}</span>
                  </Row>
                  <Divider/>
                  <Row>
                    <Col span={10}>
                      <span><strong>项目管理:&nbsp;&nbsp;</strong></span>
                      <span>{this.state.owner}</span>
                    </Col>
                    <Col span={14}>
                      <span><strong>所属公司:&nbsp;&nbsp;</strong></span>
                      <span>{this.state.company}</span>
                    </Col>
                  </Row>
                  <Divider/>
                  <Row>
                    <Col span={10}>
                      <span><strong>运营时间:&nbsp;&nbsp;</strong></span>
                      <span>{this.state.operation_date}</span>
                    </Col>
                    <Col span={14}>
                      <span><strong>项目地址:&nbsp;&nbsp;</strong></span>
                      <span>{this.state.location}</span>
                    </Col>
                  </Row>
                  <Divider/>
                  <div>
                    <h4><strong>项目描述:</strong></h4>
                    <textarea
                      className={'project-description'}
                      defaultValue={this.state.description}/>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col className={'map'} span={8}>
              <ProjectMap/>
            </Col>
          </Row>
        </div>
    )
  }
}

export default connect(null, {getProject})(ProjectInfo)