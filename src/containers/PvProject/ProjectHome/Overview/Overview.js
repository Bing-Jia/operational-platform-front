/**
 * @File   : OverView.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/15
 * @Desc   : 项目总览
 **/

import {Col, Row} from "antd";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import React, {Component} from "react";

import {
  AlignCenterOutlined, AreaChartOutlined,
  BarChartOutlined, FundOutlined
} from '@ant-design/icons';

import InfoShowUnit from "src/components/Unit/InfoShowUnit/InfoShowUnit";

import "./Overview.sass";


class Overview extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    const {dataInfo, defectInfo, defectTypeInfo} = this.props;
    return (
      <div className={'pv-home-overview'}>
        <Row className={'overview'} align="middle" justify={'space-around'}>
          <Col span={4}>
            <InfoShowUnit
              componentTitle={() => <AlignCenterOutlined/>}
              title='团队成员数'
              number={4}
              radio={'0%'}
              lift={true}
            />
          </Col>
          <Col span={4}>
            <InfoShowUnit
              componentTitle={() => <FundOutlined/>}
              title='可见光图片数'
              title_icon_color='rgb(26, 187, 156)'
              number={dataInfo.total}
              radio={dataInfo.radio}
              color='#1ABB9C'
              lift={true}
            />
          </Col>
          <Col span={4}>
            <InfoShowUnit
              componentTitle={() => <FundOutlined/>}
              title='热成像图片数'
              title_icon_color='orangered'
              number={dataInfo.total}
              radio={dataInfo.radio}
              color='orangered'
              lift={true}
            />
          </Col>
          <Col span={4}>
            <InfoShowUnit
              componentTitle={() => <BarChartOutlined/>}
              title='缺陷类型数'
              number={defectTypeInfo.count}
              radio={defectTypeInfo.radio}
              lift={false}
            />
          </Col>
          <Col span={4}>
            <InfoShowUnit
              componentTitle={() => <AreaChartOutlined/>}
              title='预警缺陷数'
              number={defectInfo.total}
              radio={defectInfo.radio}
              last={true}
              lift={true}
            />
          </Col>
        </Row>
      </div>
    )
  }
}

Overview.propTypes = {
  dataInfo: PropTypes.object,
  defectInfo: PropTypes.object,
  projectInfo: PropTypes.object,
  defectTypeInfo: PropTypes.object,
};

export default connect(
  state => ({
    dataInfo: state.domainData.dataList,
    defectInfo: state.domainData.defectList,
    projects: state.domainData.projects,
    defectTypeInfo: state.domainData.defectTypeList,
  }),
  null
)(Overview)
