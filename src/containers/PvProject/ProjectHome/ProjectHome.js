/**
 * @File   : ProjectHome.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/14
 * @Desc   : 项目首页
 **/

import {Space} from "antd";
import React, {Component} from "react";
import Overview from "./Overview/Overview";
import ProjectInfo from "./ProjectInfo/ProjectInfo";

import "./ProjectHome.sass";

class ProjectHome extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loadStatus: false,
      dataStatus: false,
    }
  }

  componentDidMount() {
    setTimeout(() => this.setState({
        loadStatus: !this.state.loadStatus
      }), 3000
    )
  }


  render() {
    return (
      <div className={'pv-project-home'}>
        <Space className={'space'} direction={'vertical'} >
          <Overview/>
          <ProjectInfo projectId={this.props.match.params.project_id}/>
        </Space>
      </div>
    )
  }
}

export default ProjectHome