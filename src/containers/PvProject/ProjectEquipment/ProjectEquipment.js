/**
 * @File   : ProjectEquipment.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/17
 * @Desc   : 项目设备
 **/

import {Row, Col} from "antd";
import React, {Component} from "react";
import UAV from "src/components/Equipment/UAV/UAV"
import Camera from "src/components/Equipment/Camera/Camera";

import "./ProjectEquipment.sass";

class ProjectEquipment extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return (
      <div className={'pv-project-equipment'}>
        <Row justify={'space-around'} align={'middle'} wrap>
          <Col className={'equipment-content'} span={12}>
            <UAV/>
          </Col>
          <Col className={'equipment-content'} span={12}>
            <Camera/>
          </Col>
          <Col className={'equipment-content'} span={12}>
            <Camera/>
          </Col>
          <Col className={'equipment-content'} span={12}>
            <UAV/>
          </Col>
        </Row>
      </div>
    )
  }
}

export default ProjectEquipment