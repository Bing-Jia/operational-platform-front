/**
 * @File   : ProjectDefect.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/16
 * @Desc   : 项目缺陷
 **/

import React, {Component} from 'react';
import {NavLink} from "react-router-dom";
import Highlighter from 'react-highlight-words';
import {SearchOutlined} from '@ant-design/icons';
import {Tabs, Table, Input, Button, Space, Image, Modal, Row, Col} from "antd";

import PV from "src/assets/images/banner/pv.jpg";
import Fan from "src/assets/images/banner/fan.jpg";
import VISIBLE_SRC from "src/assets/images/defect/wl000726.jpg";
import THERMAL_SRC from "src/assets/images/defect/wl000726_src.jpg";
import THERMAL_OUT from "src/assets/images/defect/wl000726_out.jpg";
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";

import "./ProjectDefect.sass";

const {TabPane} = Tabs;


const data = [];
for (let i = 1; i < 100; i++) {
  data.push({
    key: i,
    index: i,
    defectType: `Edward King ${i}`,
    imageType: '热成像',
    location: '106.3586, 38.1775',
  });
}

class ProjectDefect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modelStatus: false
    }
  }

  changeModelStatus = () => {
    this.setState({
      modelStatus: !this.state.modelStatus
    })
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
      <div style={{padding: 8}}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{width: 188, marginBottom: 8, display: 'block'}}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined/>}
            size="small"
            style={{width: 90}}
          >
            Search
          </Button>
          <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{width: 90}}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{color: filtered ? '#1890ff' : undefined}}/>,
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        : '',
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },
    render: text =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{backgroundColor: '#ffc069', padding: 0}}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({searchText: ''});
  };

  render() {
    const columns = [
      {
        title: '序号',
        align: 'center',
        dataIndex: 'index',
        width: '10%',
        sorter: (a, b) => a.index - b.index,
      },
      {
        title: '缺陷类型',
        align: 'center',
        dataIndex: 'defectType',
        width: '20%',
        ...this.getColumnSearchProps('defectType')
      },
      {
        title: '图片类型',
        align: 'center',
        dataIndex: 'imageType',
        width: '20%',
      },
      {
        align: 'center',
        title: '经纬度信息',
        dataIndex: 'location',
        width: '20%',
      },
      {
        title: '操作',
        align: 'center',
        dataIndex: 'action',
        width: '20%',
        render: (text, link) => {
          return (
            <Space>
              <Button type={'primary'} size={'small'} onClick={this.changeModelStatus}>
                图片
              </Button>
              <NavLink to={''}>详情</NavLink>
            </Space>
          )
        }
      },
    ];
    return (
      <div className={'pv-project-defect'}>
        <UnitTitle mainTitle={'项目'} subTitle={'项目缺陷'}/>
        <Tabs className={'defect-tabs'} type={'card'}>
          <TabPane tab="热成像" key="1">
            <Table bordered={true} columns={columns} dataSource={data} pagination={{pageSize: 6}}/>
          </TabPane>
          <TabPane tab="可见光" key="2">

          </TabPane>
        </Tabs>
        <Modal
          title="双光图片"
          centered
          footer={null}
          className={'image-info'}
          style={{textAlign: 'center'}}
          onOk={this.changeModelStatus}
          visible={this.state.modelStatus}
          onCancel={this.changeModelStatus}

          width={600}>
          <Row justify={'space-around'} align={'middle'} gutter={[16, 16]}>
            <Col span={12}>
              <Image src={THERMAL_SRC}/>
            </Col>
            <Col span={12}>
              <Image src={THERMAL_OUT}/>
            </Col>
          </Row>
          <Row justify={'space-around'} align={'middle'} gutter={[16, 16]}>
            <Col span={12}>
              <Image src={VISIBLE_SRC}/>
            </Col>
            <Col span={12}>
              <p>缺陷类型：条状缺陷</p>
            </Col>
          </Row>
        </Modal>
      </div>
    )
  }
}

export default ProjectDefect