/**
 * @File   : ProjectMap.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/16
 * @Desc   : 项目地图
 **/

import React, {Component} from 'react';
import CesiumCityHome from "src/components/Cesium/CesiumCityHome";

class ProjectMap extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return (
      <div className={'projectMap'} style={{minHeight:'calc(100vh-64px)'}}>
        <CesiumCityHome/>
      </div>
    )
  }
}

export default ProjectMap