/**
 * @File   : ServiceLease.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/19
 * @Desc   : 服务租赁
 **/

import React, {Component} from "react";
import {Form, Input, Button, Select, Row, Col, Popconfirm} from "antd";
import {CloseSquareOutlined, CheckSquareOutlined} from '@ant-design/icons';

import EditTable from "src/components/Table/EditTable/EditTable";
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";

const {Option} = Select;
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

class ServiceLease extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [
        {
          key: '0',
          index: '0',
          leaseType: '服务租赁',
          serviceRange: '可见光',
          servicePrice: '￥10000',
          serviceType: '周期性',
          leaseStatus: '0'
        },
        {
          key: '1',
          index: '1',
          leaseType: '服务租赁',
          serviceRange: '热成像',
          servicePrice: '￥15000',
          serviceType: '永久性',
          leaseStatus: '1'
        },
        {
          key: '2',
          index: '2',
          leaseType: '服务租赁',
          serviceRange: '全部',
          servicePrice: '￥10000',
          serviceType: '一次性',
          leaseStatus: '1'
        },
      ],
      columns: [
        {
          title: '序号',
          dataIndex: 'index',
          width: '10%',
        },
        {
          title: '租赁类型',
          dataIndex: 'leaseType',
          width: '20%',
        },
        {
          title: '服务范围',
          dataIndex: 'serviceRange',
          width: '10%',
        },
        {
          title: '服务价格',
          dataIndex: 'servicePrice',
          width: '10%',
        },
        {
          title: '服务类型',
          dataIndex: 'serviceType',
          width: '20%',
        },
        {
          title: '操作',
          dataIndex: 'operation',
          width: '20%',
          render: (text, record) =>
            this.state.dataSource.length >= 1 ? (
              <>
                <Popconfirm title="删除该条目?" onConfirm={() => this.deleteItem(record.key)}>
                  <a style={{color: 'red'}}>删除</a>
                </Popconfirm>&nbsp;&nbsp;
                <a style={{color: 'green'}}>发布</a>&nbsp;&nbsp;
                <a style={{color: 'blue'}}>下线</a>
              </>
            ) : null,
        },
        {
          title: '状态',
          dataIndex: 'leaseStatus',
          width: '10%',
          render: (text) =>
            text === '0' ? (<CloseSquareOutlined style={{color: 'red'}}/>) : (
              <CheckSquareOutlined style={{color: 'green'}}/>)
        },
      ]
    }
  }

  deleteItem = (key) => {
    this.setState({
      dataSource: this.state.dataSource.filter(item => item.key !== key)
    })
  };
  formRef = React.createRef();
  onGenderChange = (value) => {
    this.formRef.current.setFieldsValue({
      note: `Hi, ${value === 'male' ? 'man' : 'lady'}!`,
    });
  };
  onFinish = (values) => {
    console.log(values);
  };
  onReset = () => {
    this.formRef.current.resetFields();
  };
  onFill = () => {
    this.formRef.current.setFieldsValue({
      note: 'Hello world!',
      gender: 'male',
    });
  };

  render() {
    const {dataSource, columns} = this.state;
    return (
      <div className={'backstage-lease'}>
        <UnitTitle mainTitle={'服务租赁'} subTitle={'管理'}/>
        <Form className={'lease-form'} {...layout} ref={this.formRef} name="control-ref" onFinish={this.onFinish}>
          <Row>
            <Col span={12}>
              <Form.Item
                name="note"
                label="租赁类型"
              >
                <Input defaultValue={'服务租赁'} disabled/>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="gender"
                label="服务范围"
                rules={[
                  {
                    required: true,
                  },
                ]}>
                <Select
                  placeholder="请选择服务范围"
                  onChange={this.onGenderChange}
                  allowClear>
                  <Option value="visible">可见光</Option>
                  <Option value="thermal">热成像</Option>
                  <Option value="all">全部</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Form.Item
                name="note"
                label="服务价格"
                rules={[
                  {
                    required: true,
                  },
                ]}>
                <Input placeholder={'填写人民币数值...'}/>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="gender"
                label="服务类型"
                rules={[
                  {
                    required: true,
                  },
                ]}>
                <Select
                  placeholder="请选择服务类型..."
                  onChange={this.onGenderChange}
                  allowClear
                >
                  <Option value="cycle">周期性</Option>
                  <Option value="permanent">永久性</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Form.Item
              name="note"
              label="服务描述"
              rules={[
                {
                  required: true,
                },
              ]}
              labelCol={{span: 4}}
              wrapperCol={{span: 20}}
              className={'description'}
            >
              <textarea placeholder={'换行分割每条描述...'}/>
            </Form.Item>
          </Row>
          <Form.Item {...tailLayout}>
            <Button className={'btn-reset'} htmlType="button" onClick={this.onReset}>
              重置
            </Button>
            <Button className={'btn-submit'} htmlType="submit">
              提交
            </Button>
            <Button className={'btn-template'} htmlType="button" onClick={this.onFill}>
              模板
            </Button>
          </Form.Item>
        </Form>
        <Row className={'lease-table'}>
          <EditTable
            dataSource={dataSource} columns={columns} noAdd={true} isExpandable={true}
            addItem={this.addItem} saveItem={this.saveItem} hasBorder={false}
            expandableComponent={
              <Row align={'middle'} justify={'space-around'}>
                <Col span={2}>
                  <span>服务描述</span>
                </Col>
                <Col span={20}>
                  <textarea placeholder={'换行分割每条描述...'}/>
                </Col>
              </Row>}
          />
        </Row>
      </div>
    )
  }
}

export default ServiceLease;