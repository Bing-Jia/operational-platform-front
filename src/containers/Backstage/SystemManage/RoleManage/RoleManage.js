/**
 * @File   : RoleManage.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/19
 * @Desc   : 角色管理
 **/

import React, {Component} from "react";
import {Row, Col, Tabs, Button, Divider, Collapse, Switch, notification, Modal} from "antd";
import {
  DeleteOutlined,
  ExclamationCircleOutlined,
  PlusOutlined,
  CheckSquareFilled,
  CloseSquareFilled,
  TagTwoTone
} from '@ant-design/icons';

import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";

import "./RoleManage.sass";

const {TabPane} = Tabs;
const {confirm} = Modal;
const {Panel} = Collapse;

class RoleManage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resourceList: ["项目权限", "数据权限", "检测权限", "报告权限"],
      roleList: ["管理员", "系统管理员", "超级管理员"],
      permissionList: {
        "管理员": {
          "项目权限": {"查看项目": true, "修改项目": false, "删除项目": false},
          "数据权限": {"查看数据": true, "添加数据": false, "修改数据": false, "删除数据": false},
          "检测权限": {"启动检测": true, "重置检测": false, "选择检测": false},
          "报告权限": {"查看报告": true, "下载报告": true, "删除报告": true}
        },
        "系统管理员": {
          "项目权限": {"查看项目": true, "修改项目": false, "删除项目": false},
          "数据权限": {"查看数据": true, "添加数据": true, "修改数据": true, "删除数据": false},
          "检测权限": {"启动检测": true, "重置检测": true, "选择检测": true},
          "报告权限": {"查看报告": true, "下载报告": true, "删除报告": false}
        },
        "超级管理员": {
          "项目权限": {"查看项目": true, "修改项目": true, "删除项目": true},
          "数据权限": {"查看数据": true, "添加数据": true, "修改数据": true, "删除数据": true},
          "检测权限": {"启动检测": true, "重置检测": true, "选择检测": true},
          "报告权限": {"查看报告": true, "下载报告": true, "删除报告": true}
        }
      },
      initPermission: {
        "项目权限": {"查看项目": true, "修改项目": false, "删除项目": false},
        "数据权限": {"查看数据": true, "添加数据": false, "修改数据": false, "删除数据": false},
        "检测权限": {"启动检测": true, "重置检测": false, "选择检测": false},
        "报告权限": {"查看报告": true, "下载报告": false, "删除报告": false}
      },
      newRole: ''
    };
  }

  openNotificationWithIcon = (type, content) => {
    notification[type]({
      message: '操作成功',
      description: content
    });
  };

  deleteRole = (index) => {
    let that = this;
    confirm({
      title: '删除该角色?',
      icon: <ExclamationCircleOutlined/>,
      content: '该角色将会从你的项目角色列表中删除！',
      okText: '确认',
      okType: 'danger',
      cancelText: '取消',
      onOk() {
        const roleList = that.state.roleList;
        roleList.splice(index, 1);
        that.setState({
          roleList: roleList
        });
        that.openNotificationWithIcon('success', '成功的将该角色从角色列表中删除！')
      },
      onCancel() {
        console.log('Cancel');
      },
    })
  };

  inputChange = (e) => {
    this.setState({
      newRole: e.target.value
    })
  };

  addRole = () => {
    const {roleList, newRole, permissionList, initPermission} = this.state;
    roleList.push(newRole);
    permissionList[newRole] = initPermission;
    this.setState({
      roleList: roleList,
      permissionList: permissionList
    });
    this.openNotificationWithIcon('success', '角色添加成功，请为其分配权限！')
  };

  changePermission = (item, resource_item, permission_info_item) => {
    let value = this.state.permissionList[item][resource_item][permission_info_item];
    this.setState({
        permissionList: {
          ...this.state.permissionList,
          [item]: {
            ...this.state.permissionList[item],
            [resource_item]: {
              ...this.state.permissionList[item][resource_item],
              [permission_info_item]: !value
            }
          }
        }
      }
    );
    this.openNotificationWithIcon('success', '权限修改成功！')
  };

  dealPermissionList = (data) => {
    let retList = [];
    Object.values(data).map(item => item ? retList.push(1) : retList.push(0));
    return retList
  };

  dealPermissionCount = (data) => {
    return Object.values(data).filter(item => item === true).length;
  };


  render() {
    const {roleList, permissionList, resourceList} = this.state;
    return (
      <div className={'backstage-system-role'}>
        <UnitTitle mainTitle={'权限角色'} subTitle={'管理'}/>
        <Tabs className={'role-tabs'} tabPosition='left' size='large'>
          {roleList.map((item, index) => {
            return (
              <TabPane className={'tab-pane'} tab={item} key={index}>
                <Row>
                  <Button className={'delete-btn'} type={"danger"} icon={<DeleteOutlined/>}
                          onClick={this.deleteRole.bind(this, index)}>
                    删除
                  </Button>
                </Row>
                <Row className={'role'}>
                  <div><span>{item}</span></div>
                </Row>
                <Row className={'permission-list'}>
                  <Divider/>
                  <Collapse
                    bordered={false}
                    className={'collapse'}
                    expandIconPosition='right'
                  >
                    {resourceList.map((resource_item, resource_index) => {
                      return (
                        <Panel header={resource_item} key={resource_index}
                               extra={
                                 <div className={'permission-show'}>
                                   <span>{this.dealPermissionCount(permissionList[item][resource_item])}/{Object.keys(permissionList[item][resource_item]).length} &nbsp;&nbsp;</span>
                                   {this.dealPermissionList(permissionList[item][resource_item]).map((permissionSquaer, index) => {
                                     return (
                                       permissionSquaer === 1
                                         ? (<CheckSquareFilled className={'check-icon'} key={index}/>)
                                         : (<CloseSquareFilled className={'close-icon'} key={index}/>)
                                     )
                                   })}
                                 </div>
                               }>
                          <div className={'permission-select'}>
                            {Object.keys(permissionList[item][resource_item]).map((permission_item_key, permission_index) => {
                              return (
                                <Row className={'select-content'} key={permission_index}>
                                  <Col span={20}>
                                    {permission_item_key}
                                  </Col>
                                  <Col span={4}>
                                    <Switch className={'switch'} checkedChildren="是" unCheckedChildren="否"
                                            defaultChecked={permissionList[item][resource_item][permission_item_key]}
                                            onChange={this.changePermission.bind(this, item, resource_item, permission_item_key)}
                                    />
                                  </Col>
                                  <Divider className={'divider'}/>
                                </Row>
                              )
                            })
                            }
                          </div>
                        </Panel>
                      )
                    })}
                  </Collapse>
                </Row>
              </TabPane>
            )
          })}
          <TabPane className={'add-user-tab-pane'} tab={
            <Button className={'add-user-btn'} type="primary" icon={<PlusOutlined/>}>
              新角色
            </Button>} key="6">
            <Row className={'add-user-content'}>
              <div>
                <span><TagTwoTone className={'tag-icon'}/>&nbsp;&nbsp;角色：</span>
                <input type="text" onChange={(e) => this.inputChange(e)}/>
                <Button type="primary" onClick={this.addRole}>添加</Button><br/><br/>
                <hr/>
                <p>你将会为该项目添加一个新角色，并且请为其分配权限。</p>
              </div>
            </Row>
          </TabPane>)
        </Tabs>
      </div>
    )
  }
}

export default RoleManage