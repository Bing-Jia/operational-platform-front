/**
 * @File   : PermissionManage.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/19
 * @Desc   : 权限管理
 **/

import React, {Component} from "react";
import {Row, Col, Form, Input, Button, Collapse, Divider, notification} from "antd";
import {
  MinusCircleOutlined, PlusOutlined,
  CheckSquareFilled, CloseSquareFilled,
} from '@ant-design/icons';

import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";

import './PermissionManage.sass'

const {Panel} = Collapse;


class PermissionManage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resourceList: ["项目", "数据", "检测", "报告"],
      permissionList: {
        "项目": {"查看项目": true, "修改项目": true, "删除项目": false},
        "数据": {"查看数据": true, "添加数据": true, "修改数据": true, "删除数据": true},
        "检测": {"启动检测": true, "重置检测": false, "选择检测": true},
        "报告": {"查看报告": true, "下载报告": true, "删除报告": false}
      }
    }
  };

  onFinish = values => {
    console.log('Received values of form:', values);
    this.openNotificationWithIcon('success', '提交成功...')
  };

  dealPermissionList = (data) => {
    let retList = [];
    Object.values(data).map(item => item ? retList.push(1) : retList.push(0));
    return retList
  };
  openNotificationWithIcon = (type, content) => {
    notification[type]({
      message: '操作成功',
      description: content
    });
  };

  render() {
    const {permissionList, resourceList} = this.state;
    return (
      <div className={'backstage-system-permission'}>
        <UnitTitle mainTitle={'资源管理'} subTitle={'管理'}/>
        <Row className={'resource-form'}>
          <Form className={'form'} onFinish={this.onFinish}>
            <Form.Item
              name="source"
              label={'资源名称'}
              labelCol={{span: '4'}}
              wrapperCol={{span: '20'}}
              rules={[
                {
                  required: true,
                  whitespace: true,
                  message: "输入管理的资源",
                },
              ]}
            >
              <Input placeholder={'输入资源名称'}/>
            </Form.Item>
            <Form.List
              name="item"
            >
              {(fields, {add, remove}) => {
                return (
                  <Row>
                    {fields.map((field, index) => (
                      <Col span={12} key={index}>
                        <Form.Item
                          label={
                            <>
                              <MinusCircleOutlined onClick={() => {
                                remove(field.name);
                              }}/>&nbsp;&nbsp;资源条目
                            </>}
                          required={true}
                          key={field.key}
                          labelCol={{span: '8'}}
                          wrapperCol={{span: '16'}}
                        >
                          <Form.Item
                            {...field}
                            validateTrigger={['onChange', 'onBlur']}
                            rules={[
                              {
                                required: true,
                                whitespace: true,
                                message: "输入资源细分条目:比如【增删改查】资源",
                              },
                            ]}
                            noStyle
                          >
                            <Input placeholder="【增删改查】资源"/>
                          </Form.Item>
                        </Form.Item>
                      </Col>
                    ))}
                    <Row className={'operation-resource'}>
                      <Col span={12}>
                        <Form.Item className={'add-resource'}>
                          <Button
                            type="dashed"
                            onClick={() => {
                              add();
                            }}
                          >
                            <PlusOutlined/> 添加资源条目
                          </Button>
                        </Form.Item>
                      </Col>
                      <Col span={12}>
                        <Form.Item className={'add-resource'}>
                          <Button type="primary" htmlType="submit">
                            提交
                          </Button>
                        </Form.Item>
                      </Col>
                    </Row>
                  </Row>
                );
              }}
            </Form.List>
          </Form>
        </Row>
        <Divider/>
        <Row className={'resource-show'}>
          <Collapse
            bordered={false}
            className={'collapse'}
            expandIconPosition='right'
          >
            {resourceList.map((resource_item, resource_index) => {
              return (
                <Panel header={'【' + resource_item + '】 资源'} key={resource_index}
                       extra={
                         <div className={'resource-list'}>
                           {this.dealPermissionList(permissionList[resource_item]).map((permissionSquaer, index) => {
                             return (
                               permissionSquaer === 1
                                 ? (<CheckSquareFilled className={'check-icon'} key={index}/>)
                                 : (<CloseSquareFilled className={'close-icon'} key={index}/>)
                             )
                           })}
                         </div>
                       }>

                    <Row>
                      {Object.keys(permissionList[resource_item]).map((permission_item_key, permission_index) => {
                          return (
                            <Col span={6} key={permission_index}>
                              <div className={'resource-content'} contentEditable={true}>
                                {permission_item_key}
                              </div>
                            </Col>
                          )
                        }
                      )}
                    </Row>
                </Panel>
              )
            })}
          </Collapse>
        </Row>
      </div>
    )
  }
}

export default PermissionManage