/**
 * @File   : UserManage.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/19
 * @Desc   : 用户管理
 **/

import React, {Component} from "react";
import {Col, Row, Table, Tabs, Button} from "antd";
import {UserOutlined, PhoneOutlined, TagsOutlined} from '@ant-design/icons';

import userAvatar from "src/assets/images/teams/qiu.jpg";
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";

import "./UserManage.sass";

const {TabPane} = Tabs;

const columns = [
  {
    title: '头像',
    dataIndex: 'avatar',
    key: 'avatar',
    align: 'center',
    render: (record) => <img src={userAvatar} style={{width: '40px', height: '40px'}} alt=''/>
  },
  {
    title: '用户名',
    dataIndex: 'username',
    key: 'username',
    align: 'center',
  },
  {
    title: '姓名',
    dataIndex: 'fullname',
    key: 'fullname',
    align: 'center',
  },
  {
    title: '角色',
    dataIndex: 'role',
    key: 'role',
    align: 'center',
  },
  {
    title: '电话',
    key: 'phone',
    align: 'center',
    dataIndex: 'phone',
  },
  {
    title: '邮箱',
    key: 'email',
    align: 'center',
    dataIndex: 'email',
  },
];

const data = [
  {
    key: '1',
    avatar: {userAvatar},
    username: 'admin',
    fullname: '张三',
    role: '前端',
    phone: '18117590190',
    email: '1755241561@qq.com'
  },
  {
    key: '2',
    avatar: {userAvatar},
    username: 'admin',
    fullname: '张三',
    role: '前端',
    phone: '18117590190',
    email: '1755241561@qq.com'
  },
  {
    key: '3',
    avatar: {userAvatar},
    username: 'admin',
    fullname: '张三',
    role: '前端',
    phone: '18117590190',
    email: '1755241561@qq.com'
  },
];

class UserManage extends Component {
  render() {
    return (
      <div className={'backstage-system-user'}>
        <UnitTitle mainTitle={'用户管理'} subTitle={'管理'}/>
        <Tabs className={'user-tabs'} type="card">
          <TabPane tab="模块" key="module">
            <Row className={'user-list'} gutter={[16, 16]}>
              <Col span={8}>
                <Row className={'user-unit'}>
                  <Col className={'info'} span={14}>
                    <h2>用户信息</h2>
                    <span><UserOutlined className={'span-icon'}/>姓名：张三</span><br/>
                    <span><TagsOutlined className={'span-icon'}/>角色：前端</span><br/>
                    <span><PhoneOutlined className={'span-icon'}/>电话：18117590190</span>
                  </Col>
                  <Col className={'avatar'} span={10}>
                    <Row>
                      <img src={userAvatar} alt=""/>
                    </Row>
                    <Row className={'operation-btn'}>
                      <Col span={12}>
                        <Button className={'btn-view'} type='primary'>查看</Button>
                      </Col>
                      <Col span={12}>
                        <Button className={'btn-delete'} type="primary" danger>删除</Button>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
              <Col span={8}>
                <Row className={'user-unit'}>
                  <Col className={'info'} span={14}>
                    <h2>用户信息</h2>
                    <span><UserOutlined className={'span-icon'}/>姓名：张三</span><br/>
                    <span><TagsOutlined className={'span-icon'}/>角色：前端</span><br/>
                    <span><PhoneOutlined className={'span-icon'}/>电话：18117590190</span>
                  </Col>
                  <Col className={'avatar'} span={10}>
                    <Row>
                      <img src={userAvatar} alt=""/>
                    </Row>
                    <Row className={'operation-btn'}>
                      <Col span={12}>
                        <Button className={'btn-view'} type='primary'>查看</Button>
                      </Col>
                      <Col span={12}>
                        <Button className={'btn-delete'} type="primary" danger>删除</Button>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
              <Col span={8}>
                <Row className={'user-unit'}>
                  <Col className={'info'} span={14}>
                    <h2>用户信息</h2>
                    <span><UserOutlined className={'span-icon'}/>姓名：张三</span><br/>
                    <span><TagsOutlined className={'span-icon'}/>角色：前端</span><br/>
                    <span><PhoneOutlined className={'span-icon'}/>电话：18117590190</span>
                  </Col>
                  <Col className={'avatar'} span={10}>
                    <Row>
                      <img src={userAvatar} alt=""/>
                    </Row>
                    <Row className={'operation-btn'}>
                      <Col span={12}>
                        <Button className={'btn-view'} type='primary'>查看</Button>
                      </Col>
                      <Col span={12}>
                        <Button className={'btn-delete'} type="primary" danger>删除</Button>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
              <Col span={8}>
                <Row className={'user-unit'}>
                  <Col className={'info'} span={14}>
                    <h2>用户信息</h2>
                    <span><UserOutlined className={'span-icon'}/>姓名：张三</span><br/>
                    <span><TagsOutlined className={'span-icon'}/>角色：前端</span><br/>
                    <span><PhoneOutlined className={'span-icon'}/>电话：18117590190</span>
                  </Col>
                  <Col className={'avatar'} span={10}>
                    <Row>
                      <img src={userAvatar} alt=""/>
                    </Row>
                    <Row className={'operation-btn'}>
                      <Col span={12}>
                        <Button className={'btn-view'} type='primary'>查看</Button>
                      </Col>
                      <Col span={12}>
                        <Button className={'btn-delete'} type="primary" danger>删除</Button>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>
          </TabPane>
          <TabPane tab="列表" key="list" >
            <Table bordered={true} columns={columns} dataSource={data}/>
          </TabPane>
        </Tabs>
      </div>
    )
  }
}

export default UserManage