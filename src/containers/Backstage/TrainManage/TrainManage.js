/**
 * @File   : TrainManage.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/19
 * @Desc   : 训练管理
 **/

import {connect} from "react-redux";
import React, {Component} from "react";
import {trainModel} from "src/api/request";
import {Form, Input, InputNumber, Button, Select, Row, Col, Popconfirm, Progress} from 'antd';

import EditTable from "src/components/Table/EditTable/EditTable";
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";

import "./TrainManage.sass";

const {Option} = Select;
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

class ModelTrain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [
        {
          key: '0',
          index: '0',
          modelName: '训练模型一',
          modelType: '可见光',
          trainAuthor: '张三',
          trainProgress: 60,
          trainStatus: '1',
        },
        {
          key: '1',
          index: '1',
          modelName: '训练模型二',
          modelType: '热成像',
          trainAuthor: '张三',
          trainProgress: 0,
          trainStatus: '0',
        },
        {
          key: '2',
          index: '2',
          modelName: '训练模型二',
          modelType: '可见光',
          trainAuthor: '张三',
          trainProgress: 100,
          trainStatus: '2',
        },
      ],
      columns: [
        {
          title: '序号',
          dataIndex: 'index',
          width: '10%',
        },
        {
          title: '模型名称',
          dataIndex: 'modelName',
          width: '20%',
        },
        {
          title: '模型类型',
          dataIndex: 'modelType',
          width: '10%',
        },
        {
          title: '训练人员',
          dataIndex: 'trainAuthor',
          width: '10%',
        },
        {
          title: '训练进度',
          dataIndex: 'trainProgress',
          width: '15%',
          render: (text) => <Progress
            strokeColor={{
              from: '#108ee9',
              to: '#87d068',
            }} percent={text} size="small" status={text === 100 ? 'success' : 'active'}/>
        },
        {
          title: '训练状态',
          dataIndex: 'trainStatus',
          width: '15%',
          render: (text) => {
            if (text === '0') {
              return <Button style={{backgroundColor: '#40a9ff', color: 'white'}}>未开始</Button>
            } else if (text === '1') {
              return <Button style={{backgroundColor: '#f56c6c', color: 'white'}}>进行中</Button>
            } else if (text === '2') {
              return <Button style={{backgroundColor: '#27ad60', color: 'white'}}>已完成</Button>
            }
          }
        },
        {
          title: '操作',
          dataIndex: 'operation',
          width: '20%',
          render: (text, record) =>
            this.state.dataSource.length >= 1 ? (
              <>
                <Popconfirm title="删除该条目?" onConfirm={() => this.deleteItem(record.key)}>
                  <Button style={{backgroundColor: '#f56c6c', color: 'white'}}>删除</Button>
                </Popconfirm>&nbsp;&nbsp;
                <Button style={{backgroundColor: '#27ad60', color: 'white'}} onClick={this.startTrain}>启动</Button>
              </>
            ) : null,
        }
      ]
    }
  }

  startTrain =() =>{
    this.props.trainModel()
  };

  deleteItem = (key) => {
    this.setState({
      dataSource: this.state.dataSource.filter(item => item.key !== key)
    })
  };
  formRef = React.createRef();
  onGenderChange = (value) => {
    this.formRef.current.setFieldsValue({
      note: `Hi, ${value === 'male' ? 'man' : 'lady'}!`,
    });
  };
  onFinish = (values) => {
    console.log(values);
  };
  onReset = () => {
    this.formRef.current.resetFields();
  };
  onFill = () => {
    this.formRef.current.setFieldsValue({
      note: 'Hello world!',
      gender: 'male',
    });
  };

  render() {
    const {dataSource, columns} = this.state;
    return (
      <div className={'backstage-train'}>
        <UnitTitle mainTitle={'模型训练'} subTitle={'管理'}/>
        <Form className={'train-form'} {...layout} ref={this.formRef} name="control-ref" onFinish={this.onFinish}>
          <Row>
            <Col span={12}>
              <Form.Item
                name="modelName"
                label="模型名称"
                rules={[
                  {
                    required: true,
                  },
                ]}
              >
                <Input placeholder={'输入模型名称'}/>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="imageType"
                label="图片类型"
                rules={[
                  {
                    required: true,
                  },
                ]}>
                <Select
                  placeholder="选择图片类型"
                  onChange={this.onGenderChange}
                  allowClear>
                  <Option value="thermal">热成像</Option>
                  <Option value="visible">可见光</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Form.Item
                name="classNum"
                label="分类数目"
                rules={[
                  {
                    required: true,
                  },
                ]}>
                <InputNumber defaultValue={1}/>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="network"
                label="网络模型"
                rules={[
                  {
                    required: true,
                  },
                ]}>
                <Select
                  placeholder="选择网络模型"
                  onChange={this.onGenderChange}
                  allowClear
                >
                  <Option value="yolo">YoloV5</Option>
                  <Option value="faster-rcnn">Faster-RCNN</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item
                name="classType"
                label="分类类型"
                labelCol={{span: 4}}
                wrapperCol={{span: 20}}
                rules={[
                  {
                    required: true,
                  },
                ]}>

                <Input placeholder={'多类别逗号分割'}/>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Form.Item
                name="snapshotNum"
                label="快照数量"
                rules={[
                  {
                    required: true,
                  },
                ]}>
                <InputNumber key='2' defaultValue={1}/>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="iterationNum"
                label="迭代次数"
                rules={[
                  {
                    required: true,
                  },
                ]}>
                <InputNumber key='1' defaultValue={300}/>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Form.Item
                name="dataSet"
                label="训练数据"
                rules={[
                  {
                    required: true,
                  },
                ]}>
                <Select
                  placeholder="选择标注数据"
                  onChange={this.onGenderChange}
                  allowClear>
                  <Option value="markData1">标注数据一</Option>
                  <Option value="markData2">标注数据二</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="modelPath"
                label="保存路径"
                rules={[
                  {
                    required: true,
                  },
                ]}>
                <Select
                  placeholder="选择保存路径"
                  onChange={this.onGenderChange}
                  allowClear>
                  <Option value="storePath1">保存路径一</Option>
                  <Option value="storePath2">保存路径二</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Form.Item
              name="modelDescription"
              label="训练模型描述"
              labelCol={{span: 4}}
              wrapperCol={{span: 20}}
              className={'description'}
            >
              <textarea placeholder={'输入模型训练信息'}/>
            </Form.Item>
          </Row>
          <Form.Item {...tailLayout}>
            <Button className={'btn-reset'} htmlType="button" onClick={this.onReset}>
              重置
            </Button>
            <Button className={'btn-submit'} htmlType="submit">
              提交
            </Button>
            <Button className={'btn-template'} htmlType="button" onClick={this.onFill}>
              模板
            </Button>
          </Form.Item>
        </Form>
        <Row className={'train-table'}>
          <EditTable
            dataSource={dataSource} columns={columns} noAdd={true} isExpandable={true}
            addItem={this.addItem} saveItem={this.saveItem} hasBorder={true}
            expandableComponent={
              <Row align={'middle'} justify={'space-around'}>
                <Col span={2}>
                  <span>平台描述</span>
                </Col>
                <Col span={20}>
                  <textarea placeholder={'换行分割每条描述...'}/>
                </Col>
              </Row>}
          />
        </Row>
      </div>
    )
  }
}

export default connect(null, {trainModel})(ModelTrain)