/**
 * @File   : DefectType.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/19
 * @Desc   : 缺陷类型
 **/


import React, {Component} from "react";
import {Form, Input, Button, Select, Row, Col, Image, Popconfirm} from "antd";

import Qiu from "src/assets/images/teams/qiu.jpg";
import EditTable from "src/components/Table/EditTable/EditTable";
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";

import "./DefectType.sass";

const {Option} = Select;
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

class DefectType extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [
        {
          key: '0',
          index: '0',
          defectName: '亮斑',
          imageType: '热成像',
          repairAdvice: '建议对该光伏组件进行置换或返修。',
          defectImage: Qiu,
          defectVideo: '1',
        },
        {
          key: '1',
          index: '1',
          defectName: '遮挡',
          imageType: '可见光',
          repairAdvice: '建议对该光伏组件的周围区域以及底部区域进行植被处理。',
          defectImage: Qiu,
          defectVideo: '1',
        },
        {
          key: '2',
          index: '2',
          defectName: '鸟屎',
          imageType: '可见光',
          repairAdvice: '建议清除该光伏组件板面上的鸟类排泄物。',
          defectImage: Qiu,
          defectVideo: '1',
        },
      ],
      columns: [
        {
          title: '序号',
          dataIndex: 'index',
          width: '5%',
        },
        {
          title: '缺陷名称',
          dataIndex: 'defectName',
          width: '10%',
        },
        {
          title: '图片类型',
          dataIndex: 'imageType',
          width: '10%',
        },
        {
          title: '修复建议',
          dataIndex: 'repairAdvice',
          width: '30%',
        },
        {
          title: '缺陷样例',
          dataIndex: 'defectImage',
          width: '15%',
          render: (text) => <Image src={text} width={60} height={40} alt=''/>
        },
        {
          title: '缺陷视频',
          dataIndex: 'defectVideo',
          width: '10%',
          render: (text) => <Button style={{backgroundColor: '#27ad60', color: 'white'}}>播放视频</Button>
        },
        {
          title: '操作',
          dataIndex: 'operation',
          width: '20%',
          render: (text, record) =>
            this.state.dataSource.length >= 1 ? (
              <>
                <Popconfirm title="删除该条目?" onConfirm={() => this.deleteItem(record.key)}>
                  <a style={{color: 'red'}}>删除</a>
                </Popconfirm>&nbsp;&nbsp;
                <a style={{color: 'green'}}>发布</a>&nbsp;&nbsp;
                <a style={{color: 'blue'}}>下线</a>
              </>
            ) : null,
        },
      ]
    }
  }

  deleteItem = (key) => {
    this.setState({
      dataSource: this.state.dataSource.filter(item => item.key !== key)
    })
  };
  formRef = React.createRef();
  onGenderChange = (value) => {
    this.formRef.current.setFieldsValue({
      note: `Hi, ${value === 'male' ? 'man' : 'lady'}!`,
    });
  };
  onFinish = (values) => {
    console.log(values);
  };
  onReset = () => {
    this.formRef.current.resetFields();
  };
  onFill = () => {
    this.formRef.current.setFieldsValue({
      note: 'Hello world!',
      gender: 'male',
    });
  };

  render() {
    const {dataSource, columns} = this.state;
    return (
      <div className={'backstage-defect-type'}>
        <UnitTitle mainTitle={'缺陷类型'} subTitle={'管理'}/>
        <Form className={'defect-type-form'} {...layout} ref={this.formRef} name="control-ref" onFinish={this.onFinish}>
          <Row>
            <Col span={12}>
              <Form.Item
                name="defectType"
                label="缺陷类型"
                rules={[
                  {
                    required: true,
                  },
                ]}
              >
                <Input placeholder={'输入缺陷名称'}/>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="imageType"
                label="图片类型"
                rules={[
                  {
                    required: true,
                  },
                ]}>
                <Select
                  placeholder="选择图片类型"
                  onChange={this.onGenderChange}
                  allowClear>
                  <Option value="thermal">热成像</Option>
                  <Option value="visible">可见光</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Form.Item
                name="defectImage"
                label="缺陷图片"
                rules={[
                  {
                    required: true,
                  },
                ]}>
                <Input/>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="repair"
                label="修复视频"
                rules={[
                  {
                    required: true,
                  },
                ]}>
                <Input/>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item
                name="repairAdvce"
                label="修复建议"
                labelCol={{span: 4}}
                wrapperCol={{span: 20}}
                rules={[
                  {
                    required: true,
                  },
                ]}>
                <Input placeholder={'输入缺陷修复建议'}/>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Form.Item
              name="defectDescription"
              label="缺陷描述"
              labelCol={{span: 4}}
              wrapperCol={{span: 20}}
              className={'description'}
            >
              <textarea placeholder={'输入模型缺陷生成信息'}/>
            </Form.Item>
          </Row>
          <Form.Item {...tailLayout}>
            <Button className={'btn-reset'} htmlType="button" onClick={this.onReset}>
              重置
            </Button>
            <Button className={'btn-submit'} htmlType="submit">
              提交
            </Button>
            <Button className={'btn-template'} htmlType="button" onClick={this.onFill}>
              模板
            </Button>
          </Form.Item>
        </Form>
        <Row className={'defect-type-table'}>
          <EditTable
            dataSource={dataSource} columns={columns} noAdd={true} isExpandable={true}
            addItem={this.addItem} saveItem={this.saveItem} hasBorder={true}
            expandableComponent={
              <Row align={'middle'} justify={'space-around'}>
                <Col span={2}>
                  <span>缺陷描述</span>
                </Col>
                <Col span={20}>
                  <textarea/>
                </Col>
              </Row>}
          />
        </Row>
      </div>
    )
  }
}

export default DefectType;