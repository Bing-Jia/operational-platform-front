/**
 * @File   : HomeContent.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/18
 * @Desc   : 官网内容
 **/

import {Space} from "antd";
import React, {Component} from "react";

import TeamContent from "./TeamContent/TeamContent";
import FeatureContent from "./FeatureContent/FeatureContent";
import BackgroundImage from "./BackgroundImage/BackgroundImage";

import "./HomeContent.sass";

class HomeContent extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return (
      <div className={'backstage-home-content'}>
        <Space className={'space'} size={'small'} direction={'vertical'} >
          <BackgroundImage/>
          <FeatureContent/>
          <TeamContent/>
        </Space>
      </div>
    )
  }
}

export default HomeContent