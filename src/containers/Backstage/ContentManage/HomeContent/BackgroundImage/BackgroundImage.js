/**
 * @File   : BackgroundImage.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/18
 * @Desc   : 官网背景图片
 **/

import {Row, Col} from "antd";
import React, {Component} from "react";
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";

import PV from "src/assets/images/banner/pv.jpg";
import Fan from "src/assets/images/banner/fan.jpg";
import ImageUpload from "src/components/Upload/ImageUpload/ImageUpload";

import "./BackgroundImage.sass";

class BackgroundImage extends Component {
  constructor(props){
    super(props);
    this.state ={}
  }
  render() {
    return (
     <Row className='home-content-background-manage' justify={'space-around'}>
        <UnitTitle mainTitle={'背景图片'} subTitle={'管理'}/>
        <Col span={12}>
          <ImageUpload srcImage={PV}/>
        </Col>
        <Col span={12}>
          <ImageUpload srcImage={Fan}/>
        </Col>
      </Row>
    )
  }
}

export default BackgroundImage