/**
 * @File   : TeamContent.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/18
 * @Desc   : 官网团队模块
 **/

import {UserAddOutlined} from "@ant-design/icons";
import {Row, Table, Input, Button, Popconfirm, Form} from "antd";
import React, {Component, useContext, useState, useEffect, useRef} from "react";

import Qiu from "src/assets/images/teams/qiu.jpg"
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";

import "./TeamContent.sass";

const EditableContext = React.createContext();

const EditableRow = ({index, ...props}) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};
const EditableCell = ({
                        title,
                        editable,
                        children,
                        dataIndex,
                        record,
                        handleSave,
                        ...restProps
                      }) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef();
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async (e) => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSave({...record, ...values});
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let childNode = children;

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: `${title} is required.`,
          },
        ]}
      >
        <Input ref={inputRef} onPressEnter={save} onBlur={save}/>
      </Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{
          paddingRight: 24,
        }}
        onClick={toggleEdit}
      >
        {children}
      </div>
    );
  }

  return <td {...restProps}>{childNode}</td>;
};

class TeamContent extends Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        title: '序号',
        dataIndex: 'index',
        width: '10%',
      },
      {
        title: '名字',
        dataIndex: 'name',
        editable: true,
      }, {
        title: '头像',
        dataIndex: 'image',
        render: (image) =>
          <img src={image} alt="" style={{width: '30px', height: '30px'}}/>
      },
      {
        title: '简介',
        dataIndex: 'introduce',
        editable: true,
        width: '50%',
      },
      {
        title: '操作',
        dataIndex: 'operation',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.key)}>
              <a style={{color: 'red'}}>删除</a>
            </Popconfirm>
          ) : null,
      },
    ];
    this.state = {
      dataSource: [
        {
          key: '0',
          index: '0',
          name: '张三',
          image: Qiu,
          introduce: '这个小伙很是聪明伶俐',
        },
        {
          key: '1',
          index: '1',
          name: '李四',
          image: Qiu,
          introduce: '这个小伙很是聪明伶俐',
        },
      ],
      count: 2,
    };
  }

  handleDelete = (key) => {
    const dataSource = [...this.state.dataSource];
    this.setState({
      dataSource: dataSource.filter((item) => item.key !== key),
    });
  };
  handleAdd = () => {
    const {count, dataSource} = this.state;
    const newData = {
      key: count,
      index: `${count}`,
      name: `bing ${count}`,
      image: `${Qiu}`,
      introduce: `很是聪明伶俐.... ${count}`,
    };
    this.setState({
      dataSource: [...dataSource, newData],
      count: count + 1,
    });
  };
  handleSave = (row) => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex((item) => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, {...item, ...row});
    this.setState({
      dataSource: newData,
    });
  };

  render() {
    const {dataSource} = this.state;
    const components = {
      body: {
        row: EditableRow,
        cell: EditableCell,
      },
    };
    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }

      return {
        ...col,
        onCell: (record) => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });
    return (
      <Row className={'backstage-home-content-team'}>
        <UnitTitle mainTitle={'官网团队'} subTitle={'管理'}/>
        <Row className={'team-list'} justify="space-around">
          <div className={'team-content'}>
            <Button
              onClick={this.handleAdd}
              type="primary"
              className={'add-team-user-btn'}
            >
              <UserAddOutlined/>新增成员
            </Button>
            <Table
              components={components}
              rowClassName={() => 'editable-row'}
              bordered
              dataSource={dataSource}
              columns={columns}
            />
          </div>
        </Row>
      </Row>
    )
  }
}

export default TeamContent