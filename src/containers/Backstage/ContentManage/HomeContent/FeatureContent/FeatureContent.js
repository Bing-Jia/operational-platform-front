/**
 * @File   : FeatureContent.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/18
 * @Desc   : 官网特征模块
 **/

import {Row, Col} from "antd";
import React, {Component} from 'react';

import uav from "src/assets/images/feature/无人机.png";
import report from "src/assets/images/feature/报告.png";
import UnitEdit from "src/components/Unit/UnitEdit/UnitEdit";
import tensorflow from "src/assets/images/feature/TensrFlow.png";
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";

import "./FeatureContent.sass";


class FeatureContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          image: uav,
          title: "无人机巡检",
          color: 'rgb(14, 144, 210)',
          content: "利用工业级无人机搭载可见光、热红外双光镜头,在光伏厂区规划航线 长航时自动飞行,采集高精度、高稳定性的光伏图片数据。"
        },
        {
          image: tensorflow,
          title: "目标检测",
          color: 'lightcoral',
          content: " 利用卷积神经网络CNN提取图片中物体的特征信息,利用线性回归拟合物体偏移信息, 从而推断被检测图片的中物体的类别以及位置信息。"
        },
        {
          image: report,
          title: "光伏报告",
          color: 'mediumaquamarine',
          content: "基于专家认定的光伏缺陷特征,结合大样本训练的检测模型推断被检测数据的缺陷情况生成具有一定 参考意义的光伏缺陷检测报告。"
        },
      ]
    }
  }

  render() {
    return (
      <Row className='home-content-feature-management'>
        <UnitTitle mainTitle={'官网特征'} subTitle={'管理'}/>
        <Row className={'feature-list'} justify="space-around">
          {
            this.state.data.map((feature_info, index) =>
              <Col className={'feature'} span={6} key={index}>
                <UnitEdit
                  key={index}
                  color={feature_info.color}
                  image={feature_info.image}
                  title={feature_info.title}
                  content={feature_info.content}
                />
              </Col>
            )
          }
        </Row>
      </Row>
    )
  }
}

export default FeatureContent