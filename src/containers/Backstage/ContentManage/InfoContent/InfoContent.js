/**
 * @File   : InfoContent.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/18
 * @Desc   : 咨询发布
 **/


import React, {Component} from "react";
import {PlusOutlined} from "@ant-design/icons";
import {Row, Popconfirm, Table, Button} from "antd";
import {CloseSquareOutlined, CheckSquareOutlined} from '@ant-design/icons';

import Qiu from 'src/assets/images/teams/qiu.jpg'
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";

import "./InfoContent.sass";


class InfoContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [
        {
          key: '0',
          index: '0',
          infoName: '后补贴时代 光伏+的猜想',
          infoContent: '近年来，随着补贴退坡，光伏产业早已不再是单纯地依靠发电+补贴盈利',
          image: Qiu,
          infoDate: '2020年02月20日',
          infoFrom: '北极星电力网',
          infoStatus: '0'
        },
        {
          key: '1',
          index: '1',
          infoName: '后补贴时代 光伏+的猜想',
          infoContent: '近年来，随着补贴退坡，光伏产业早已不再是单纯地依靠发电+补贴盈利',
          image: Qiu,
          infoDate: '2020年02月20日',
          infoFrom: '北极星电力网',
          infoStatus: '1'
        },
        {
          key: '2',
          index: '2',
          infoName: '后补贴时代 光伏+的猜想',
          infoContent: '近年来，随着补贴退坡，光伏产业早已不再是单纯地依靠发电+补贴盈利',
          image: Qiu,
          infoDate: '2020年02月20日',
          infoFrom: '北极星电力网',
          infoStatus: '1'
        },
      ],
      columns: [
        {
          title: '序号',
          dataIndex: 'index',
          width: '3%',
          editable: false,

        },
        {
          title: '标题',
          dataIndex: 'infoName',
          editable: true,
          width: '20%',
          align:'center'
        },
        {
          title: '摘要',
          dataIndex: 'infoContent',
          editable: true,
          width: '30%',
          align:'center'
        },
        {
          title: '图片',
          dataIndex: 'image',
          width: '5%',
          align:'center',
          render: (image) =>
            <img src={image} alt="" style={{width: '30px', height: '30px'}}/>
        },
        {
          title: '时间',
          dataIndex: 'infoDate',
          width: '17%',
          editable: true,
          align:'center',
        },
        {
          title: '出处',
          dataIndex: 'infoFrom',
          width: '13%',
          editable: true,
        },
        {
          title: '操作',
          dataIndex: 'operation',
          width: '18%',
          align:'center',
          render: (text, record) =>
            this.state.dataSource.length >= 1 ? (
              <>
                <Popconfirm title="删除该条目?" onConfirm={() => this.deleteItem(record.key)}>
                  <a style={{color: 'red'}}>删除</a>
                </Popconfirm>&nbsp;&nbsp;
                <a style={{color: 'green'}}>发布</a>&nbsp;&nbsp;
                <a style={{color: 'blue'}}>下线</a>
              </>
            ) : null,
        },
        {
          title: '状态',
          dataIndex: 'infoStatus',
          width: '3%',
          align:'center',
          render: (text) =>
            text === '0' ? (<CloseSquareOutlined style={{color: 'red'}}/>) : (
              <CheckSquareOutlined style={{color: 'green'}}/>)
        },
      ]
    }
  }

  addItem = () => {
    const newDataSource = this.state.dataSource[0];
    this.setState({
      dataSource: [...this.state.dataSource, newDataSource]
    })
  };

  deleteItem = (key) => {
    this.setState({
      dataSource: this.state.dataSource.filter(item => item.key !== key)
    })
  };

  saveItem = (key) => {

  };

  render() {
    const {dataSource, columns} = this.state;
    return (
      <div className={'backstage-content-info'}>
        <UnitTitle mainTitle={'咨询内容'} subTitle={'管理'}/>
        <Row className={'info-content'}>
          <Button className={'add-content-btn'} type="primary" onClick={this.addItem}>
            <PlusOutlined/>新增内容
          </Button>
          <Row className={'table-content'} justify="space-around">
            <Table centered dataSource={dataSource} columns={columns}/>
          </Row>
        </Row>
      </div>
    )
  }
}


export default InfoContent