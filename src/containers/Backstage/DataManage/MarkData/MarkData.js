/**
 * @File   : MarkData.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/19
 * @Desc   : 标注数据
 **/


import React, {Component} from "react";
import Highlighter from "react-highlight-words";
import {SearchOutlined} from '@ant-design/icons';
import {Row, Popconfirm, Input, Button, Space, Select} from "antd";

import EditTable from "src/components/Table/EditTable/EditTable";
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";

const {Option} = Select;

class MarkData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [
        {
          key: '0',
          index: '0',
          dataName: '标注数据一',
          markDefect: '鸟屎、亮斑、遮挡',
          dataSize: '50000',
          markNum: 3,
          datatype: '热成像',
          markDate: '2020-02-21',
        },
        {
          key: '1',
          index: '1',
          dataName: '标注数据二',
          markDefect: '鸟屎、亮斑、遮挡、黑晕',
          dataSize: '10000',
          markNum: 5,
          datatype: '可见光',
          markDate: '2020-02-21',

        },
        {
          key: '2',
          index: '2',
          dataName: '标注数据三',
          markDefect: '鸟屎、亮斑、遮挡',
          dataSize: '50000',
          markNum: 3,
          datatype: '可见光',
          markDate: '2020-02-21',

        },
      ],
      columns: [
        {
          title: '序号',
          dataIndex: 'index',
          width: '5%',
          editable: false,
          sorter: (a, b) => a.index - b.index,
        },
        {
          title: '数据名称',
          dataIndex: 'dataName',
          editable: true,
          width: '15%',
          ...this.getColumnSearchProps('projectName', '项目名称')
        },
        {
          title: '标注缺陷',
          dataIndex: 'markDefect',
          editable: true,
          width: '30%',
          ...this.getColumnSearchProps('projectCompany', '所属公司')
        },
        {
          title: '数据量',
          dataIndex: 'dataSize',
          width: '15%',
          editable: true,
          ...this.getColumnSearchProps('projectManager', '项目联系人')
        },
        {
          title: '分类数',
          dataIndex: 'markNum',
          width: '10%',
          editable: true,
          ...this.getColumnSearchProps('projectManager', '项目联系人')
        },
        {
          title: '数据类型',
          dataIndex: 'datatype',
          width: '12%',
          editable: true,
          ...this.getColumnSearchProps('projectManager', '项目联系人')
        },
        {
          title: '标注时间',
          dataIndex: 'markDate',
          width: '15%',
          editable: true,
          ellipsis: true,
          ...this.getColumnSearchProps('operationDate', '运维时间')
        },
        {
          title: '操作',
          dataIndex: 'operation',
          width: '13%',
          render: (text, record) =>
            this.state.dataSource.length >= 1 ? (
              <>
                <Popconfirm title="删除该条目?" onConfirm={() => this.deleteItem(record.key)}>
                  <a style={{color: 'red'}}>删除</a>
                </Popconfirm>&nbsp;&nbsp;
                <a style={{color: 'green'}}>查看</a>&nbsp;&nbsp;
              </>
            ) : null,
        },
      ],
      searchText: '',
      searchedColumn: '',
    }
  }

  addItem = () => {
    const newDataSource = this.state.dataSource[0];
    this.setState({
      dataSource: [...this.state.dataSource, newDataSource]
    })
  };
  deleteItem = (key) => {
    this.setState({
      dataSource: this.state.dataSource.filter(item => item.key !== key)
    })
  };

  saveItem = (key) => {
    console.log('key---------', key)
  };

  getColumnSearchProps = (dataIndex, title) => ({
    filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
      <div style={{padding: 8}}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`输入 ${title}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{width: 188, marginBottom: 8, display: 'block'}}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined/>}
            size="small"
            style={{width: 90}}
          >
            Search
          </Button>
          <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{width: 90}}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{color: filtered ? '#1890ff' : undefined}}/>,
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        : '',
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },
    render: text =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{backgroundColor: '#ffc069', padding: 0}}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({searchText: ''});
  };

  render() {
    const {dataSource, columns} = this.state;
    return (
      <div className={'backstage-data'}>
        <UnitTitle mainTitle={'标记数据'} subTitle={'管理'}/>
        <Row className={'content'} justify="space-around">
          <EditTable
            dataSource={dataSource} columns={columns} noAdd={false}
            hasBorder={false} addItem={this.addItem} saveItem={this.saveItem}
          />
        </Row>
      </div>
    )
  }
}

export default MarkData