/**
 * @File   : DetailsTable.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/18
 * @Desc   : 项目后台
 **/

import {Row, Col, Spin} from "antd";
import React, {Component} from "react";
import {Switch, Redirect, Route} from "react-router-dom";

import logo from "src/assets/images/logo/logo.png";
import SrcData from "./DataManage/SrcData/SrcData";
import ModelTrain from "./TrainManage/TrainManage";
import ModelManage from "./ModelManage/ModelManage";
import CheckManage from "./CheckManage/CheckManage";
import MarkData from "./DataManage/MarkData/MarkData";
import Copyright from "src/components/Copyright/Copyright";
import ModelLease from "./LeaseManage/ModelLease/ModelLease";
import UserManage from "./SystemManage/UserManage/UserManage";
import RoleManage from "./SystemManage/RoleManage/RoleManage";
import DefectType from "./DefectManage/DefectType/DefectType";
import AdminMenu from "../../components/Menu/AdminMenu/AdminMenu";
import HomeContent from "./ContentManage/HomeContent/HomeContent";
import InfoContent from "./ContentManage/InfoContent/InfoContent";
import ServiceLease from "./LeaseManage/ServiceLease/ServiceLease";
import PlatformLease from "./LeaseManage/PlatformLease/PlatformLease";
import ManageHeader from "src/components/Header/ManageHeader/ManageHeader";
import MessageContent from "./ContentManage/MessageContent/MessageContent";
import ProjectSetting from "./ProjectManage/ProjectSetting/ProjectSetting";
import ProjectReboot from "src/components/Reboot/ProjectReboot/ProjectReboot";
import PermissionManage from "./SystemManage/PermissionManage/PermissionManage";

import "./Backstage.sass";


class Backstage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: {
        collapsed: false,
        menuWidth: 3
      },
      number: 0,
      loadStatus: false,
    }
  };

  changeMenuWidth = () => {
     this.setState({
      collapsed: {
        collapsed: !this.state.collapsed.collapsed,
        menuWidth: !this.state.collapsed.collapsed ? 1 : 3
      }
    })
  };

  componentDidMount() {
    setTimeout(
      () => this.setState({
        loadStatus: !this.state.loadStatus
      }), 2000
    )
  }

  render() {
    const {collapsed, loadStatus} = this.state;
    return (
      loadStatus ?
        (
          <div className={'backstage'}>
            <ManageHeader menuWidth={collapsed.menuWidth} logo={logo} key={'adminHeader'}
                          projectName={'智能检测平台管理后台'} changeMenuWidth={this.changeMenuWidth}/>
            <Row className={'backstage-content'}>
              <Col className={'menu'} span={collapsed.menuWidth}>
                <AdminMenu collapsed={collapsed.collapsed}/>
              </Col>
              <Col className={'content'} span={24 - collapsed.menuWidth}>
                <Switch>
                  <Route exact path='/backstage/contentManage/homeContent' component={HomeContent}/>
                  <Route exact path='/backstage/contentManage/infoContent' component={InfoContent}/>
                  <Route exact path='/backstage/contentManage/messageContent' component={MessageContent}/>
                  <Route exact path='/backstage/projectManage/projectSetting' component={ProjectSetting}/>
                  <Route exact path='/backstage/leaseManage/platformLease' component={PlatformLease}/>
                  <Route exact path='/backstage/leaseManage/serviceLease' component={ServiceLease}/>
                  <Route exact path='/backstage/leaseManage/modelLease' component={ModelLease}/>
                  <Route exact path='/backstage/systemManage/userManage' component={UserManage}/>
                  <Route exact path='/backstage/systemManage/roleManage' component={RoleManage}/>
                  <Route exact path='/backstage/systemManage/permissionManage' component={PermissionManage}/>
                  <Route exact path='/backstage/dataManage/srcData' component={SrcData}/>
                  <Route exact path='/backstage/dataManage/markData' component={MarkData}/>
                  <Route exact path='/backstage/trainManage/modelTrain' component={ModelTrain}/>
                  <Route exact path='/backstage/modelManage/modelSetting' component={ModelManage}/>
                  <Route exact path='/backstage/checkManage/checkSetting' component={CheckManage}/>
                  <Route exact path='/backstage/defectManage/defectType' component={DefectType}/>
                  <Redirect to="/backstage/contentManage/homeContent" from='/backstage' exact/>
                </Switch>
              </Col>
            </Row>
            <Row>
              <ProjectReboot/>
            </Row>
          </div>
        ) : (
          <div className={'backstage-span'}>
            <Spin size={'large'} tip={'加载中...'}/>
          </div>
        )
    )
  }
}

export default Backstage