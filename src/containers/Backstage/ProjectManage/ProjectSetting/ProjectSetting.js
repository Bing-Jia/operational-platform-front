/**
 * @File   : ProjectSetting.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/19
 * @Desc   : 项目设置
 **/

import React, {Component} from "react";
import Highlighter from "react-highlight-words";
import {SearchOutlined} from "@ant-design/icons";
import {Row, Popconfirm, Input, Button, Space, Select, Table} from "antd";

import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";

import "./ProjectSetting.sass";

const {Option} = Select;


class ProjectSetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [
        {
          key: '0',
          index: '0',
          defectModel: 'YoloV5',
          projectName: '金山光伏项目一',
          projectCompany: '上海市金山区漫华路8号',
          operationDate: '2020-02-21',
          projectManager: '王五',
        },
        {
          key: '1',
          index: '1',
          defectModel: 'YoloV5',
          projectName: '金山光伏项目二',
          projectCompany: '上海市金山区漫华路8号',
          operationDate: '2020-02-20',
          projectManager: '张三',

        },
        {
          key: '2',
          index: '2',
          defectModel: 'YoloV5',
          projectName: '金山光伏项目三',
          projectCompany: '上海市金山区漫华路8号',
          operationDate: '2020-02-22',
          projectManager: '李四',

        },
      ],
      columns: [
        {
          title: '序号',
          dataIndex: 'index',
          width: '8%',
          align: 'center',
          editable: false,
          sorter: (a, b) => a.index - b.index,
        },
        {
          title: '项目名称',
          dataIndex: 'projectName',
          editable: false,
          width: '20%',
          align: 'center',
          ...this.getColumnSearchProps('projectName', '项目名称')
        },
        {
          title: '所属公司',
          dataIndex: 'projectCompany',
          editable: false,
          width: '25%',
          align: 'center',
          ...this.getColumnSearchProps('projectCompany', '所属公司')
        },
        {
          title: '项目联系人',
          dataIndex: 'projectManager',
          width: '15%',
          align: 'center',
          editable: false,
          ...this.getColumnSearchProps('projectManager', '项目联系人')
        },
        {
          title: '运维时间',
          dataIndex: 'operationDate',
          width: '17%',
          editable: false,
          ellipsis: true,
          align: 'center',
          ...this.getColumnSearchProps('operationDate', '运维时间')
        },
        {
          title: '操作',
          align: 'center',
          dataIndex: 'operation',
          width: '15%',
          render: (text, record) =>
            this.state.dataSource.length >= 1 ? (
              <>
                <Popconfirm title="删除该条目?" onConfirm={() => this.deleteItem(record.key)}>
                  <a style={{color: 'red'}}>删除</a>
                </Popconfirm>&nbsp;&nbsp;
                <a style={{color: 'green'}}>查看</a>&nbsp;&nbsp;
              </>
            ) : null,
        },
      ],
      searchText: '',
      searchedColumn: '',
    }
  }

  deleteItem = (key) => {
    this.setState({
      dataSource: this.state.dataSource.filter(item => item.key !== key)
    })
  };

  saveItem = (key) => {

  };

  getColumnSearchProps = (dataIndex, title) => ({
    filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
      <div style={{padding: 8}}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`输入 ${title}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{width: 188, marginBottom: 8, display: 'block'}}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined/>}
            size="small"
            style={{width: 90}}
          >
            Search
          </Button>
          <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{width: 90}}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{color: filtered ? '#1890ff' : undefined}}/>,
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        : '',
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },
    render: text =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{backgroundColor: '#ffc069', padding: 0}}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({searchText: ''});
  };

  render() {
    const {dataSource, columns} = this.state;
    return (
      <div className={'backstage-project-setting'}>
        <UnitTitle mainTitle={'项目设置'} subTitle={'管理'}/>
        <Row className={'setting-content'} justify="space-around">
          <Table
            dataSource={dataSource} columns={columns}
            expandable={{
              expandedRowRender: record => (
                <div className={'extra-content'}>
                  <span>检测模型设置:</span>
                  <Select defaultValue={'YoloV5'} allowClear>
                    <Option value="Yolov5-4">Yolov5-4</Option>
                    <Option value="Yolov5-6">Yolov5-6</Option>
                    <Option value="Yolov5-8">Yolov5-8</Option>
                  </Select>
                </div>)
            }}
          />
        </Row>
      </div>
    )
  }
}

export default ProjectSetting