/**
 * @File   : ModelManage.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/19
 * @Desc   : 模型管理
 **/

import React, {Component} from "react";
import {Col, Row, Table, Tabs, Button} from "antd";
import {
  FileImageOutlined,
  OrderedListOutlined,
  TagsOutlined,
  NumberOutlined,
  DribbbleOutlined
} from '@ant-design/icons';

import userAvatar from "src/assets/images/teams/qiu.jpg";
import UnitTitle from "src/components/Title/UnitTitle/UnitTitle";

import "./ModelManage.sass";

const {TabPane} = Tabs;

const columns = [
  {
    title: '头像',
    dataIndex: 'avatar',
    key: 'avatar',
    align: 'center',
    render: (record) => <img src={userAvatar} style={{width: '40px', height: '40px'}} alt=''/>
  },
  {
    title: '用户名',
    dataIndex: 'username',
    key: 'username',
    align: 'center',
  },
  {
    title: '姓名',
    dataIndex: 'fullname',
    key: 'fullname',
    align: 'center',
  },
  {
    title: '角色',
    dataIndex: 'role',
    key: 'role',
    align: 'center',
  },
  {
    title: '电话',
    key: 'phone',
    align: 'center',
    dataIndex: 'phone',
  },
  {
    title: '邮箱',
    key: 'email',
    align: 'center',
    dataIndex: 'email',
  },
];

const data = [
  {
    key: '1',
    avatar: {userAvatar},
    username: '训练模型一',
    fullname: '张三',
    role: '前端',
    phone: '18117590190',
    email: '1755241561@qq.com'
  },
  {
    key: '2',
    avatar: {userAvatar},
    username: '训练模型一',
    fullname: '张三',
    role: '前端',
    phone: '18117590190',
    email: '1755241561@qq.com'
  },
  {
    key: '3',
    avatar: {userAvatar},
    username: '训练模型一',
    fullname: '张三',
    role: '前端',
    phone: '18117590190',
    email: '1755241561@qq.com'
  },
];

class ModelManage extends Component {
  render() {
    return (
      <div className={'backstage-model'}>
        <UnitTitle mainTitle={'训练模型'} subTitle={'管理'}/>
        <Tabs className={'model-tabs'} type="card">
          <TabPane tab="模块" key="module">
            <Row className={'model-list'} gutter={[16, 16]}>
              <Col span={12}>
                <Row className={'model-unit'}>
                  <Col className={'model-info'} span={16} >
                    <h2 style={{textAlign: 'center'}}>训练模型一</h2>
                    <span><FileImageOutlined className={'file-icon'}/>图片：&nbsp;热成像</span><br/>
                    <span><OrderedListOutlined className={'class-icon'}/>分类：&nbsp;5</span><br/>
                    <span><DribbbleOutlined className={'network-icon'}/>网络：&nbsp;Yolov5</span><br/>
                    <span><OrderedListOutlined className={'iteration-icon'}/>迭代：&nbsp;300</span><br/>
                    <span><TagsOutlined className={'defect-icon'}/>缺陷：&nbsp;<span>黑晕、亮斑、遮挡、黑晕、亮斑、</span></span>
                  </Col>
                  <Col className={'model-image'} span={8}>
                    <Row>
                      <img src={userAvatar} alt="" />
                    </Row>
                    <Row className={'operation-btn'}>
                      <Col span={12}>
                        <Button type='primary'>查看</Button>
                      </Col>
                      <Col span={12}>
                        <Button type="primary" danger>删除</Button>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
              <Col span={12}>
                <Row className={'model-unit'}>
                  <Col className={'model-info'} span={16} >
                    <h2 style={{textAlign: 'center'}}>训练模型二</h2>
                    <span><FileImageOutlined className={'file-icon'}/>图片：&nbsp;热成像</span><br/>
                    <span><OrderedListOutlined className={'class-icon'}/>分类：&nbsp;5</span><br/>
                    <span><DribbbleOutlined className={'network-icon'}/>网络：&nbsp;Yolov5</span><br/>
                    <span><OrderedListOutlined className={'iteration-icon'}/>迭代：&nbsp;300</span><br/>
                    <span><TagsOutlined className={'defect-icon'}/>缺陷：&nbsp;<span>黑晕、亮斑、遮挡、黑晕、亮斑、</span></span>
                  </Col>
                  <Col className={'model-image'} span={8}>
                    <Row>
                      <img src={userAvatar} alt="" />
                    </Row>
                    <Row className={'operation-btn'}>
                      <Col span={12}>
                        <Button type='primary'>查看</Button>
                      </Col>
                      <Col span={12}>
                        <Button type="primary" danger>删除</Button>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
              <Col span={12}>
                <Row className={'model-unit'}>
                  <Col className={'model-info'} span={16} >
                    <h2 style={{textAlign: 'center'}}>训练模型三</h2>
                    <span><FileImageOutlined className={'file-icon'}/>图片：&nbsp;热成像</span><br/>
                    <span><OrderedListOutlined className={'class-icon'}/>分类：&nbsp;5</span><br/>
                    <span><DribbbleOutlined className={'network-icon'}/>网络：&nbsp;Yolov5</span><br/>
                    <span><OrderedListOutlined className={'iteration-icon'}/>迭代：&nbsp;300</span><br/>
                    <span><TagsOutlined className={'defect-icon'}/>缺陷：&nbsp;<span>黑晕、亮斑、遮挡、黑晕、亮斑、</span></span>
                  </Col>
                  <Col className={'model-image'} span={8}>
                    <Row>
                      <img src={userAvatar} alt="" />
                    </Row>
                    <Row className={'operation-btn'}>
                      <Col span={12}>
                        <Button type='primary'>查看</Button>
                      </Col>
                      <Col span={12}>
                        <Button type="primary" danger>删除</Button>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
              <Col span={12}>
                <Row className={'model-unit'}>
                  <Col className={'model-info'} span={16} >
                    <h2 style={{textAlign: 'center'}}>训练模型四</h2>
                    <span><FileImageOutlined className={'file-icon'}/>图片：&nbsp;热成像</span><br/>
                    <span><OrderedListOutlined className={'class-icon'}/>分类：&nbsp;5</span><br/>
                    <span><DribbbleOutlined className={'network-icon'}/>网络：&nbsp;Yolov5</span><br/>
                    <span><OrderedListOutlined className={'iteration-icon'}/>迭代：&nbsp;300</span><br/>
                    <span><TagsOutlined className={'defect-icon'}/>缺陷：&nbsp;<span>黑晕、亮斑、遮挡、黑晕、亮斑、</span></span>
                  </Col>
                  <Col className={'model-image'} span={8}>
                    <Row>
                      <img src={userAvatar} alt="" />
                    </Row>
                    <Row className={'operation-btn'}>
                      <Col span={12}>
                        <Button type='primary'>查看</Button>
                      </Col>
                      <Col span={12}>
                        <Button type="primary" danger>删除</Button>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>
          </TabPane>
          <TabPane tab="列表" key="list">
            <Table bordered={true} columns={columns} dataSource={data}/>
          </TabPane>
        </Tabs>
      </div>
    )
  }
}

export default ModelManage