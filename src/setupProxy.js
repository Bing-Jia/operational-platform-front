/**
 *开发模式下跨域配置
 */

const {createProxyMiddleware} = require('http-proxy-middleware');

module.exports = function (app) {
  app.use(
    '/api',
    createProxyMiddleware(
      {
        target: "http://127.0.0.1:8001/api/",
        pathRewrite: {
          '^/api': ''
        },
        changeOrigin: true
      })
  );

  // app.use(
  //   '/static',
  //   createProxyMiddleware(
  //     {
  //       target: "http://127.0.0.1:8001/static/",
  //       pathRewrite: {
  //         '^/static': ''
  //       },
  //       changeOrigin: true
  //     })
  // );

  app.use(
    '/reboot',
    createProxyMiddleware(
      {
        target: "https://api.ownthink.com",
        changeOrigin: true
      })
  );
  app.use(
    '/maps',
    createProxyMiddleware(
      {
        target: "http://lskxhp.cn",
        changeOrigin: true
      })
  )

};

