/**
 * @File   : store.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/4
 * @Desc   : redux 对象管理
 **/

import {createStore, applyMiddleware} from 'redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from './reducers'

// 向外暴露store对象
export default createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk, logger)))