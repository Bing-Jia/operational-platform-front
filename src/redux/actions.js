/**
 * @File   : action.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/4
 * @Desc   : 定义所有的action
 **/

import * as action_type from './actionTypes'


/**
 * 用户
 **/


// 用户登录
export const loginUser = (userInfo) => {
  return {
    type: action_type.LOGIN_USER,
    userInfo
  }
};

// 用户登出
export const logoutUser = () => {
  return {
    type: action_type.LOGOUT_USER,
  }
};


// 删除用户
export const deleteUserById = (userId) => {
  return {
    type: action_type.DELETE_USER_BY_ID,
    userId
  }
};
export const deleteUserByUserName = (userName) => {
  return {
    type: action_type.DELETE_USER_BY_USERNAME,
    userName
  }
};

// 所有用户
export const getAllUsers = () => {
  return {
    type: action_type.GET_ALL_USERS
  }
};

// 编辑用户
export const editUserById = (userId) => {
  return {
    type: action_type.EDIT_USER_BY_ID,
    userId
  }
};

/**
 * 官网
 * */

// 轮播图
export const getWebsiteScrollImage = (data) => ({
  type: action_type.GET_WEBSITE_SCROLL_IMAGE_LIST,
  data
});

// 特征
export const getWebsiteFeatureList = (data) => ({
  type: action_type.GET_WEBSITE_FEATURE_LIST,
  data
});

// 设备
export const getWebsiteEquipmentData = (data) => ({
  type: action_type.GET_WEBSITE_EQUIPMENT_DATA,
  data
});

// 特征组
export const getWebsiteFeaturesList = (data) => ({
  type: action_type.GET_WEBSITE_FEATURES_DATA,
  data
});

// 团队
export const getWebsiteTeamsData = (data) => ({
  type: action_type.GET_WEBSITE_TEAMS_DATA,
  data
});

// 成果
export const getWebsiteResultsData = (data) => ({
  type: action_type.GET_WEBSITE_RESULTS_DATA,
  data
});

// 租赁
export const getWebsiteLeasesData = (data) => ({
  type: action_type.GET_WEBSITE_LEASES_DATA,
  data
});


/**
 * 项目
 * */

// 添加项目
export const addProject = (projectData) => {
  return {
    type: action_type.ADD_PROJECT,
    projectData
  }
};

// 删除项目
export const deleteProject = (projectId) => {
  return {
    type: action_type.DELETE_PROJECT,
    projectId
  }
};

// 获取所有项目列表
export const getAllProjects = (projectsData) => {
  return {
    type: action_type.GET_ALL_PROJECTS,
    projectsData
  }
};

// 通过类型获取项目
export const getProjectsByType = (projectsData) => {
  return {
    type: action_type.GET_PROJECTS_BY_TYPE,
    projectsData
  }
};


/**
 * 缺陷
 * */

// 获取缺陷列表
export const getDefectTypes = (defectTypesData) => {
  return {
    type:action_type.GET_DEFECT_TYPES,
    defectTypesData
  }
};





