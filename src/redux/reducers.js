/**
 * @File   : reducers.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/4
 * @Desc   : 定义所有纯reducer
 **/

import {combineReducers} from 'redux'
import isEmpty from 'lodash/isEmpty'
import * as action_type from './actionTypes'

import pv from 'src/assets/images/banner/pv.jpg'
import fan from 'src/assets/images/banner/fan.jpg'
import map from 'src/assets/images/banner/map.png'

import uav from 'src/assets/images/feature/无人机.png'
import tensorflow from 'src/assets/images/feature/TensrFlow.png'
import report from 'src/assets/images/feature/报告.png'

import advice from 'src/assets/images/feature/advice.png'
import ai from 'src/assets/images/feature/AI.png'
import check from 'src/assets/images/feature/check.png'
import map1 from 'src/assets/images/feature/map.png'
import feature from 'src/assets/images/feature/feature.png'

import qiu from 'src/assets/images/teams/qiu.jpg'
import chun from 'src/assets/images/teams/chun.jpg'
import wangye from 'src/assets/images/teams/wangye.jpeg'
import chulan from 'src/assets/images/teams/zhangchulan.jpg'
import baobao from 'src/assets/images/teams/fengbaobao.png'

import users from 'src/assets/images/result/团队.png'
import model from 'src/assets/images/result/模型.png'
import data from 'src/assets/images/result/数据.png'
import defects from 'src/assets/images/result/缺陷.png'
import project from 'src/assets/images/result/项目.png'
import equipment_image_1 from "src/assets/images/equipment/1.jpg";
import equipment_image_2 from "src/assets/images/equipment/2.jpg";
import equipment_image_3 from "src/assets/images/equipment/3.jpg";


/**
 * 用户认证
 * */

const userState = {
  fullName: localStorage.getItem('userName'),
  isAuthenticated: !isEmpty(localStorage.getItem('Authorization'))
};

const user = (state = userState, action) => {
  switch (action.type) {
    case action_type.LOGIN_USER:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.userInfo),
        fullName: action.userInfo.full_name
      };
    case action_type.LOGOUT_USER:
      return {
        ...state,
        isAuthenticated: false,
        fullName: null
      };
    default:
      return state
  }
};


/**
 * 官网
 * */

const websiteInitState = {
  total: 100,
  bannerInitState: [
    {
      "title": "达到",
      "content": "90%  准确率",
      "image": pv,
      "map": map,
    },
    {
      "title": "多个",
      "content": "数据特征库",
      "image": fan,
      "map": map,
    }
  ],
  featureInitState: [
    {
      image: uav,
      title: "无人机巡检",
      color: 'rgb(14, 144, 210)',
      content: "利用工业级无人机搭载可见光、热红外双光镜头,在光伏厂区规划航线 长航时自动飞行,采集高精度、高稳定性的光伏图片数据。"
    },
    {
      image: tensorflow,
      title: "目标检测",
      color: 'lightcoral',
      content: " 利用卷积神经网络CNN提取图片中物体的特征信息,利用线性回归拟合物体偏移信息, 从而推断被检测图片的中物体的类别以及位置信息。"
    },
    {
      image: report,
      title: "光伏报告",
      color: 'mediumaquamarine',
      content: "基于专家认定的光伏缺陷特征,结合大样本训练的检测模型推断被检测数据的缺陷情况生成具有一定 参考意义的光伏缺陷检测报告。"
    },
  ],
  equipmentInitState: {
    title: '利用',
    content: '专业设备',
    leftImage: equipment_image_1,
    rightTopImage: equipment_image_2,
    rightBottomImage: equipment_image_3
  },
  featuresInitState: {
    title: '探索',
    content: '更多特色',
    features: [
      {
        image: ai,
        title: "人工智能",
      },
      {
        image: map1,
        title: "光伏地图",
      },
      {
        image: feature,
        title: "数据特征库",
      },
      {
        image: check,
        title: "模型检测",
      },
      {
        image: advice,
        title: "分析建议",
      },
    ]
  },
  teamsInitState: {
    title: '汇聚',
    content: '技术成员',
    teams: [
      {
        image: baobao,
        name: "冯宝宝",
        role: "前端",
        description: '基于react框架的项目开发经验，参与多个前后端分离前端项目的研发'
      }, {
        image: wangye,
        name: "王也",
        role: "后端",
        description: '基于django、以及django rest framework框架的开发经验以及技术积累'
      }, {
        image: chulan,
        name: "张楚岚",
        role: "飞手",
        description: '熟练飞行大疆旗下多个产品的飞行经验，比如精灵4、经纬M300RTK等型号'
      }, {
        image: chun,
        name: "椿",
        role: "算法",
        description: '熟练多种目标检测方法，faster-rcnn以及yoloV5等，训练多个检测模型'
      }, {
        image: qiu,
        name: "湫",
        role: "项目经理",
        description: '参与实施多个项目的前期功能分析以及后期开发、协调、测试、上线工作'
      }
    ]
  },
  resultsInitState: {
    title: '分享',
    content: '技术成果',
    results: [
      {
        image: project,
        title: "上传项目",
        number: 5
      }, {
        image: users,
        title: "参加用户",
        number: 30
      }, {
        image: model,
        title: "训练模型",
        number: 10
      }, {
        image: data,
        title: "拥有数据",
        number: 2020
      }, {
        image: defects,
        title: "检测缺陷",
        number: 1314
      },
    ]
  },
  leasesInitState: {
    title: '租赁',
    content: '训练模型',
    leases: [
      {
        name: '模型',
        price: "￥10000",
        cycle: "每月",
        contentList: [
          "2种热成像光伏检测", "5000张训练数据集"
        ],
        url: '/'
      }, {
        name: '平台',
        price: "￥10000",
        cycle: "每月",
        contentList: [
          "2种热成像光伏检测", "5000张训练数据集"
        ],
        url: '/'
      }, {
        name: '服务',
        price: "￥10000",
        cycle: "每月",
        contentList: [
          "2种热成像光伏检测", "5000张训练数据集"
        ],
        url: '/'
      }
    ]
  },
  newsInitState: {
    title: '发布',
    content: '新闻资讯',
    news: [
      {
        image: baobao,
        date: "1 day ago",
        title: '后补贴时代 光伏+的猜想',
        fromPlace: '北极星电力网',
        content: '近年来，随着补贴退坡，光伏产业早已不再是单纯地依靠发电+补贴盈利，而是呈现\n' +
          '                                            多样化的特点，在应用场景上与不同行业跨界融合的趋势愈发凸显。光伏+时代呼之欲出。'
      }, {
        image: wangye,
        date: "2 day ago",
        title: '产能出海！光伏扩产潮加速行业洗牌',
        fromPlace: '北极星电力网',
        content: "疫情之下，光伏市场逆全球化思潮涌动，一时间'重新培育海外供应链，实现制造本土化'成为海外市场的重要话题，光伏制造业'外迁'形势不断显现。"
      }, {
        image: null,
        date: "2 days ago",
        title: '重新认识“新能源+储能”',
        fromPlace: '北极星电力网',
        content: '截止到2019年底，我国风电装机达到2.1亿千瓦、光伏发电装机2.04亿千瓦，提前一年完成可再生能源发展“十三五”规划目标。'
      }, {
        image: chun,
        date: "10 hours ago",
        title: '后补贴时代 光伏+的猜想',
        fromPlace: '北极星电力网',
        content: '近年来，随着补贴退坡，光伏产业早已不再是单纯地依靠发电+补贴盈利，而是呈现\n' +
          '                                            多样化的特点，在应用场景上与不同行业跨界融合的趋势愈发凸显。光伏+时代呼之欲出。'
      }, {
        image: qiu,
        date: "2 day ago",
        title: '产能出海！光伏扩产潮加速行业洗牌',
        fromPlace: '北极星电力网',
        content: "疫情之下，光伏市场逆全球化思潮涌动，一时间'重新培育海外供应链，实现制造本土化'成为海外市场的重要话题，光伏制造业'外迁'形势不断显现。"
      }
    ]
  }
};

const userInitState = {
  info: [],
  total: 5,
  radio: '50%',
};

const userList = (state = userInitState, action) => {
  switch (action.type) {
    default:
      return state
  }
};


const initialProjectsState = {
  count: 0,
  next: null,
  previous: null,
  results: [],
  radio: '0%',
};

const projects = (state = initialProjectsState, action) => {
  switch (action.type) {
    case action_type.GET_ALL_PROJECTS:
      return action.projectsData;
    case action_type.GET_PROJECTS_BY_TYPE:
      return action.projectsData;
    default:
      return state
  }
};

const dataInitState = {
  info: [
    {
      "size": 1000,
      "dataType": "thermal",
      "projectType": "PV"
    },
    {
      "size": 1000,
      "dataType": "visible",
      "projectType": "Fan"
    }
  ],
  total: 1314,
  radio: '10%',
};

const dataList = (state = dataInitState, action) => {
  switch (action.type) {
    default:
      return state
  }
};

const defectTypeInitState = {
  count: 0,
  next: null,
  previous: null,
  results: [],
  radio: '0%',

};

const defectTypeList = (state = defectTypeInitState, action) => {
  switch (action.type) {
    case action_type.GET_DEFECT_TYPES:
      return {
        ...state,
        count: action.defectTypesData.count,
        next: action.defectTypesData.next,
        previous: action.defectTypesData.previous,
        results: action.defectTypesData.results
      };
    default:
      return state
  }
};

const defectInitState = {
  info: [
    {
      "defectName": "",
      "defectType": "",
      "thermal": "",
      "visible": "",
      "projectType": "PV"
    },
    {
      "defectName": "",
      "defectType": "",
      "thermal": "",
      "visible": "",
      "projectType": "Fan"
    }
  ],
  total: 325,
  radio: '30%',
};

const defectList = (state = defectInitState, action) => {
  switch (action.type) {
    default:
      return state
  }
};

const bannerList = (state = websiteInitState.bannerInitState, action) => {
  switch (action.type) {
    case action_type.GET_WEBSITE_SCROLL_IMAGE_LIST:
      return {
        ...state
      };
    default:
      return state
  }
};

const featureList = (state = websiteInitState.featureInitState, action) => {
  switch (action.type) {
    case action_type.GET_WEBSITE_FEATURE_LIST:
      return {
        ...state
      };
    default:
      return state
  }
};

const equipmentData = (state = websiteInitState.equipmentInitState, action) => {
  switch (action.type) {
    case action_type.GET_WEBSITE_EQUIPMENT_DATA:
      return {
        ...state
      };
    default:
      return state
  }
};

const featuresData = (state = websiteInitState.featuresInitState, action) => {
  switch (action.type) {
    case action_type.GET_WEBSITE_FEATURES_DATA:
      return {
        ...state
      };
    default:
      return state
  }
};

const teamsData = (state = websiteInitState.teamsInitState, action) => {
  switch (action.type) {
    case action_type.GET_WEBSITE_TEAMS_DATA:
      return {
        ...state
      };
    default:
      return state
  }
};

const resultsData = (state = websiteInitState.resultsInitState, action) => {
  switch (action.type) {
    case action_type.GET_WEBSITE_RESULTS_DATA:
      return {
        ...state
      };
    default:
      return state
  }
};

const leasesData = (state = websiteInitState.leasesInitState, action) => {
  switch (action.type) {
    case action_type.GET_WEBSITE_LEASES_DATA:
      return state;
    default:
      return state
  }
};

const newsData = (state = websiteInitState.newsInitState, action) => {
  switch (action.type) {
    case action_type.GET_WEBSITE_NEWS_DATA:
      return {
        ...state
      };
    default:
      return state
  }
};

const domainData = combineReducers({
  user,
  bannerList,
  featureList,
  equipmentData,
  featuresData,
  teamsData,
  resultsData,
  leasesData,
  newsData,

  projects,
  userList,
  dataList,
  defectList,
  defectTypeList,
});

// const appState = combineReducers({});
//
// const uiState = combineReducers({});

const rootReducer = combineReducers({
  domainData: domainData,
  // appState: appState,
  // uiState: uiState,
});

export default rootReducer;