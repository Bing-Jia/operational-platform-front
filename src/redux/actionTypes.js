/**
 * @File   : actionTypes.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/4
 * @Desc   : 定义所有的action宏
 **/

/**
 * 用户
 **/

// 用户登录
export const LOGIN_USER = 'LOGIN_USER';

// 用户登出
export const LOGOUT_USER = 'LOGOUT_USER';

// 删除用户
export const DELETE_USER_BY_ID = 'DELETE_USER_BY_ID';
export const DELETE_USER_BY_USERNAME = 'DELETE_USER_BY_USERNAME';

// 所有用户
export const GET_ALL_USERS = 'GET_ALL_PROJECTS';

// 编辑用户
export const EDIT_USER_BY_ID = 'EDIT_USER_BY_ID';



/**
 * 官网
 * */

// 轮播图
export const GET_WEBSITE_SCROLL_IMAGE_LIST = 'GET_WEBSITE_SCROLL_IMAGE_LIST';

// 特征
export const GET_WEBSITE_FEATURE_LIST = 'GET_WEBSITE_FEATURE_LIST';

// 设备
export const GET_WEBSITE_EQUIPMENT_DATA = 'GET_WEBSITE_EQUIPMENT_DATA';

// 特征组
export const GET_WEBSITE_FEATURES_DATA = 'GET_WEBSITE_FEATURES_DATA';

// 团队
export const GET_WEBSITE_TEAMS_DATA = 'GET_WEBSITE_TEAMS_DATA';

// 成果
export const GET_WEBSITE_RESULTS_DATA = 'GET_WEBSITE_RESULTS_DATA';

// 租赁
export const GET_WEBSITE_LEASES_DATA = 'GET_WEBSITE_LEASES_DATA';

// 新闻
export const GET_WEBSITE_NEWS_DATA = 'GET_WEBSITE_NEWS_DATA';



/**
 * 项目
 * */

// 添加项目
export const ADD_PROJECT = 'ADD_PROJECT';

// 删除项目
export const DELETE_PROJECT = 'DELETE_PROJECT';

//所有项目
export const GET_ALL_PROJECTS = 'GET_ALL_PROJECTS';

//获取项目列表通过项目类型
export const GET_PROJECTS_BY_TYPE = 'GET_PROJECTS_BY_TYPE';

/**
 * 缺陷
 * */

// 获取缺陷列表
export const GET_DEFECT_TYPES = 'GET_DEFECT_TYPES';

