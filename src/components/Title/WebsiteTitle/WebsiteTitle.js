/**
 * 主副标题组件
 * */

import React from "react";
import {Col, Row} from "antd/lib/index";

import "./WebsiteTitle.sass";


function WebsiteTitle(props) {
  return (
    <Row className={'website-title'} justify='center'>
      <Col span={20}>
        <h4 style={{color: `${props.color}`}}>
          <span style={{color: `${props.color}`}}>
              {props.title}
            </span>
          {props.content}
        </h4>
      </Col>
    </Row>
  )
}

export default WebsiteTitle