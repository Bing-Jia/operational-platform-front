/**
 * @File   : UnitTitle.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/1
 * @Desc   : 单元标题组件
 **/

import React from "react";

import "./UnitTitle.sass";

function UnitTitle(props) {
  return (
    <div className='unit-title' style={{backgroundColor:props.color}}>
      <h3>
        {props.mainTitle}
        <span>{props.subTitle}</span>
      </h3>
      <hr/>
    </div>
  )
}

export default UnitTitle