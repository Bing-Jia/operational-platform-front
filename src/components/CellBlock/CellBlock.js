/**
 * @File   : CellBlock.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/3
 * @Desc   : 项目列表单元块
 **/
import React from 'react'
import {PageHeader, Tag, Row, Button, Col, Statistic} from "antd";

import datas from 'src/assets/images/result/数据.png'
import defects from 'src/assets/images/result/缺陷.png'
import projects from 'src/assets/images/result/项目.png'

import "./CellBlock.sass";

const Content = ({children, extraContent}) => {
  return (
    <Row justify="space-around" align="middle">
      <Col span={17}>
        <div style={{flex: 1}}>{children}</div>
      </Col>
      <Col span={6}>
        <div className="image">{extraContent}</div>
      </Col>
    </Row>
  );
};

function CellBlock(props) {
  return (
    <div className={'cell-block'} style={{borderRadius: '15px'}}>
      <PageHeader
        title={props.data.title}
        tags={<Tag color='blue'>运行中</Tag>}
        extra={[<Button key='1' type={'primary'} href={props.data.url}>进入</Button>]}
        avatar={{src: props.data.logo}}
        style={{backgroundColor: `${props.data.color ? props.data.color : 'white'}`, borderRadius: '5px'}}
      >
        <Content className={'content'}
          extraContent={
            <img
              src={props.data.image}
              alt="content"
              width="100%"
            />
          }
        >
          {props.data.description}
        </Content>
        <Row justify="center" className={'project-info'}>
          <Col className={'info'} span={6} style={{textAlign: 'center'}}>
            <Statistic title={<><img src={projects} alt=""/>项目数</>}
                       value={props.data.projectNum}/>
          </Col>
          <Col className={'info'} span={6} style={{textAlign: 'center'}}>
            <Statistic title={<><img src={datas} alt=""/>&nbsp;&nbsp;数据数</>}
                       value={props.data.imageNum}/>
          </Col>
          <Col className={'info'} span={6} style={{textAlign: 'center'}}>
            <Statistic title={<><img src={defects} alt=""/>&nbsp;&nbsp;缺陷数</>}
                       value={props.data.defectNum}/>
          </Col>
        </Row>
      </PageHeader>
    </div>
  )
}

export default CellBlock