/**
 * @File   : TableWithClick.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/1
 * @Desc   : table带有点击事件组件
 **/

import {Table} from "antd";
import React, {Component} from "react";

import "./TableWithClick.sass";


class TableWithClick extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    const {columns, data} = this.props;
    return (
      <div className={'table-with-click'} >
        <Table className={'table'} columns={columns} dataSource={data} scroll={{y:160}}/>
      </div>
    )
  }
}

export default TableWithClick