/**
 * @File   : ManageHeader.js * @Author : xue.xiaoBing
 * @Date   : 2020/12/8
 * @Desc   : 项目详细 头部组件
 **/

import PropTypes from "prop-types";
import {connect} from "react-redux";
import React, {Component} from "react";
import {Row, Col} from "antd/lib/index";
import {MenuOutlined,} from '@ant-design/icons';

import UserAvatar from "src/components/UserAvatar/UserAvatar";

import './ManageHeader.sass';


class ManageHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  change = () => {
    this.props.changeMenuWidth()
  };

  render() {
    const {user} = this.props;
    const {logo, menuWidth, projectName} = this.props;
    return (
      <Row className={'manage-header'} align={'middle'}>
        <Col span={menuWidth} className={'header-logo'}>
          <img src={logo} alt=""/>
        </Col>
        <Col span={1}>
          <MenuOutlined onClick={this.change}/>
        </Col>
        <Col span={20 - menuWidth} className={'header-content'}>
          <h1>{projectName}</h1>
        </Col>
        <Col span={3}>
          <UserAvatar username={user.fullName}/>
        </Col>
      </Row>
    )
  }
}

ManageHeader.propTypes = {
  user: PropTypes.object
};

const mapStateToProps = (state) => {
  return ({
    user: state.domainData.user
  })
};

export default connect(mapStateToProps, null)(ManageHeader)