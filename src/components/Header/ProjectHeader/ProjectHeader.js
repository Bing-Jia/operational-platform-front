/**
 * @File   : NavigationProjects.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/3
 * @Desc   : 导航栏组件
 **/

import PropTypes from "prop-types";
import {connect} from "react-redux";
import {logout} from "src/api/request";
import React, {Component} from "react";
import {Row, Col} from "antd/lib/index";
import {NavLink} from "react-router-dom";

import UserAvatar from "src/components/UserAvatar/UserAvatar";

import "./ProjectHeader.sass";


class ProjectHeader extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    const {user} = this.props;
    const {logo, title, navList} = this.props.data;
    return (
      <div className={'project-header'}>
        <Row justify='center' gutter={16} >
          <Col span={4} className={'header-logo'}>
            <img src={logo} alt="" />
            <span>{title}</span>
          </Col>
          <Col span={19} className={'header-content'}>
            <ul>
              {
                navList.map((data, index) =>
                  <li key={index}>
                    <NavLink to={data["url"]}>
                      {data["name"]}
                    </NavLink>
                  </li>)
              }
              <li>
                <UserAvatar username={user.fullName}/>
              </li>
            </ul>
          </Col>
        </Row>
      </div>
    )
  }
}

ProjectHeader.propTypes = {
  user: PropTypes.object
};

const mapStateToProps = (state) => {
  return ({
    user: state.domainData.user
  })
};


export default connect(mapStateToProps, {logout})(ProjectHeader)