/**
 * @File   : request.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/4
 * @Desc   : 官网header
 **/

import PropTypes from "prop-types";
import {connect} from "react-redux";
import React, {Component} from "react";
import intl from "react-intl-universal";
import {NavLink} from "react-router-dom";
import {Row, Col, Menu, Select} from "antd";


import {
  AppstoreOutlined,
  BarsOutlined,
  DatabaseOutlined,
  HomeOutlined,
  UserOutlined,
  AreaChartOutlined,
  LogoutOutlined,
} from '@ant-design/icons';

import {logout} from 'src/api/request';
import {emit} from "src/utils/emit.js";
import logo from "src/assets/images/logo/logo.png";


import "./WebsiteHeader.sass";


const {SubMenu} = Menu;
const {Option} = Select;

class WebsiteHeader extends Component {
  handleChange = val => {
    emit.emit('change_language', val);
  };

  onLogout = () => {
    this.props.logout()
  };

  render() {
    const {user} = this.props;
    return (
      <div className={'website-header'}>
        <Row className={'content'} align={'middle'}>
          <Col span={6} className={'header-left'}>
            <Row align={'middle'}>
              <Col span={10} offset={1}>
                <img src={logo} alt="logo" className={'img-logo'}/>
              </Col>
              <Col span={11}>
                <span className={'platform-name'}>{intl.get('platformName') || '智能运维平台'}</span>
              </Col>
            </Row>
          </Col>
          <Col span={18} className={'header-right'}>
            <Row align={'middle'}>
              <Menu onClick={this.handleClick} selectedKeys={['home']} mode="horizontal" theme='dark'>
                  <Menu.Item key="home" icon={<HomeOutlined/>}>
                    {/*{intl.get('home')}*/}
                    首页
                  </Menu.Item>
                  <SubMenu icon={<BarsOutlined/>} title={'特征'} style={{position: 'inline'}}>
                    <Menu.Item key="setting:1" icon={<HomeOutlined/>}>
                      <NavLink to="/login">无人机</NavLink>
                    </Menu.Item>
                    {/*<Menu.Item key="setting:2" icon={<AreaChartOutlined/>}>{intl.get('target detection')}</Menu.Item>*/}
                    <Menu.Item key="setting:2" icon={<AreaChartOutlined/>}>目标检测</Menu.Item>
                    <Menu.Item key="setting:3" icon={<DatabaseOutlined/>}>智能报告</Menu.Item>
                  </SubMenu>
                  <SubMenu icon={<BarsOutlined/>} title={'功能'} style={{position: 'inline'}}>
                    <Menu.Item key="setting:1" icon={<HomeOutlined/>}>
                      <NavLink to="/overview">光伏首页</NavLink>
                    </Menu.Item>
                    <Menu.Item key="setting:2" icon={<AreaChartOutlined/>}>
                      <NavLink to="/backstage">
                        管理后台
                      </NavLink>
                    </Menu.Item>
                    <Menu.Item key="setting:3" icon={<DatabaseOutlined/>}>管理数据</Menu.Item>
                  </SubMenu>
                  <Menu.Item key="news" icon={<AppstoreOutlined/>}>
                    <NavLink to="/classify">
                      {/*{intl.get('operations module')}*/}
                      运维模块
                    </NavLink>
                  </Menu.Item>
                  <Menu.Item key="language" className={'platform-language'} selectable={false}>
                    <Select defaultValue="中文" onChange={this.handleChange.bind(this)}>
                      <Option value="zh-CN">中文</Option>
                      <Option value="en-US">English</Option>
                    </Select>
                  </Menu.Item>
                  {
                    user.fullName ?
                      <SubMenu icon={<UserOutlined/>} className={'platform-user'} title={user.fullName}>
                        <Menu.Item key="setting:1" icon={<HomeOutlined/>}>用户资料</Menu.Item>
                        <Menu.Item key="setting:2" icon={<AreaChartOutlined/>}>修改密码</Menu.Item>
                        <Menu.Item key="setting:3" icon={<DatabaseOutlined/>}>用户信息</Menu.Item>
                        <Menu.Item key="setting:3" icon={<LogoutOutlined/>} onClick={this.onLogout}>退出</Menu.Item>
                      </SubMenu> :
                      <Menu.Item icon={<UserOutlined/>} className={'platform-user'}>
                        <NavLink to="/login">
                          登录
                        </NavLink>
                      </Menu.Item>
                  }
                </Menu>
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}


WebsiteHeader.propTypes = {
  user: PropTypes.object
};

const mapStateToProps = (state) => {
  return ({
    user: state.domainData.user
  })
};


export default connect(mapStateToProps, {logout})(WebsiteHeader)