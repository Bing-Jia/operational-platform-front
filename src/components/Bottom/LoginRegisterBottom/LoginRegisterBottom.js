/**
 * @File   : LoginRegisterBottom.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/3
 * @Desc   : 登录注册底部
 **/

import React  from "react";
import logo from "src/assets/images/logo/operation_logo.png";

import "./LoginRegisterBottom.sass";

export default function LoginRegisterBottom (){
    return (
      <div className='login-register-bottom'>
        <img src={logo} alt="运维logo"/><br/>
        <span>无人机光伏智能运维</span>
      </div>
    )
}
