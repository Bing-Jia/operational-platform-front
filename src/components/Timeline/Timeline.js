/**
 * @File   : Timeline.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/17
 * @Desc   : 时间线组件
 **/

import {Timeline} from "antd";
import React, {Component} from "react";

import pv from "src/assets/images/projects/光伏板.png";
import pv_image from "src/assets/images/banner/pv.jpg";
import CellBlock from "src/components/CellBlock/CellBlock";

const PV = {
  "logo": pv,
  "title": '无人机智能光伏运维',
  "image": pv_image,
  "url": '/pv',
  "description": "无人机智能光伏运维利用无人机搭载可见光和热成像图片采集设备，通过标注、训练的缺陷" +
    "模型来识别光伏组件的可见光和热成像的缺陷并生成缺陷报告和光伏地图等相关信息。",
  "projectNum": 3,
  "imageNum": 2020,
  "defectNum": 103,
  "color": "skyblue"
};

class TimelineComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return (
      <Timeline mode={'alternate'} style={{maxHeight: "700px"}}>
          <Timeline.Item color="red">
            <CellBlock data={PV}/>
          </Timeline.Item>
          <Timeline.Item style={{position: 'relative', top: '-150px'}}>
            <CellBlock data={PV}/>
          </Timeline.Item>
          <Timeline.Item color="gray" style={{position: 'relative', top: '-200px'}}>
            <CellBlock data={PV}/>
          </Timeline.Item>
          <Timeline.Item color="gray" style={{position: 'relative', top: '-350px'}}>
            <CellBlock data={PV}/>
          </Timeline.Item>
      </Timeline>
    )
  }
}

export default TimelineComponent