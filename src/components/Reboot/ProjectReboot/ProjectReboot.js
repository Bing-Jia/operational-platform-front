/**
 * @File   : ProjectReboot.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/15
 * @Desc   : 项目机器人组件
 **/

import {Popover} from "antd";
import React, {Component} from "react";
import RebootDialogBox from "./RebootDialogBox";
import reboot from "src/assets/images/reboot.png"

import "./ProjectReboot.sass";

export default class ProjectReboot extends Component {
  state = {
    visible: false,
  };

  hide = () => {
    this.setState({
      visible: false,
    });
  };

  handleVisibleChange = visible => {
    this.setState({visible});
  };

  render() {
    return (
       <Popover
        title="问答机器人"
        trigger="click"
        placement="leftTop"
        visible={this.state.visible}
        content={<RebootDialogBox />}
        onVisibleChange={this.handleVisibleChange}
      >
        <div className={'reboot-icon'}>
            <img src={reboot} alt="问答机器人"/>
        </div>
      </Popover>
    )
  }
}