/**
 * @File   : RebootDialogBox.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/15
 * @Desc   : 机器人对话框
 **/

import React, {Component} from 'react'
import {Button, Col, Row} from "antd/lib/index";
import userAvatar from "src/assets/images/qiu.jpg";
import userAvatar1 from "src/assets/images/chun.jpg";
import {connect} from "react-redux";
import {sendMessageToReboot} from "src/api/request";

import "./RebootDialogBox.sass";

class RebootDialogBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      send_message: '',
      messages: ['嗨，好久不见！'],
    }
  };

  handleTextareaChange = (e) => {
    this.setState({
      send_message: e.target.value,
    })
  };

  sendMessage = (e, data) => {
    if (data === 'btn' || e.keyCode === 13) {
      // 更新状态
      this.setState({
        send_message: '',
        messages: [...this.state.messages, this.state.send_message]
      });

      // todo 发送消息
      this.props.sendMessageToReboot(this.state.send_message)
        .then(
          (res) => {
            if (res.data.data.messages === "error") {
              this.setState({
                messages: [...this.state.messages, '不好意思，这个问题太难了...']
              });
            } else {
              const ret = res.data.data.info.text;
              this.setState({
                messages: [...this.state.messages, ret]
              });
            }
          }
        )
        .catch(() => {
          this.setState({
            messages: [...this.state.messages, '不好意思，这个问题太难了...']
          });
        })
    }

  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    // todo 滚动到底部
    const element = document.getElementById('rebootContent');
    element.scrollTop = element.scrollHeight - element.clientHeight
  }

  render() {
    const {messages, send_message} = this.state;
    return (
      <div className={'reboot-box'}>
        <Row className={'dialog'}>
          <div className='reboot-content' id='rebootContent'>
            {messages.map((item, index) => {
              if (index % 2 === 0) {
                return (
                  <Row className={'reboot'} key={index}>
                    <Col className={'avatar'} span={3}>
                      <img src={userAvatar} alt=""/>
                    </Col>
                    <Col className={'content'} span={21}>
                      <div key={index}>
                        <p>{item}</p>
                      </div>
                    </Col>
                  </Row>
                )
              } else {
                return (
                  <Row className={'person'}>
                    <Col className={'content'} span={21}>
                      <div key={index}>
                        <p>{item}</p>
                      </div>
                    </Col>
                    <Col className={'avatar'} span={3}>
                      <img src={userAvatar1} alt=""/>
                    </Col>
                  </Row>
                )
              }
            })}
          </div>
        </Row>
        <Row className={'operate'} justify="space-between" align="middle">
          <Col span={19}>
            <textarea onChange={this.handleTextareaChange} onKeyUp={e => this.sendMessage(e, null)}
                      value={send_message}/>
          </Col>
          <Col span={4}>
            <Button type='primary' onClick={e => this.sendMessage(e, 'btn')}>发送</Button>
          </Col>

        </Row>
      </div>
    )
  }
}

export default connect(
  null
  , {
    sendMessageToReboot
  }
)
(RebootDialogBox)