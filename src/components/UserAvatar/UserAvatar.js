/**
 * 用户头像和下拉组件
 * */

import React from "react";
import {Menu, Dropdown} from "antd";
import {NavLink} from "react-router-dom";

import {
  UserOutlined, LockOutlined, MessageOutlined,
  SettingOutlined, LogoutOutlined
} from "@ant-design/icons";

import "./UserAvatar.sass";

const menu = (
  <Menu style={{minWidth: '150px'}} className={'user-avatar-menu'}>
    <Menu.Item>
      <NavLink to="/">
        <UserOutlined className={'menu-icon'}/>个人信息
      </NavLink>
    </Menu.Item>
    <Menu.Item>
      <NavLink to="/">
        <LockOutlined className={'menu-icon'}/>修改密码
      </NavLink>
    </Menu.Item>
    <Menu.Item>
      <NavLink to="/">
        <MessageOutlined className={'menu-icon'} />收发消息
      </NavLink>
    </Menu.Item>
    <Menu.Item>
      <NavLink to="/">
        <SettingOutlined className={'menu-icon'} />
        <span>设置</span>
      </NavLink>
    </Menu.Item>
    <Menu.Divider/>
    <Menu.Item>
      <NavLink to="/">
        <LogoutOutlined className={'menu-icon'} />
        <span>退出</span>
      </NavLink>
    </Menu.Item>
  </Menu>
);

function UserAvatar(props) {
  return (
    <Dropdown overlay={menu}>
      <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
        <UserOutlined/> {props.username}
      </a>
    </Dropdown>
  )
}

export default UserAvatar