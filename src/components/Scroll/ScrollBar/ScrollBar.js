/**
 * 滚动条表单
 * */

import React, {Component} from "react";
import {List, Avatar} from "antd/lib/index";

import project_logo from "src/assets/images/logo/光伏1.png";

import "./ScrollBar.sass";

class ScrollBar extends Component {
  state = {
    listMarginTop: "0",
    animate: false,
    data: this.props.data
  };

  scrollUp = e => {
    let height;
    this.state.data.push(this.state.data[0]);
    if (document.getElementById("scrollList")) {
      height = document.getElementById("scrollList").getElementsByTagName("li")[0].scrollHeight + 1;
    } else {
      height = 0
    }

    this.setState({
      animate: true,
      listMarginTop: "-" + height + "px",
    });
    setTimeout(() => {
      this.state.data.shift();
      this.setState({
        animate: false,
        listMarginTop: "0",
      });
      this.forceUpdate();
    }, 2000)
  };

  componentDidMount() {
    if (this.props.data.length !== 0) {
      this.scrollUp();
      setInterval(this.scrollUp, 3000);
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.setState({
      data: nextProps.data
    })
  }

  render() {
    return (
      <div className="scroll-list">
        <List
          itemLayout="horizontal"
          id="scrollList"
          style={{marginTop: this.state.listMarginTop}}
          className={this.state.animate ? "animate" : ''}
          dataSource={this.state.data}
          renderItem={(item, index) => (
            <List.Item key={index}>
              <List.Item.Meta
                avatar={<Avatar className={'avatar'} src={project_logo}/>}
                title={item.name} description={item.company}
              />
            </List.Item>
          )}
        />
      </div>
    )
  }
}

export default ScrollBar