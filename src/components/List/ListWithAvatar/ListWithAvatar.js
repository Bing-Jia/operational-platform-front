/**
 * @File   : ListWithAvatar.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/17
 * @Desc   : 带头像的list组件
 **/

import React, {Component} from 'react';
import {List, Avatar, Space} from "antd";
import {MessageOutlined, LikeOutlined, StarOutlined} from '@ant-design/icons';

import "./ListWithAvatar.sass";

const listData = [];
for (let i = 0; i < 23; i++) {
  listData.push({
    href: 'https://ant.design',
    title: `ant design part ${i}`,
    avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
    description:
      'Ant Design, a design language for background applications, is refined by Ant UED Team.',
    content:
      'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
  });
}

class ListWithAvatar extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    const IconText = ({icon, text}) => (
      <Space>
        {React.createElement(icon)}
        {text}
      </Space>
    );
    return (
      <List
        className={'list-with-avatar'}
        itemLayout="vertical"
        size="small"
        pagination={{
          onChange: page => {
            console.log(page);
          },
          pageSize: 4,
        }}
        dataSource={listData}
        renderItem={item => (
          <List.Item
            key={item.title}
            actions={[
              <IconText icon={StarOutlined} text="156" key="list-vertical-star-o"/>,
              <IconText icon={LikeOutlined} text="156" key="list-vertical-like-o"/>,
              <IconText icon={MessageOutlined} text="2" key="list-vertical-message"/>,
            ]}
            extra={
              <img
                width={250}
                alt="logo"
                src="https://gw.alipayobjects.com/zos/rmsportal/mqaQswcyDLcXyDKnZfES.png"
              />
            }
          >
            <List.Item.Meta
              avatar={<Avatar src={item.avatar}/>}
              title={<a href={item.href}>{item.title}</a>}
              description={item.description}
            />
            {item.content}
          </List.Item>
        )}
      />
    )
  }
}

export default ListWithAvatar