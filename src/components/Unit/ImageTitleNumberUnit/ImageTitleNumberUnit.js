/**
 * @File   : ImageTitleNumberUnit.js
 * @Author : xue.xiaoBing
 * @Date   : 2021/1/6
 * @Desc   : 图片标题数字单元
 **/

import CountUp from "react-countup";
import React, {Component} from "react";

import "./ImageTitleNumberUnit.sass";

class ImageTitleNumberUnit extends Component {

  render() {
    const {data} = this.props;
    return (
      <div className={'image-title-number-unit'} >
      <img src={data.image} alt="" />
      <h4>{data.title}</h4>
      <h2><CountUp start={0} end={data.number} duration="3" /></h2>
    </div>
    )
  }
}

export default ImageTitleNumberUnit