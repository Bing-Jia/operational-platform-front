/**
 * @File   : UnitEdit.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/18
 * @Desc   : 单元编辑组件
 **/

import React, {Component} from "react";
import {Row, Col, Typography} from "antd";

const {Paragraph} = Typography;

class UnitEdit extends Component {
  constructor(props) {
    super(props);
  }

  changeContent = () => {

  };

  render() {
    return (
      <Row justify="space-around" style={{backgroundColor: 'white', height: '200px'}}>
        <Col span={5} style={{backgroundColor: `${this.props.color}`, height: '60px'}}>
          <img src={this.props.image} alt="" style={{width: '50px', height: '50px', margin: '5px 5px'}}/>
        </Col>
        <Col span={18} offset={1}>
          <div style={{margin: '10px auto', color: 'black', fontSize: '12', textAlign: 'center', width: '100%'}}>
            <strong style={{fontSize: '24px'}}>
              <Paragraph editable={{onChange: this.changeContent}}>{this.props.title}</Paragraph>
            </strong>
          </div>
        </Col>
        <Row style={{margin:'10px 20px'}}>
          <Paragraph editable={{onChange: this.changeContent}}>{this.props.content}</Paragraph>
        </Row>
      </Row>
    )
  }
}

export default UnitEdit