/**
 * @File   : FeatureUnit.js
 * @Author : xue.xiaoBing
 * @Date   : 2021/1/4
 * @Desc   : 首页特色单元块
 **/

import {Col, Row} from "antd";
import React, {Component} from "react";

import "./FeatureUnit.sass";


class FeatureUnit extends Component {

  render() {
    const {data} = this.props;
    return (
      <Col span={6} className={'feature-unit'}>
        <Row className={'unit'}>
          <Col className={'image'} span={4} style={{backgroundColor: `${data.color}`}}>
            <img src={data.image} alt="" />
          </Col>
          <Col className={'content'} span={18} offset={1}>
            <div className={'title'}>
              <strong>{data.title}</strong>
            </div>
            <p className={'detail'}>{data.content}</p>
          </Col>
        </Row>
      </Col>
    )
  }
}

export default FeatureUnit