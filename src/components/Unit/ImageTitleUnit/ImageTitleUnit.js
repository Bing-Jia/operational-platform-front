/**
 * @File   : ImageTitleUnit.js
 * @Author : xue.xiaoBing
 * @Date   : 2021/1/4
 * @Desc   : 图片标题单元块
 **/

import React, {Component} from "react";

import "./ImageTitleUnit.sass";

class ImageTitleUnit extends Component {

  render() {
    const {data} = this.props;
    return (
      <div className={'image-title-unit'}>
        <img src={data.image} alt=""/>
        <h4>{data.title}</h4>
      </div>
    )
  }
}

export default ImageTitleUnit