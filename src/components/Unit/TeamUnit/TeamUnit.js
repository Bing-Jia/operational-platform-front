/**
 * @File   : TeamUnit.js
 * @Author : xue.xiaoBing
 * @Date   : 2021/1/6
 * @Desc   : 首页团队单元
 **/

import React, {Component} from "react";
import OwlCarousel from "react-owl-carousel";

import WebsiteTitle from "src/components/Title/WebsiteTitle/WebsiteTitle";
import ImageTitleContentUnit from "src/components/Unit/ImageTitleContentUnit/ImageTitleContentUnit";

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

import "./TeamUnit.sass";



class TeamUnit extends Component {
  constructor(props){
    super(props);
    this.state ={}
  }
  render() {
    const {data} = this.props;
    return (
      <div className={'team-unit'} >
        <WebsiteTitle className={'title'} title={data.title} content={data.content} color='white'/>
        <OwlCarousel loop margin={32} items={4} className="owl-theme">
          {
            data.teams.map((team, key) => (
              <ImageTitleContentUnit data={team} key={key.toString()}/>
            ))
          }
        </OwlCarousel>
      </div>
    )
  }
}

export default TeamUnit