/**
 * @File   : EquipmentUnit.js
 * @Author : xue.xiaoBing
 * @Date   : 2021/1/4
 * @Desc   : 首页设备单元块
 **/

import {Col, Row} from "antd";
import React, {Component} from "react";

import WebsiteTitle from "src/components/Title/WebsiteTitle/WebsiteTitle";

import "./EquipmentUnit.sass";

class EquipmentUnit extends Component {

  render() {
    const {data} = this.props;
    return (
      <div className={'equipment-unit'}>
        <WebsiteTitle className={'title'} title={data.title} content={data.content} color='white'/>
        <Row className={'unit'} justify='center'>
          <Col className={'left'} span={11}>
            <img src={data.leftImage} alt="" />
          </Col>
          <Col className={'right'} span={11} >
            <Row>
              <img className={'top'} src={data.rightTopImage} alt="" />
            </Row>
            <Row>
              <img className={'bottom'} src={data.rightBottomImage} alt="" />
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default EquipmentUnit