/**
 * @File   : FeaturesUnit.js
 * @Author : xue.xiaoBing
 * @Date   : 2021/1/4
 * @Desc   : 首页特色单元块
 **/

import {Col, Row} from "antd";
import React, {Component} from "react";

import WebsiteTitle from "src/components/Title/WebsiteTitle/WebsiteTitle";
import ImageTitleUnit from "src/components/Unit/ImageTitleUnit/ImageTitleUnit";

import "./FeaturesUnit.sass";

class FeaturesUnit extends Component {

  render() {
    const {data} = this.props;
    return (
       <div className={'features-unit'}>
        <WebsiteTitle className={'title'} title={data.title} content={data.content}/>
        <Row className={'unit'} justify='center'>
          <Col span={20}>
            <Row justify='space-between'>
              {data.features.map((features, index) =>
                <Col className={'image-title'} key={index} span={4} >
                  <ImageTitleUnit data={features}/>
                </Col>
              )}
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default FeaturesUnit