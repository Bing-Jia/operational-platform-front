/**
 * @File   : NewUnit.js
 * @Author : xue.xiaoBing
 * @Date   : 2021/1/6
 * @Desc   : 首页新闻单元
 **/

import {Typography} from "antd";
import React, {Component} from "react";
import {NavLink} from "react-router-dom";
import OwlCarousel from "react-owl-carousel";

import WebsiteTitle from "src/components/Title/WebsiteTitle/WebsiteTitle";
import noFound from "src/assets/images/nofound/nofound.jpg";

import "./NewUnit.sass";

const {Paragraph} = Typography;


class NewUnit extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    const {data} = this.props;
    return (
      <div className={'new-unit'}>
        <WebsiteTitle title={data.title} content={data.content} color='white'/>
        <OwlCarousel loop margin={32} items={3} className="owl-theme">
          {data.news.map((newData, index) =>
            <div className="item" key={index}>
              <blockquote>
                <div className='image'>
                  {newData.image ? <img src={newData.image} alt=""/> : <img src={noFound} alt=""/>}
                  <div className={'date-auth'}>
                    <span className='date'>{newData.date}</span>
                    <span className='auth'>{newData.fromPlace}</span>
                  </div>
                </div>
                <div className="text">
                  <h3>
                    <NavLink to="">{newData.title}</NavLink>
                  </h3>
                  <Paragraph className={'content'} ellipsis={{rows: 1, expandable: true}}>{newData.content}</Paragraph>
                </div>
              </blockquote>
            </div>
          )}
        </OwlCarousel>
      </div>
    )
  }
}

export default NewUnit