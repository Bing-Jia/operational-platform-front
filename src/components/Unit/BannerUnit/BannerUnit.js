/**
 * @File   : BannerUnit.js
 * @Author : xue.xiaoBing
 * @Date   : 2021/1/4
 * @Desc   : 首页轮播图单元
 **/

import {Col, Row} from "antd";
import React, {Component} from "react";

import "./BannerUnit.sass";

class BannerUnit extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    const {data} = this.props;
    return (
      <div style={{backgroundImage: `url('${data.image}')`}} className={'scroll-image'}>
        <Row justify="space-around" align="middle" className={'carousel-content'}>
          <Col span={8} className={'feature'}>
            <div className={'title'}><span>{data.title}</span></div>
            <div className={'content'}><span>{data.content}</span></div>
            <div className={'details'}><span>了解更多</span></div>
          </Col>
          <Col span={12} className={'image'}>
            <img src={data.map} alt=""/>
          </Col>
        </Row>
      </div>
    )
  }
}

export default BannerUnit