/**
 * @File   : ResultUnit.js
 * @Author : xue.xiaoBing
 * @Date   : 2021/1/6
 * @Desc   : 首页成果单元
 **/

import {Col, Row} from "antd";
import React, {Component} from "react";

import WebsiteTitle from "src/components/Title/WebsiteTitle/WebsiteTitle";
import ImageTitleNumberUnit from "src/components/Unit/ImageTitleNumberUnit/ImageTitleNumberUnit";

import "./ResultUnit.sass";

class ResultUnit extends Component {
  constructor(props){
    super(props);
    this.state ={}
  }
  render() {
    const {data} = this.props;
    return (
      <div className={'result-unit'}>
        <WebsiteTitle title={data.title} content={data.content} color='black'/>
        <Row className={'unit'} justify='center'>
          <Col span={20}>
            <Row justify='space-between'>
              {data.results.map((data, index) =>
                <Col className='image_with_title_number' span={4}  key={index}>
                  <ImageTitleNumberUnit data={data} key={index}/>
                </Col>
              )}
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

export default ResultUnit