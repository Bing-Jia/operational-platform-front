/**
 * @File   : InfoShowUnit.js
 * @Author : xue.xiaoBing
 * @Date   : 2021/1/9
 * @Desc   : 信息展示单元
 **/

import React,{Fragment} from "react";
import {Row, Col, Divider} from "antd";
import {CaretUpOutlined, CaretDownOutlined} from "@ant-design/icons";

import "./InfoShowUnit.sass";

function InfoShowUnit(props) {
  const ComponentTitle = props.componentTitle;
  return (
    <Row className='info-show-unit'>
      <Col span={23}>
        <Row className='info-title'>
          <ComponentTitle/>
          <span>{props.title}</span>
        </Row>
        <Row className='info-number'>
          <span style={{color: props.color}}>{props.number}</span>
        </Row>
        <Row className='info-radio'>
          {props.lift ? (
            <div className={'radio-up'}>
              <CaretUpOutlined className={'radio-up-icon'}/>
              <span>{props.radio}&nbsp;</span>
              <span>相对于上周</span>
            </div>
          ) : (
            <div className={'radio-down'}>
              <CaretDownOutlined className={'radio-down-icon'}/>
              <span>{props.radio}&nbsp;</span>
              <span>相对于上周</span>
            </div>
          )}

        </Row>
      </Col>
      {
        props.last ? null
          : (
            <Fragment>
              <Col span={1}  className={'info-divider'}>
                <Divider className={'divider'} type='vertical'/>
              </Col>
            </Fragment>
          )}
    </Row>
  )
}

export default InfoShowUnit