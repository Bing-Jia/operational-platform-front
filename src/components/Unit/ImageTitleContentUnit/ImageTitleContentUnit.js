/**
 * @File   : ImageTitleContentUnit.js
 * @Author : xue.xiaoBing
 * @Date   : 2021/1/6
 * @Desc   : 内容图片介绍单元
 **/

import React, {Component} from "react";

import "./ImageTitleContentUnit.sass";

class ImageTitleContentUnit extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    const {data} = this.props;
    return (
      <div className={'image-title-content-unit'}>
        <blockquote >
          <p className={'description'}>
            {data.description}
          </p>
          <div className="user-info">
            <img alt="" className={'avatar'} src={data.image}/>
            <span className={'name'}>{data.name}【{data.role}】</span>
          </div>
        </blockquote>
      </div>

    )
  }
}

export default ImageTitleContentUnit