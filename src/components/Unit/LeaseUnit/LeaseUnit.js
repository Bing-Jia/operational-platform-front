/**
 * @File   : LeaseUnit.js
 * @Author : xue.xiaoBing
 * @Date   : 2021/1/6
 * @Desc   : 首页租赁单元
 **/

import {Col, Row} from "antd";
import React, {Component} from "react";
import {NavLink} from "react-router-dom";

import WebsiteTitle from "src/components/Title/WebsiteTitle/WebsiteTitle";

import "./LeaseUnit.sass";


class LeaseUnit extends Component {

  render() {
    const {data} = this.props;
    return (
      <div className={'lease-unit'}>
        <WebsiteTitle title={data.title} content={data.content} color='black'/>
        <Row className={'unit'} justify='center' gutter={32}>
          {data.leases.map((lease, index) => (
            <Col span={7} key={index}>
              <div className='item'>
                <div className='head'>
                  <h1>{lease.name}</h1>
                </div>
                <div className='price'>
                  <h1>{lease.price}</h1>
                </div>
                <div className='cycle'>
                  <h1>{lease.cycle}</h1>
                </div>
                <div className='info'>
                  {
                    lease.contentList.map(
                      (item, key) =>
                        <h2 key={key}>{item}</h2>
                    )
                  }
                </div>
                <div className='link'>
                  <NavLink to={lease.url}>了解更多</NavLink>
                </div>
              </div>
            </Col>
          ))}
        </Row>
      </div>
    )
  }
}

export default LeaseUnit