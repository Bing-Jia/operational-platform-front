/**
 * @File   : Camera.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/17
 * @Desc   : 镜头设备组件
 **/

import React, {Component} from 'react';
import {Row, Col, Image, Space} from "antd";

import CameraImage from "src/assets/images/equipment/3.jpg";

import "./Camera.sass";
import UAVImage from "../../../assets/images/equipment/2.jpg";


class Camera extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return (
      <Space direction={'vertical'} className={'camera'}>
        <Row align={'middle'}>
          <Col span={3} offset={2}>
            <span>型号</span>
          </Col>
          <Col span={4}>
            <span>XT2</span>
          </Col>
          <Col span={3}>
            <span>公司</span>
          </Col>
          <Col span={10}>
            <span>深圳市大疆创新科技有限公司</span>
          </Col>
        </Row>
        <Row align={'middle'}>
          <Col span={3} offset={2}>
            <span>可见光</span>
          </Col>
          <Col span={4}>
            <span>4k*3k</span>
          </Col>
          <Col span={3}>
            <span>热成像</span>
          </Col>
          <Col span={10}>
            <span>640*512</span>
          </Col>
        </Row>
        <Row align={'middle'} justify={'center'}>
          <Col span={20}>
            <Image src={CameraImage}/>
          </Col>
        </Row>
      </Space>
    )
  }
}

export default Camera