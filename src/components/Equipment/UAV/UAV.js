/**
 * @File   : UAV.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/17
 * @Desc   :
 **/

/**
 * @File   : Camera.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/17
 * @Desc   : 无人机设备组件
 **/

import React, {Component} from 'react';
import {Row, Col, Image, Space} from "antd";

import UAVImage from "src/assets/images/equipment/2.jpg";

import "./UAV.sass";
import CameraImage from "../../../assets/images/equipment/3.jpg";


class UAV extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return (
      <Space direction={'vertical'} className={'uav'}>
        <Row align={'middle'}>
          <Col span={3} offset={2}>
            <span>型号</span>
          </Col>
          <Col span={4}>
            <span>M300RTK</span>
          </Col>
          <Col span={3}>
            <span>公司</span>
          </Col>
          <Col span={10}>
            <span>深圳市大疆创新科技有限公司</span>
          </Col>
        </Row>
        <Row align={'middle'}>
          <Col span={3} offset={2}>
            <span>时长</span>
          </Col>
          <Col span={4}>
            <span>55分钟</span>
          </Col>
          <Col span={3}>
            <span>抗风</span>
          </Col>
          <Col span={10}>
            <span>7级</span>
          </Col>
        </Row>
        <Row align={'middle'} justify={'center'}>
          <Col span={20}>
            <Image src={UAVImage}/>
          </Col>
        </Row>
      </Space>
    )
  }
}

export default UAV