/**
 * @File   : EchartBar.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/1
 * @Desc   : echart 混合图
 **/

import "echarts/lib/chart/bar";
import "echarts/lib/component/title";
import "echarts/lib/component/legend";
import "echarts/lib/component/tooltip";
import "echarts/lib/component/markPoint";
import React, {Component} from "react";
import ReactEcharts from "echarts-for-react";

import "./EchartMixture.sass";


class EchartMixture extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  getOption = () => {
    let option = {
      title: {
        text: this.props.text,
        subtext: this.props.sub_text,
      },
      tooltip: {
        trigger: 'axis'
      },
      legend: {
        x: 20,
        y: 220,
        data: ['鸟屎', '亮斑', '黑晕', '白纸', '棍子', '裂痕']
      },
      toolbox: {
        show: true,
        feature: {
          magicType: {
            show: true,
            title: {
              line: 'Line',
              bar: 'Bar',
              stack: 'Stack',
            },
            type: ['line', 'bar', 'stack']
          },
          saveAsImage: {
            show: false,
            title: "Save Image"
          }
        }
      },
      calculable: true,
      grid: {
        y:10,
        left: '2%',
        right: '5%',
        bottom: '13%',
        containLabel: true
      },
      xAxis: [{
        type: 'category',
        boundaryGap: false,
        data: ['2020.05', '2020.06', '2020.07', '2020.08', '2020.09', '2020.10', '2020.11']
      }],
      yAxis: [{
        type: 'value'
      }],
      series: [{
        name: '鸟屎',
        type: 'line',
        smooth: true,
        itemStyle: {
          normal: {
            areaStyle: {
              type: 'default'
            }
          }
        },
        data: [10, 12, 21, 54, 260, 830, 710]
      }, {
        'name': '亮斑',
        'type': 'line',
        'smooth': true,
        itemStyle: {
          normal: {
            areaStyle: {
              type: 'default'
            }
          }
        },
        'data': [30, 182, 84, 791, 30, 30, 10]
      }, {
        name: '黑晕',
        type: 'line',
        smooth: true,
        itemStyle: {
          normal: {
            areaStyle: {
              type: 'default'
            }
          }
        },
        data: [320, 1132, 501, 234, 120, 90, 20]
      }, {
        name: '白纸',
        type: 'line',
        smooth: true,
        itemStyle: {
          normal: {
            areaStyle: {
              type: 'default'
            }
          }
        },
        data: [130, 112, 601, 234, 120, 910, 20]
      }, {
        name: '棍子',
        type: 'line',
        smooth: true,
        itemStyle: {
          normal: {
            areaStyle: {
              type: 'default'
            }
          }
        },
        data: [120, 1132, 601, 634, 120, 90, 20]
      }, {
        name: '裂痕',
        type: 'line',
        smooth: true,
        itemStyle: {
          normal: {
            areaStyle: {
              type: 'default'
            }
          }
        },
        data: [100, 1132, 601, 1134, 120, 90, 20]
      }
      ]
    };
    return option
  };

  render() {
    return (
      <ReactEcharts className={'echart-mixture'} option={this.getOption()}/>
    )
  }
}

export default EchartMixture