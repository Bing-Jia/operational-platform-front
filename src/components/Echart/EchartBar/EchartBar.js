/**
 * @File   : EchartBar.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/2
 * @Desc   : echart 饼图
 **/

import React, {Component} from 'react';
import 'echarts/lib/chart/bar'
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/title'
import 'echarts/lib/component/legend'
import 'echarts/lib/component/markPoint'
import ReactEcharts from 'echarts-for-react';

import "./EchartBar.sass";


class EchartBar extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  getOption = () => {
    let option = {
      title: {
        text: this.props.text,
      },
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b}: {c} ({d}%)'
      },
      legend: {
        orient: 'vertical',
        right: 10,
        data: ['亮斑', '黑晕', '鸟屎']
      },
      series: [
        {
          type: 'pie',
          radius: ['40%', '70%'],
          avoidLabelOverlap: false,
          label: {
            show: false,
            position: 'center'
          },
          emphasis: {
            label: {
              show: true,
              fontSize: '30',
              fontWeight: 'bold'
            }
          },
          labelLine: {
            show: false
          },
          data: [
            {value: 335, name: '亮斑'},
            {value: 310, name: '黑晕'},
            {value: 234, name: '鸟屎'},
          ]
        }
      ]
    };
    return option
  };

  render() {
    return (
      <ReactEcharts className={'echart-bar'} option={this.getOption()}/>
    )
  }
}

export default EchartBar