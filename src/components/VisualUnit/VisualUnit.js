/**
 * @File   : VisualUnit.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/7
 * @Desc   : 可视化单元
 **/

import React,{Fragment} from 'react'
import {Row, Col, Divider} from 'antd';
import {CaretUpOutlined, CaretDownOutlined} from '@ant-design/icons';

import './VisualUnit.css'

function VisualUnit(props) {
  const ComponentTitle = props.componentTitle;
  return (
    <Row className='visual_unit'>
      <Col span={23}>
        <Row className='visual_unit_title'>
          <ComponentTitle/>&nbsp;&nbsp;
          <span>{props.title}</span>
        </Row>
        <Row className='visual_unit_number'>
          <span style={{color: props.color}}>{props.number}</span>
        </Row>
        <Row className='visual_unit_radio'>
          {props.lift ? (
            <>
              <CaretUpOutlined style={{color: 'red'}}/>
              <span style={{fontStyle: 'italic', color: 'red'}}>{props.radio}&nbsp;</span>
              <span style={{fontSize: '13px'}}>相对于上周</span>
            </>
          ) : (
            <>
              <CaretDownOutlined style={{color: 'green'}}/>
              <span style={{fontStyle: 'italic', color: 'green'}}>{props.radio}&nbsp;</span>
              <span style={{fontSize: '13px'}}>相对于上周</span>
            </>
          )}

        </Row>
      </Col>
      {
        props.last ? null
          : (
            <Fragment>
              <Col span={1} style={{marginTop: '10%', position: 'relative'}}>
                <Divider type='vertical' style={{backgroundColor: 'black', height: '80%', width: '1px'}}/>
              </Col>
            </Fragment>
          )}
    </Row>
  )
}

export default VisualUnit