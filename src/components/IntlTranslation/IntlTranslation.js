/**
 * 国际化组件
 * */

import React, {Component} from 'react';
import intl from 'react-intl-universal';
import {emit} from "../../utils/emit";
import zh_CN from "antd/es/locale/zh_CN";
import en_US from "antd/es/locale/en_US";

const locales = {
  'en-US': require('src/assets/locales/en-US.json'),
  'zh-CN': require('src/assets/locales/zh-CN.json'),
};

export default class IntlTranslation extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    emit.on('change_language', lang => this.loadLocales(lang));
    this.loadLocales()
  }

  loadLocales = (lang = 'zh-CN') => {
    intl.init({
      currentLocale: lang,
      locales,
    }).then(() => {
      this.setState({
        antdLang: lang === 'zh-CN' ? zh_CN : en_US
      })
    })
  };

  render() {
    return (
      <span>{`${intl.get(this.props.intlKey)}`}</span>
    );
  }
}
