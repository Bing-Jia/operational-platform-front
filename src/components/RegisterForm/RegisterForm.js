/**
 * @File   : RegisterForm.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/3
 * @Desc   : 注册表单组件
 **/

import React, {Fragment} from 'react';
import {Link} from "react-router-dom";
import {Button, Input, Form, Checkbox} from "antd";
import {UserOutlined, LockTwoTone, MailTwoTone} from "@ant-design/icons";


import "./RegisterForm.sass";

function RegisterForm(props) {
  return (
    <Fragment>
      <h1>注册</h1>
      <Form.Item
        name="username"
        rules={[
          {
            required: true,
            message: '请输入你的用户名!',
            whitespace: true,
          }, {
            min: 2,
            message: '用户名不能少于2个字符',
          }
        ]}
      >
        <Input className='register-input' placeholder='用户名'
               prefix={<UserOutlined className="site-form-item-icon" style={{color: "#1890ff"}}/>}/>
      </Form.Item>
      <Form.Item
        name="fullName"
        rules={[
          {
            required: true,
            message: '请输入你的姓名!',
            whitespace: true,
          }, {
            min: 2,
            message: '姓名不能少于2个字符',
          }
        ]}
      >
        <Input className='register-input' placeholder='姓名'
               prefix={<UserOutlined className="site-form-item-icon" style={{color: "#1890ff"}}/>}/>
      </Form.Item>
      <Form.Item
        name="password"
        rules={[
          {
            required: true,
            message: '请输入你的密码!',
          }, {
            min: 6,
            message: '密码不能少于6个字符',
          }
        ]}
        hasFeedback
      >
        <Input.Password className='register-input' placeholder='密码'
                        prefix={<LockTwoTone className="site-form-item-icon"/>}/>
      </Form.Item>
      <Form.Item
        name='email'
        rules={[
          {
            type: 'email',
            message: "邮件格式不合法！"
          }, {
            required: true,
            message: "请输入你的邮箱地址！"
          },
        ]}
      >
        <Input className='register-input' placeholder='邮件'
               prefix={<MailTwoTone className="site-form-item-icon"/>}/>
      </Form.Item>
      <Form.Item
        name="accepted_terms"
        valuePropName="checked"
        rules={[
          {
            validator: (_, value) =>
              value ? Promise.resolve() : Promise.reject('请接受此声明！'),
          },
        ]}
      >
        <Checkbox className={'statement'}>
          我已经阅读此 <Link to="/#">声明</Link>
        </Checkbox>
      </Form.Item>
      <Form.Item>
        <Button className='button-register-reset' type="danger" htmlType='reset'>
          重置
        </Button>
        <Button className='button-register-register' type="primary" htmlType="submit"
                disabled={props.status}>
          注册
        </Button>
      </Form.Item>
    </Fragment>
  )
}

export default RegisterForm