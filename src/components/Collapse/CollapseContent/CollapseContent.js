/**
 * @File   : CollapseContent.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/2
 * @Desc   : 内容折叠组件
 **/

import {Collapse} from "antd";
import React, {Component} from "react";
import {CaretRightOutlined} from "@ant-design/icons";

import "./CollapseContent.sass";

const {Panel} = Collapse;

const data = [
  {
    name: '【亮斑】修复建议 热成像',
    data: '自然界中的一切物体，无论是北极冰川，还是火焰、人体，甚至极寒冷的宇宙深空，只要它们的\n' +
      '温度高于绝对零度-273℃，都会有红外辐射，这是由于物体内部分子热运动的结果。其辐射能量\n' +
      '正比于自身温度的四次方成正比，辐射出的波长与其温度成反比。红外成像技术就是根据探测到\n' +
      '的物体的辐射能量的高低。经系统处理转变为目标物体的热图像，以灰度级或伪彩色显示出来，\n' +
      '即得到被测目标的温度分布从而判断物体所处的状态。'
  },
  {
    name: '【黑晕】修复建议 热成像',
    data: '1、生产过程中的隐裂因素：设备因素、原料因素、工艺参数因素;\n ' +
      '2、存储运输中的隐裂因素：组件箱体变形、长期雨水浸透、组件来回搬运等造成箱体歪斜， 箱体内单块组件具有活动空间，搬运过程组件晃动造成对角式隐裂；\n' +
      '3、安装施工中的隐裂因素：工人安装、清洗过程中操作不规范造成组件隐裂。',
  },
  {
    name: '【裂痕】修复建议 热成像',
    data: '待定...'
  }
];

class CollapseContent extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return (
      <div className={'collapse-content'}>
        <Collapse
          accordion={true}
          bordered={false}
          defaultActiveKey={['0']}
          expandIcon={({isActive}) => <CaretRightOutlined rotate={isActive ? 90 : 0}/>}
          className="site-collapse-custom-collapse"
        >
          {data.map((item, index) =>
            <Panel header={item.name} key={index} className="site-collapse-custom-panel">
              <p>{item.data}</p>
            </Panel>)}
        </Collapse>
      </div>
    )
  }
}

export default CollapseContent