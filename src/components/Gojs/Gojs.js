/**
 * Go js组件
 * */

import React, {Component} from 'react'
import DiagramWrapper3 from "./DiagramWrapper3";


import './Gojs.css'


export default class Gojs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nodeDataArray: [
        {
          "key": 1, "name": "Unit One", "loc": "101 204",
          "leftArray": [
            {"portColor": "#fae3d7", "portId": "left0"}],
          "topArray": [
            {"portColor": "#d6effc", "portId": "top0"}],
          "bottomArray": [
            {"portColor": "#ebe3fc", "portId": "bottom0"}],
          "rightArray": [
            {"portColor": "#eaeef8", "portId": "right0"},
            {"portColor": "#fadfe5", "portId": "right1"}]
        },
        {
          "key": 2, "name": "Unit Two", "loc": "320 152",
          "leftArray": [
            {"portColor": "#6cafdb", "portId": "left0"},
            {"portColor": "#66d6d1", "portId": "left1"},
            {"portColor": "#fae3d7", "portId": "left2"}],
          "topArray": [
            {"portColor": "#d6effc", "portId": "top0"}],
          "bottomArray": [
            {"portColor": "#eaeef8", "portId": "bottom0"},
            {"portColor": "#eaeef8", "portId": "bottom1"},
            {"portColor": "#6cafdb", "portId": "bottom2"}],
          "rightArray": []
        },
        {
          "key": 3, "name": "Unit Three", "loc": "384 319",
          "leftArray": [
            {"portColor": "#66d6d1", "portId": "left0"},
            {"portColor": "#fadfe5", "portId": "left1"},
            {"portColor": "#6cafdb", "portId": "left2"}],
          "topArray": [
            {"portColor": "#66d6d1", "portId": "top0"}],
          "bottomArray": [
            {"portColor": "#6cafdb", "portId": "bottom0"}],
          "rightArray": []
        },
        {
          "key": 4, "name": "Unit Four", "loc": "138 351",
          "leftArray": [
            {"portColor": "#fae3d7", "portId": "left0"}],
          "topArray": [
            {"portColor": "#6cafdb", "portId": "top0"}],
          "bottomArray": [
            {"portColor": "#6cafdb", "portId": "bottom0"}],
          "rightArray": [
            {"portColor": "#6cafdb", "portId": "right0"},
            {"portColor": "#66d6d1", "portId": "right1"}]
        }
      ],
      linkDataArray: [
        {
          key: -1, "from": 4, "to": 2, "fromPort": "top0", "toPort": "bottom0"
        }, {
          key: -2, "from": 4, "to": 2, "fromPort": "top0", "toPort": "bottom0"
        }, {
          key: -3, "from": 3, "to": 2, "fromPort": "top0", "toPort": "bottom1"
        }, {
          key: -4, "from": 4, "to": 3, "fromPort": "right0", "toPort": "left0"
        }, {
          key: -5, "from": 4, "to": 3, "fromPort": "right1", "toPort": "left2"
        }, {
          key: -6, "from": 1, "to": 2, "fromPort": "right0", "toPort": "left1"
        }, {
          key: -7, "from": 1, "to": 2, "fromPort": "right1", "toPort": "left2"
        }
      ],
      modelData: {
        canRelink: true
      },
      selectedKey: null,
      skipsDiagramUpdate: false
    }
    ;
    // bind handler methods
    this.handleDiagramEvent = this.handleDiagramEvent.bind(this);
    this.handleRelinkChange = this.handleRelinkChange.bind(this);
  }

  handleDiagramEvent(e) {
    const name = e.name;
    switch (name) {
      case 'ChangedSelection': {
        const sel = e.subject.first();
        if (sel) {
          this.setState({selectedKey: sel.key});
        } else {
          this.setState({selectedKey: null});
        }
        break;
      }
      default:
        break;
    }
  }

  /**
   * Handle changes to the checkbox on whether to allow relinking.
   * @param e a change event from the checkbox
   */
  handleRelinkChange(e) {
    const target = e.target;
    const value = target.checked;
    this.setState({modelData: {canRelink: value}, skipsDiagramUpdate: false});
  }

  render() {
    return (
      <div>
        <DiagramWrapper3/>
      </div>
    );
  }
}