/**
 * Go js组件
 * */

import React, {Component} from 'react'
// import {DiagramWrapper} from "./DiagramWrapper";
// import DiagramWrapper1 from "./DiagramWrapper1";
// import DiagramWrapper2 from "./DiagramWrapper2";
// import DiagramWrapper3 from "./DiagramWrapper3";
import GojsDiagram from 'react-gojs';


import './Gojs.css'


export default class Gojs2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // nodeDataArray: [
      //   {
      //     "key": 1, "name": "Unit One", "loc": "101 204",
      //     "leftArray": [
      //       {"portColor": "#fae3d7", "portId": "left0"}],
      //     "topArray": [
      //       {"portColor": "#d6effc", "portId": "top0"}],
      //     "bottomArray": [
      //       {"portColor": "#ebe3fc", "portId": "bottom0"}],
      //     "rightArray": [
      //       {"portColor": "#eaeef8", "portId": "right0"},
      //       {"portColor": "#fadfe5", "portId": "right1"}]
      //   },
      //   {
      //     "key": 2, "name": "Unit Two", "loc": "320 152",
      //     "leftArray": [
      //       {"portColor": "#6cafdb", "portId": "left0"},
      //       {"portColor": "#66d6d1", "portId": "left1"},
      //       {"portColor": "#fae3d7", "portId": "left2"}],
      //     "topArray": [
      //       {"portColor": "#d6effc", "portId": "top0"}],
      //     "bottomArray": [
      //       {"portColor": "#eaeef8", "portId": "bottom0"},
      //       {"portColor": "#eaeef8", "portId": "bottom1"},
      //       {"portColor": "#6cafdb", "portId": "bottom2"}],
      //     "rightArray": []
      //   },
      //   {
      //     "key": 3, "name": "Unit Three", "loc": "384 319",
      //     "leftArray": [
      //       {"portColor": "#66d6d1", "portId": "left0"},
      //       {"portColor": "#fadfe5", "portId": "left1"},
      //       {"portColor": "#6cafdb", "portId": "left2"}],
      //     "topArray": [
      //       {"portColor": "#66d6d1", "portId": "top0"}],
      //     "bottomArray": [
      //       {"portColor": "#6cafdb", "portId": "bottom0"}],
      //     "rightArray": []
      //   },
      //   {
      //     "key": 4, "name": "Unit Four", "loc": "138 351",
      //     "leftArray": [
      //       {"portColor": "#fae3d7", "portId": "left0"}],
      //     "topArray": [
      //       {"portColor": "#6cafdb", "portId": "top0"}],
      //     "bottomArray": [
      //       {"portColor": "#6cafdb", "portId": "bottom0"}],
      //     "rightArray": [
      //       {"portColor": "#6cafdb", "portId": "right0"},
      //       {"portColor": "#66d6d1", "portId": "right1"}]
      //   }
      // ],
      // linkDataArray: [
      //   {
      //     key: -1, "from": 4, "to": 2, "fromPort": "top0", "toPort": "bottom0"
      //   }, {
      //     key: -2, "from": 4, "to": 2, "fromPort": "top0", "toPort": "bottom0"
      //   }, {
      //     key: -3, "from": 3, "to": 2, "fromPort": "top0", "toPort": "bottom1"
      //   }, {
      //     key: -4, "from": 4, "to": 3, "fromPort": "right0", "toPort": "left0"
      //   }, {
      //     key: -5, "from": 4, "to": 3, "fromPort": "right1", "toPort": "left2"
      //   }, {
      //     key: -6, "from": 1, "to": 2, "fromPort": "right0", "toPort": "left1"
      //   }, {
      //     key: -7, "from": 1, "to": 2, "fromPort": "right1", "toPort": "left2"
      //   }
      // ],
      "nodeDataArray": [
        {"category": "Comment", "loc": "360 -10", "text": "Kookie Brittle", "key": -13},
        {"key": -1, "category": "Start", "loc": "175 0", "text": "Start"},
        {"key": 0, "loc": "-5 75", "text": "Preheat oven to 375 F"},
        {
          "key": 1,
          "loc": "175 100",
          "text": "In a bowl, blend: 1 cup margarine, 1.5 teaspoon vanilla, 1 teaspoon salt"
        },
        {"key": 2, "loc": "175 200", "text": "Gradually beat in 1 cup sugar and 2 cups sifted flour"},
        {"key": 3, "loc": "175 290", "text": "Mix in 6 oz (1 cup) Nestle's Semi-Sweet Chocolate Morsels"},
        {"key": 4, "loc": "175 380", "text": "Press evenly into ungreased 15x10x1 pan"},
        {"key": 5, "loc": "355 85", "text": "Finely chop 1/2 cup of your choice of nuts"},
        {"key": 6, "loc": "175 450", "text": "Sprinkle nuts on top"},
        {"key": 7, "loc": "175 515", "text": "Bake for 25 minutes and let cool"},
        {"key": 8, "loc": "175 585", "text": "Cut into rectangular grid"},
        {"key": -2, "category": "End", "loc": "175 660", "text": "Enjoy!"}
      ],
      "linkDataArray": [
        {"from": 1, "to": 2, "fromPort": "B", "toPort": "T"},
        {"from": 2, "to": 3, "fromPort": "B", "toPort": "T"},
        {"from": 3, "to": 4, "fromPort": "B", "toPort": "T"},
        {"from": 4, "to": 6, "fromPort": "B", "toPort": "T"},
        {"from": 6, "to": 7, "fromPort": "B", "toPort": "T"},
        {"from": 7, "to": 8, "fromPort": "B", "toPort": "T"},
        {"from": 8, "to": -2, "fromPort": "B", "toPort": "T"},
        {"from": -1, "to": 0, "fromPort": "B", "toPort": "T"},
        {"from": -1, "to": 1, "fromPort": "B", "toPort": "T"},
        {"from": -1, "to": 5, "fromPort": "B", "toPort": "T"},
        {"from": 5, "to": 4, "fromPort": "B", "toPort": "T"},
        {"from": 0, "to": 4, "fromPort": "B", "toPort": "T"}
      ],
      modelData: {
        canRelink: true
      },
      selectedKey: null,
      skipsDiagramUpdate: false
    }
    ;
    // bind handler methods
    this.handleDiagramEvent = this.handleDiagramEvent.bind(this);
    this.handleRelinkChange = this.handleRelinkChange.bind(this);
  }

  handleDiagramEvent(e) {
    const name = e.name;
    switch (name) {
      case 'ChangedSelection': {
        const sel = e.subject.first();
        if (sel) {
          this.setState({selectedKey: sel.key});
        } else {
          this.setState({selectedKey: null});
        }
        break;
      }
      default:
        break;
    }
  }

  /**
   * Handle changes to the checkbox on whether to allow relinking.
   * @param e a change event from the checkbox
   */
  handleRelinkChange(e) {
    const target = e.target;
    const value = target.checked;
    this.setState({modelData: {canRelink: value}, skipsDiagramUpdate: false});
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log("---------",this.state)
  }

  render() {
    return (
      <div>
        <GojsDiagram
          nodeDataArray={this.state.nodeDataArray}
          linkDataArray={this.state.linkDataArray}
          modelData={this.state.modelData}
          skipsDiagramUpdate={this.state.skipsDiagramUpdate}
          onDiagramEvent={this.handleDiagramEvent}
        />
      </div>
    );
  }
}