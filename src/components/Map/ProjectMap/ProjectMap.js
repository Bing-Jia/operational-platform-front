/**
 * @File   : ProjectMap.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/15
 * @Desc   : 项目位置地图组件
 **/


import React, {Component} from "react";

import "./ProjectMap.sass";


class ProjectMap extends Component {

  componentDidMount() {
    // 地图设置
    const {BMap} = window;
    const map = new BMap.Map("baidu-project-map");
    map.centerAndZoom(new BMap.Point(106.404, 36.915), 3);
    map.addControl(new BMap.MapTypeControl()); //添加地图类型控件
    map.setCurrentCity("北京");

    map.enableScrollWheelZoom();
    map.enableContinuousZoom();

    const point = new BMap.Point(97.36747029997534,37.37534590746335 );//创建坐标点
    const marker = new BMap.Marker(point);
    map.addOverlay(marker);              // 将标注添加到地图中

    const opts = {
      width: 100,
      height: 100,
      color: 'green',
      title: '金山光伏项目一'
    };
    const infoWindow = new BMap.InfoWindow("地址：上海市金山区漫华路8号", opts);
    marker.addEventListener("click", function () {
      map.openInfoWindow(infoWindow, point); //开启信息窗口
    });

  }

  render() {
    return (
      <div id="baidu-project-map" className='baidu-project-map' />
    )
  }
}

export default ProjectMap