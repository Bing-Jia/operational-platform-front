/**
 * @File   : Cesium3.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/27
 * @Desc   : cesium 组件
 **/

import React, {Component} from "react";
import * as Cesium from "cesium/Cesium";

class CesiumCityHome extends Component {

  componentDidMount() {
    // cesium token
    Cesium.Ion.defaultAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJiYTBiYTIwZS1jODEwLTRiMjAtYTk" +
      "3Ni0wYTNiNzhiN2YxYzYiLCJpZCI6MzczMDEsImlhdCI6MTYwNDkwMDI5MH0.-LssTx68F6jrPEdihmt9K8gP4yLKRpU3VZghsMRSfxA";

    // 视图
    let viewer = new Cesium.Viewer("cesiumContainer", {
      animation: false,  //是否显示动画控件
      baseLayerPicker: true, //是否显示图层选择控件
      geocoder: false, //是否显示地名查找控件
      timeline: false, //是否显示时间线控件
      sceneModePicker: false, //是否显示投影方式控件
      navigationHelpButton: false, //是否显示帮助信息控件
      infoBox: false,  //是否显示点击要素之后显示的信息
      homeButton: false,
      terrainProvider: new Cesium.CesiumTerrainProvider({
        url: Cesium.IonResource.fromAssetId(1),
      }),
    });


    /****************************obj*****************************/
    viewer.scene.globe.depthTestAgainstTerrain = true;
    viewer.cesiumWidget.creditContainer.style.display = "none";

    // 添加模型
    const tileset = viewer.scene.primitives.add(
      new Cesium.Cesium3DTileset({
        url: Cesium.IonResource.fromAssetId(253792),
      })
    );


    // tileset.readyPromise
    //   .then(function () {
    //     viewer.zoomTo(tileset);
    //     const extras = tileset.asset.extras;
    //     if (
    //       Cesium.defined(extras) &&
    //       Cesium.defined(extras.ion) &&
    //       Cesium.defined(extras.ion.defaultStyle)
    //     ) {
    //       tileset.style = new Cesium.Cesium3DTileStyle(extras.ion.defaultStyle);
    //     }
    //   })
    //   .otherwise(function (error) {
    //     console.log(error);
    //   });

    viewer.camera.flyTo({
      destination: Cesium.Cartesian3.fromDegrees(97.234135, 37.344919, 6000.0),
      orientation: {
        heading: Cesium.Math.toRadians(0.0),
        pitch: Cesium.Math.toRadians(-90.0),
        roll: 0.0
      },
      duration: 15,
      pitchAdjustHeight: -90,
      maximumHeight: 150000
    });


    /****************************osgb*****************************/
    // viewer.cesiumWidget.creditContainer.style.display = "none";
    // let tileset = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
    //   url: 'http://127.0.0.1:8001/static/project/29/model/3dtiles/tileset.json',
    //   shadows: Cesium.ShadowMode.DISABLED,//去除阴影
    //   scale: 1
    // }));
    // viewer.camera.flyTo({
    //   destination: Cesium.Cartesian3.fromDegrees(97.234135, 37.344919, 5000.0),
    //   orientation: {
    //     heading: Cesium.Math.toRadians(0.0),
    //     pitch: Cesium.Math.toRadians(-90.0),
    //     roll: 0.0
    //   },
    //   duration: 15,
    //   pitchAdjustHeight: -90,
    //   maximumHeight: 150000
    // });
    // tileset.readyPromise.then(function () {
    //
    // });
  }

  render() {
    return (
      <div id="cesiumContainer" style={{width: '100%', height: 'calc(100vh - 64px)', position: 'relative'}}/>
    )
  }
}


export default CesiumCityHome