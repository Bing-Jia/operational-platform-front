/**
 * @File   : CopyRight.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/2
 * @Desc   : 版权组件
 **/

import React from "react";

import "./Copyright.sass";

function Copyright(props) {
  const {fontColor} = props;
  return(
    <div className={'copyright'} style={{color: `${fontColor?fontColor:"white"}`}}>
      <span>Copyright © 2019-2020 by bingjia. All rights reserved. </span>
    </div>
  )
}

export default Copyright