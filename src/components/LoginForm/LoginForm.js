/**
 * @File   : LoginForm.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/3
 * @Desc   : 登录表单
 **/

import React, {Fragment} from "react";
import {Button, Input, Form, Checkbox} from "antd";
import {UserOutlined, LockTwoTone} from '@ant-design/icons';

import "./LoginForm.sass";


function LoginForm(props) {
  return (
    <Fragment>
      <h1>登录</h1>
      <Form.Item
        name="username"
        rules={[
          {
            required: true,
            message: '请输入你的用户名!',
            whitespace: true,
          }, {
            min: 2,
            message: '用户名不能少于2个字符',
          }
        ]}
      >
        <Input className='login-input' placeholder='用户名'
               prefix={<UserOutlined className="site-form-item-icon" style={{color: "#1890ff"}}/>}/>
      </Form.Item>
      <Form.Item
        name="password"
        rules={[
          {
            required: true,
            message: '请输入你的密码!',
          }, {
            min: 6,
            message: '密码不能少于6个字符',
          }
        ]}
        hasFeedback
      >
        <Input.Password className='login-input' placeholder='密码'
                        prefix={<LockTwoTone className="site-form-item-icon"/>}/>
      </Form.Item>
      <Form.Item
        name="accepted_terms"
        valuePropName="checked"
      >
        <Checkbox className={'forget-password'} >
          记住密码？
        </Checkbox>
      </Form.Item>
      <Form.Item>
        <Button className='button-login-reset' type="danger" htmlType='reset'>
          重置
        </Button>
        <Button className='button-login-login' type="primary" htmlType="submit"
                disabled={props.status}>
          登录
        </Button>
      </Form.Item>
    </Fragment>
  )
}

export default LoginForm