/**
 * @File   : NotFound.js
 * @Author : xue.xiaoBing
 * @Date   : 2021/1/9
 * @Desc   : 未发现
 **/

import {Button, Result} from "antd";
import React, {Component} from "react";


export default class NotFound extends Component {

  render() {
    return (
      <Result
        className={'no-found'}
        status="404" title="404"
        subTitle="抱歉, 你访问的页面不存在！"
        extra={
          <Button type="primary" href='/'>
            返回首页
          </Button>
        }
      />
    )
  }
}