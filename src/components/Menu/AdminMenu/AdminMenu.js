/**
 * @File   : AdminMenu.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/18
 * @Desc   : 管理后台菜单
 **/

import {Menu} from "antd";
import React, {useState} from 'react';
import {NavLink} from "react-router-dom";

import "./AdminMenu.sass";


const {SubMenu} = Menu;

const AdminMenu = (props) => {

  const rootSubmenuKeys =
    ['HomeContent', 'ProjectManage', 'LeaseManage',
      'SystemManage', 'DataManage', 'TrainManage',
      'ModelManage', 'CheckManage', 'DefectManage'];

  const [openKeys, setOpenKeys] = useState(['HomeContent']);

  const onOpenChange = keys => {
    const latestOpenKey = keys.find(key => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };

  return (
    <div className={'admin-menu'}>
      <Menu
        className={'menu'}
        defaultSelectedKeys={['ContentManage']}
        defaultOpenKeys={['ContentManage']}
        onOpenChange={onOpenChange}
        openKeys={openKeys}
        mode="inline"
        inlineCollapsed={props.collapsed}
      >
        <SubMenu
          className={'content-manage manage'}
          key="ContentManage" title={props.collapsed?null:'内容发布'}
          icon={
            <svg className="icon" aria-hidden="true">
              <use xlinkHref="#iconContentManage"></use>
            </svg>
          }
          defaultSelectedKeys={['HomeContent']}
          defaultOpenKeys={['HomeContent']}
        >
          <Menu.Item key="HomeContent">
            <NavLink to={"/backstage/contentManage/homeContent"}> 官网内容</NavLink>
          </Menu.Item>
          <Menu.Item key="InfoContent">
            <NavLink to={"/backstage/contentManage/infoContent"}>资讯发布</NavLink>
          </Menu.Item>
          <Menu.Item key="MessageContent">
            <NavLink to={"/backstage/contentManage/messageContent"}>信息发布</NavLink>
          </Menu.Item>
        </SubMenu>
        <SubMenu
          className={'project-manage manage'}
          key="ProjectManage" title={props.collapsed?null:'项目管理'}
          icon={
            <svg className="icon" aria-hidden="true">
              <use xlinkHref="#iconProjectSetting"/>
            </svg>
          }>
          <Menu.Item key="ProjectSetting">
            <NavLink
              to={"/backstage/projectManage/projectSetting"}>项目设置</NavLink>
          </Menu.Item>
        </SubMenu>
        <SubMenu
          className={'lease-manage manage'}
          key="LeaseManage"  title={props.collapsed?null:'租赁管理'}
          icon={
            <svg className="icon" aria-hidden="true" >
              <use xlinkHref="#iconLeaseManage"/>
            </svg>
          }>
          <Menu.Item key="PlatformLease">
            <NavLink to={"/backstage/leaseManage/platformLease"}>平台租赁</NavLink>
          </Menu.Item>
          <Menu.Item key="ServiceLease">
            <NavLink to={"/backstage/leaseManage/serviceLease"}>服务租赁</NavLink>
          </Menu.Item>
          <Menu.Item key="ModelLease">
            <NavLink to={"/backstage/leaseManage/modelLease"}>模型租赁</NavLink>
          </Menu.Item>
        </SubMenu>
        <SubMenu
          className={'system-manage manage'}
          key="SystemManage"  title={props.collapsed?null:'系统管理'}
          icon={
            <svg className="icon" aria-hidden="true">
              <use xlinkHref="#iconSystemManage"/>
            </svg>
          }>
          <Menu.Item key="UserManage">
            <NavLink
              to={"/backstage/systemManage/userManage"}>用户管理</NavLink>
          </Menu.Item>
          <Menu.Item key="RoleManage">
            <NavLink to={"/backstage/systemManage/roleManage"}>角色管理</NavLink>
          </Menu.Item>
          <Menu.Item key="PermissionManage">
            <NavLink
              to={"/backstage/systemManage/permissionManage"}>权限管理</NavLink>
          </Menu.Item>
        </SubMenu>
        <SubMenu
          className={'data-manage manage'}
          key="DataManage"  title={props.collapsed?null:'数据管理'}
          icon={
            <svg className="icon" aria-hidden="true">
              <use xlinkHref="#iconDataManage"/>
            </svg>
          }>
          <Menu.Item key="SrcData">
            <NavLink
              to={"/backstage/dataManage/srcData"}>原始数据</NavLink>
          </Menu.Item>
          <Menu.Item key="MarkData">
            <NavLink to={"/backstage/dataManage/markData"}>标注数据</NavLink>
          </Menu.Item>
        </SubMenu>
        <SubMenu
          className={'train-manage manage'}
          key="TrainManage"  title={props.collapsed?null:'训练管理'}
          icon={
            <svg className="icon" aria-hidden="true" >
              <use xlinkHref="#iconTrainManage"/>
            </svg>
          }>
          <Menu.Item key="ModelTrain">
            <NavLink
              to={"/backstage/trainManage/modelTrain"}>模型训练</NavLink>
          </Menu.Item>
        </SubMenu>
        <SubMenu
          className={'model-manage manage'}
          key="ModelManage"  title={props.collapsed?null:'模型管理'}
          icon={
            <svg className="icon" aria-hidden="true">
              <use xlinkHref="#iconModelManage"/>
            </svg>
          }>
          <Menu.Item key="ModelSetting">
            <NavLink
              to={"/backstage/modelManage/modelSetting"}>模型操作</NavLink>
          </Menu.Item>
        </SubMenu>
        <SubMenu
          className={'check-manage manage'}
          key="CheckManage"  title={props.collapsed?null:'检测管理'}
          icon={
            <svg className="icon" aria-hidden="true">
              <use xlinkHref="#iconCheckManage"/>
            </svg>
          }>
          <Menu.Item key="CheckSetting">
            <NavLink
              to={"/backstage/checkManage/checkSetting"}>检测设置</NavLink>
          </Menu.Item>
        </SubMenu>
        <SubMenu
          className={'defect-manage manage'}
          key="DefectManage"  title={props.collapsed?null:'缺陷管理'}
          icon={
            <svg className="icon" aria-hidden="true">
              <use xlinkHref="#iconDefectManage"/>
            </svg>
          }>
          <Menu.Item key="DefectType">
            <NavLink
              to={"/backstage/defectManage/defectType"}>缺陷类型</NavLink>
          </Menu.Item>
        </SubMenu>
      </Menu>
    </div>
  )
}

export default AdminMenu