/**
 * @File   : ProjectMenu.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/12/14
 * @Desc   : 项目菜单组件
 **/

import {Menu} from "antd";
import React, {useState} from "react";
import {NavLink} from "react-router-dom";
import {AppstoreOutlined, SettingOutlined} from '@ant-design/icons';

import "./ProjectMenu.sass";

const {SubMenu} = Menu;

const ProjectMenu = (props) => {
  const {auth_id, project_id} = props;
  const rootSubmenuKeys = ['projectInfo', 'projectSetting'];
  const [openKeys, setOpenKeys] = useState(['projectInfo']);

  const onOpenChange = keys => {
    const latestOpenKey = keys.find(key => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };

  return (
    <div className={'project-menu'}>
      <Menu mode="inline" openKeys={openKeys} className={'menu'}
            onOpenChange={onOpenChange} inlineCollapsed={props.collapsed}>
        <SubMenu key="projectInfo" className={'project-info'} icon={<AppstoreOutlined/>} title="项目信息">
          <Menu.Item key="home">
            <svg className={'icon'} aria-hidden={"true"}>
              <use xlinkHref={'#iconhome'}></use>
            </svg>
            <NavLink className={'menuItem'} to={`/pv/project/${auth_id}/${project_id}/home`}>首页</NavLink>
          </Menu.Item>
          <Menu.Item key="report">
            <svg className="icon" aria-hidden="true">
              <use xlinkHref="#iconreport"></use>
            </svg>
            <NavLink className={'menuItem'} to={`/pv/project/${auth_id}/${project_id}/report`}>报告</NavLink>
          </Menu.Item>
          <Menu.Item key="defect">
            <svg className="icon" aria-hidden="true">
              <use xlinkHref="#icondefect"></use>
            </svg>
            <NavLink className={'menuItem'} to={`/pv/project/${auth_id}/${project_id}/defect`}>缺陷</NavLink>
          </Menu.Item>
          <Menu.Item key="map">
            <svg className="icon" aria-hidden="true">
              <use xlinkHref="#iconmap"></use>
            </svg>
            <NavLink className={'menuItem'} to={`/pv/project/${auth_id}/${project_id}/map`}>地图</NavLink>
          </Menu.Item>
          <Menu.Item key="message">
            <svg className="icon" aria-hidden="true">
              <use xlinkHref="#iconmessage"></use>
            </svg>
            <NavLink className={'menuItem'}
              to={`/pv/project/${auth_id}/${project_id}/message`}>消息</NavLink>
          </Menu.Item>
          <Menu.Item key="team">
            <svg className="icon" aria-hidden="true">
              <use xlinkHref="#iconteam"></use>
            </svg>
            <NavLink className={'menuItem'} to={`/pv/project/${auth_id}/${project_id}/equipment`}>设备</NavLink>
          </Menu.Item>
        </SubMenu>
        <SubMenu key="projectSetting" className={'project-setting'} icon={<SettingOutlined/>} title="项目设置">
          <Menu.Item key="data">
            <svg className="icon" aria-hidden="true">
              <use xlinkHref="#iconproject"></use>
            </svg>
            <NavLink className={'menuItem'}
              to={`/pv/project/${auth_id}/${project_id}/data`}>数据</NavLink>
          </Menu.Item>
          <Menu.Item key="member">
            <svg className="icon" aria-hidden="true">
              <use xlinkHref="#iconmember"></use>
            </svg>
            <NavLink className={'menuItem'} to={`/pv/project/${auth_id}/${project_id}/team`}>成员</NavLink>
          </Menu.Item>
          <Menu.Item key="permission">
            <svg className="icon" aria-hidden="true">
              <use xlinkHref="#iconpermission"></use>
            </svg>
            <NavLink className={'menuItem'}
              to={`/pv/project/${auth_id}/${project_id}/permission`}>权限</NavLink>
          </Menu.Item>
        </SubMenu>
      </Menu>
    </div>
  )
}

export default ProjectMenu