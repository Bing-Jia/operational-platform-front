/**
 * @File   : index.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/4
 * @Desc   : 组件入口js
 **/

import React from "react";
import ReactDOM from "react-dom";
import {ConfigProvider} from "antd";
import {Provider} from "react-redux";
import intl from 'react-intl-universal';
import zh_CN from 'antd/es/locale/zh_CN';
import en_US from 'antd/es/locale/en_US';
import {BrowserRouter as Router} from "react-router-dom";

import Routers from "./Routers";
import store from "./redux/store";
import {emit} from "./utils/emit";

import "antd/dist/antd.css";
import 'cesium/Widgets/widgets.css';


const locales = {
  'en-US': require('./assets/locales/en-US.json'),
  'zh-CN': require('./assets/locales/zh-CN.json'),
};

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      antdLang: zh_CN
    }
  }

  componentDidMount() {
    emit.on('change_language', lang => this.loadLocales(lang));
    this.loadLocales()
  }

  loadLocales = (lang = 'zh-CN') => {
    intl.init({
      currentLocale: lang,
      locales,
    }).then(() => {
      this.setState({
        antdLang: lang === 'zh-CN' ? zh_CN : en_US
      })
    })
  };

  render() {
    return (
      <Provider store={store}>
        <ConfigProvider locale={this.state.antdLang}>
          <Router>
            <Routers/>
          </Router>
        </ConfigProvider>
      </Provider>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
