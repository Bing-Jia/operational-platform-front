/**
 *
 * 提供get, post, put, patch, delete请求
 *
 * */

import axios from "axios";


export default function ajax(url, data, method) {
  if (method === "GET") {
    // 转换格式
    return axios({
      method: 'GET',
      url: url,
      params: data,
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': localStorage.getItem("Authorization")
      }

    })
  } else if (method ==="POST") {
    // 转换格式
    let formData = new FormData();
    for (const key in data) {
      formData.append(key, data[key])
    }

    return axios({
      method: 'POST',
      url: url,
      data: formData,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Authorization': localStorage.getItem("Authorization")
      }
    })
  } else if (method === "PUT") {
    return axios.put(url, data)
  } else if (method === "PATCH") {
    // 转换格式
    let formData = new FormData();
    for (const key in data) {
      formData.append(key, data[key])
    }

    return axios({
      method: 'PATCH',
      url: url,
      data: formData,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Authorization': localStorage.getItem("Authorization")
      }
    })

  } else if (method === "DELETE") {

  }
}