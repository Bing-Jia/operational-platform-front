/**
 * @File   : request.js
 * @Author : xue.xiaoBing
 * @Date   : 2020/11/4
 * @Desc   : 所有的action的dispatch,
 *           包括触发api后台的action和非后台的action。
 **/


import {api} from './api';
import * as action from '../redux/actions';
import setAuthToken from '../utils/AuthToken';
import {openNotification} from "../utils/Notification";


/**
 * 用户
 * */

// 用户注册
export const register = (user) => {
  return api.reqRegister(user)
    .then(res => {
      openNotification("注册操作", "注册成功，即将跳转到首页！", 2)
      this.history.push('/login')
    })
    .catch(res => {
      openNotification("注册失败", this.state.errorInfo, 2)
    });
};

// 用户登录
export const login = (user) => {
  return async dispatch => {
    return await api.reqLogin(user)
      .then(res => {
        // 添加redux中的数据
        dispatch(action.loginUser(res.data));
        // 保存用户信息
        localStorage.setItem('userName', res.data.full_name);
        localStorage.setItem('Authorization', 'Bearer ' + res.data.token);
        // 添加请求头中的信息
        setAuthToken('Bearer ' + res.data.token);

        openNotification("登录操作", "登录成功，即将跳转到首页！", 2)
      })
      .catch(() => {
        openNotification("登录失败", '登录失败', 2)
      })
  }
};

// 用户登出
export const logout = () => {
  return dispatch => {
    // 取消请求头中的信息
    setAuthToken(false);
    // 清除掉redux中的数据
    dispatch(action.logoutUser());
    // 清除local storage中的信息
    localStorage.removeItem('userName');
    localStorage.removeItem('Authorization');
  }
};


/**
 * 项目
 * */
// 添加项目
export const createProject = (data) => {
  return dispatch => {
    return api.reqCreateProject(data)
  }
};

// 添加项目成员
export const addProjectMember = (data) => {
  return dispatch => {
    return api.reqAddProjectMember(data)
      .then(
        ret => {
          console.log(ret, "-------------member---------------")
        }
      )
  }
};

// 通过类型获取项目列表
export const getProjectsByType = (project_type) => {
  return dispatch => {
    return api.reqGetProjectsByType(project_type)
      .then(
        (ret) =>
          dispatch(action.getProjectsByType(ret.data))
      )
  }
};

// 获取项目总数
export const getProjectTotal = () => {
  return dispatch => {
    return api.reqGetProjectsTotal()
  }
};

// 获取所有项目
export const getProjects = () => {
  return dispatch => {
    return api.reqGetProjects()
      .then(ret =>
        dispatch(action.getAllProjects(ret.data))
      )
  }
};

// 获取单个项目
export const getProject = (projectId) =>{
  return dispatch => {
    return api.reqGetProject(projectId)
  }
};


// 获取项目进度
export const getProjectProgress = (id) => {
  return dispatch => {
    return api.reqGetProjectProgress(id)
  }
};
/**
 * 缺陷
 * */

// 获取所有缺陷
export const getAdminDefectType = () => {
  return dispatch => {
    return api.reqGetAdminDefectTypes()
      .then(
        ret =>
          dispatch(action.getDefectTypes(ret.data))
      )
  }
};

export const checkDefect = (data) => {
  return async dispatch => {
    return await api.reqCheckDefect(data)
  }
};

/**
 * 数据
 * */
// 分片检测
export const checkFile = (data) => {
  return dispatch => {
    return api.reqCheckFile(data)
  }
};

// 分片检测
export const checkChunk = (data) => {
  return dispatch => {
    return api.reqCheckChunk(data)
  }
};

// 分片上传
export const uploadChunk = api.reqUploadChunk;

// 分片合并
export const mergeChunk = (data) => {
  return dispatch => {
    return api.reqMergeChunk(data)
  }
};


/**
 * 项目角色
 * */

export const getProjectRole = (id) => {
  return async dispatch => {
    return await api.reqProjectRole(id)
  }
};
export const changeProjectRole = (projectId, roleId, data) => {
  return async dispatch => {
    return await api.reqChangeProjectRole(projectId, roleId, data)
  }
};

/**
 * 训练
 * */

export const trainModel = (data) => {
  return async dispatch => {
    return await api.reqTrainModel(data)
  }
};


/**
 * 项目机器人
 * */

export const sendMessageToReboot = (message) => {
  return async dispatch => {
    return await api.reqSendMessageToReboot(message)
  }
};



