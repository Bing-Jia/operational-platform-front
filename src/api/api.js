/**
 *
 * 提供前端所有api请求
 *
 * */

import ajax from './ajax';
import axios from 'axios';


export const api = {

  /**
   * 用户
   **/

  // 注册
  reqRegister(user) {
    return ajax(
      '/api/v1/auth/register',
      {
        full_name: user.full_name,
        username: user.username,
        email: user.email,
        password: user.password,
        accepted_terms: user.accepted_terms
      },
      'POST')
  },

  // 登录
  reqLogin(user) {
    return ajax(
      '/api/v1/auth/login',
      {
        username: user.username,
        password: user.password
      },
      'POST')
  },

  // 通过名字获取用户
  reqSearchUserByName(username) {
    return ajax(
      '/api/v1/users/by_username',
      {"username": username},
      'GET'
    )
  },


  /**
   * 项目
   * */
  reqCreateProject(projectData) {
    return ajax(
      '/api/v1/projects',
      projectData,
      'POST'
    )
  },

  reqAddProjectMember(memberData) {
    console.log(memberData, 'memberData')
    return ajax(
      '/api/v1/project/' + memberData.project_id + '/memberships',
      memberData,
      'POST'
    )
  },

  reqGetProjectsTotal() {
    return ajax(
      '/api/v1/admin/projects/1/get_project_number',
      null,
      'GET'
    )
  },

  // 获取所有项目列表
  reqGetProjects() {
    return ajax(
      '/api/v1/projects',
      null,
      'GET'
    )
  },

  // 通过类型获取项目列表
  reqGetProjectsByType(projectType) {
    return ajax(
      '/api/v1/projects',
      {
        "project_type_id": projectType,
        "owner_id": 1
      },
      'GET'
    )
  },

  // 获取单个项目信息
  reqGetProject(projectId){
    return ajax(
      '/api/v1/projects/' + projectId,
      {},
      'GET'
    )
  },

  // 获取项目进度
  reqGetProjectProgress(projectId) {
    return ajax(
      '/api/v1/projects/' + projectId + '/get_project_progress',
      {},
      'GET'
    )
  },

  /**
   * 缺陷
   * */

  // 获取缺陷类型
  reqGetAdminDefectTypes() {
    return ajax(
      '/api/v1/admin/checkTypes',
      {},
      'GET'
    )
  },

  // 缺陷检测
  reqCheckDefect(data) {
    return ajax(
      "/api/v1/project/" + data.project_id + "/checks/start_check",
      data,
      "POST"
    )
  },

  /**
   *  数据
   * */

  // 文件检测
  reqCheckFile(data) {
    return ajax(
      "/api/v1/project/" + data.project_id + "/datas/check_file",
      data,
      "POST"
    )
  },

  // 分片检测
  reqCheckChunk(data) {
    return ajax(
      "/api/v1/project/" + data.project_id + "/datas/check_chunk",
      data,
      "POST"
    )
  },

  // 分片上传
  reqUploadChunk: "/api/v1/project/5/datas/upload_chunk",


  // 分片合并
  reqMergeChunk(data) {
    return ajax(
      "/api/v1/project/" + data.project_id + "/datas/merge_chunk",
      data,
      "POST"
    )
  },

  /**
   *  角色
   * */

  // 获取项目角色权限
  reqProjectRole(id) {
    return ajax(
      "/api/v1/project/" + id + "/roles",
      {},
      "GET"
    )
  },

   // 修改项目角色权限
  reqChangeProjectRole(projectId, roleId, data) {
    return ajax(
      "/api/v1/project/" + projectId + "/roles/" + roleId,
      data,
      "PATCH"
    )
  },

  /**
   *  训练
   * */

  // 文件训练
  reqTrainModel(data) {
    return ajax(
      "/api/v1/admin/trains/models",
      data,
      "POST"
    )
  },


  /**
   *  外部第三方api
   * */

  // 机器人
  reqSendMessageToReboot(message) {
    return axios({
        url: 'https://api.ownthink.com/bot?appid=xiaosi&userid=user&spoken=' + message,
        method: 'GET'
      }
    )
  },

};


